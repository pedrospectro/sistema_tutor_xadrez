
#include <stdio.h>
#include <stdlib.h>


/*Altere aqui a profundidade mÃ¡xima da 
busca heurÃ­stica competitiva*/
#define DEPTH 4 


/*Variaveis globais*/
int momentoJogo = 0; 
//0 = inicio , 1 = meio , fim = 3
int rodadas=0;	     
//NÃºmero de rodadas da partida
int noSet=0;	     
//Quantidade de nÃ³s avaliados na busca competitiva
int qtLances = 50;   
//Quantidade de lances mÃ¡ximos apÃ³s um rei ficar sozinho
int reiSo = 0;       
//Se algum rei estÃ¡ sozinho no tabuleiro, entÃ£o reiSo <- 1
int melhorFatNeto=0; 
//Armazena a melhor pontuaÃ§Ã£o dos estados netos


typedef void* objeto;

typedef unsigned char byte;


typedef struct no
{
	objeto o;
	struct no *prox;	
}no;

typedef struct lista
{
	int tam;
	no *head;
}lista;

typedef struct pos
{
	char l; //linha
	char c; //coluna
}pos;

typedef struct estado
{
	//Valor heurÃ­stico propagado na arvore de decisoes...
	int h; 	
	//Lista de filhos com as prÃ³ximas jogadas
	lista *filhos; 
	//Tabuleiro do estado atual
	byte tab[8][8]; 
	//fat atual do estado...
	int fatCorrente; 
	//Variaveis utilizadas para ROOK
	byte reiP1Mexeu;  
	byte reiP2Mexeu;
	byte torre1P1Mexeu;
	byte torre1P2Mexeu;
	byte torre2P1Mexeu;
	byte torre2P2Mexeu;

	//Posicao de cada peca (linha e coluna do tabuleiro)
	pos reiP1;     pos damaP1;
	pos reiP2;     pos damaP2;
	pos torre1P1;  pos torre2P1;
	pos torre1P2;  pos torre2P2;
	pos cavalo1P1; pos cavalo2P1;
	pos cavalo1P2; pos cavalo2P2;
	pos bispo1P1;  pos bispo2P1;
	pos bispo1P2;  pos bispo2P2;
	pos peaoP1[8]; pos peaoP2[8];
	/*Bytes que indicam os peÃµes promovidos para cada 
	jogador (Inicialmente 0b00000000)
	Exemplo, se o peÃ£o 1 do jogador 1 foi promovido 
	entÃ£o prom1 = prom1 | 0b10000000 = 0b10000000 (Or bitwise)*/
	byte prom1;
	byte prom2;
}estado;

/*------------------------------------------------------------------------------------------------------------*/
void destroiEstado(estado *e);


lista *constroiLista()
{
	lista * l = (lista *)malloc(sizeof(lista));
	if(!l){ printf("lista nao criada\n");return NULL;}

	l->tam = 0;
	l->head=NULL;
	return l;
}

no *constroiNo(objeto o)
{
	no * n = (no *)malloc(sizeof(no));	
	if(!n){ printf("No nao criado\n");return NULL;}

	n->o = o;
	n->prox=NULL;
	return n;	
}

int tamLista(lista *l)
{
	return l->tam;;
}

void insereLista(lista *l,objeto o)
{
	no* p=constroiNo(o);
	
	if(tamLista(l)==0)
		l->head=p;
	else
	{
		int i=0;
		no * aux = l->head;
		int tam = tamLista(l);
		for(i;i<tam-1;i++)
			aux=aux->prox;
		aux->prox=p;
	}
	l->tam=l->tam+1;
}

void insereListaProx(lista *l,objeto o)
{
	no* p=constroiNo(o);
	
	if(tamLista(l)==0)
		l->head=p;
	else
	{
		int i=0;
		no * aux = l->head;
		int tam = tamLista(l);
		for(i;i<tam-1;i++)
			aux=aux->prox;
		aux->prox=p;
	}
	l->tam=l->tam+1;
}

void destroiLista(lista *l)
{
	if(l->head==NULL)
		return;

	no *aux = l->head;
	l->head = aux->prox;
	l->tam = l->tam-1;
	free(aux);

	destroiLista(l);
}

void imprimeLista(lista *l)
{
	int i=0;int val;

	int tam = l->tam;
	no *aux = l->head;
	for(i;i<tam;i++)
	{
		val = aux->o;
		printf("%d \n",val);
		aux=aux->prox;
	}
}
/*-----------------------------------------------------------------------------------------------------------------*/
void setPos(pos *p,int l,int c)
{
	p->l=l;
	p->c=c;
}

void informaEstado(estado *e);

pos getPos(estado *e,byte peca)
{
	if(peca<=16)
	{
		
		switch(peca)
		{	
			case 1:
				return e->reiP1;
			case 2:
				return e->damaP1;
			case 3:
				return e->torre1P1;
			case 4:
				return e->torre2P1;
			case 7:
				return e->bispo1P1;
			case 8:
				return e->bispo2P1;
			case 5:
				return e->cavalo1P1;
			case 6:
				return e->cavalo2P1;
			default:
				return e->peaoP1[peca-9];
		}
	}
	else
	{
		peca = peca-16;
		switch(peca)
		{	
			case 1:
				return e->reiP2;
			case 2:
				return e->damaP2;
			case 3:
				return e->torre1P2;
			case 4:
				return e->torre2P2;
			case 7:
				return e->bispo1P2;
			case 8:
				return e->bispo2P2;
			case 5:
				return e->cavalo1P2;
			case 6:
				return e->cavalo2P2;

			default:
				return e->peaoP2[peca-9];
		}
	}
}

pos *getPosA(estado *e,byte peca)
{
	if(peca<=16)
	{
		
		switch(peca)
		{	
			case 1:
				return &e->reiP1;
			case 2:
				return &e->damaP1;
			case 3:
				return &e->torre1P1;
			case 4:
				return &e->torre2P1;
			case 7:
				return &e->bispo1P1;
			case 8:
				return &e->bispo2P1;
			case 5:
				return &e->cavalo1P1;
			case 6:
				return &e->cavalo2P1;
			default:
				return &e->peaoP1[peca-9];
		}
	}
	else
	{
		peca = peca-16;
		switch(peca)
		{	
			case 1:
				return &e->reiP2;
			case 2:
				return &e->damaP2;
			case 3:
				return &e->torre1P2;
			case 4:
				return &e->torre2P2;
			case 7:
				return &e->bispo1P2;
			case 8:
				return &e->bispo2P2;
			case 5:
				return &e->cavalo1P2;
			case 6:
				return &e->cavalo2P2;
			default:
				return &e->peaoP2[peca-9];
		}
	}
}

void verificaReiSozinho(estado *e)
{
	int i;
	pos p;
	int peca=0;
	for(i=2;i<17;i++)
	{
		p=getPos(e,i);
		if(p.l!=-1)
		{	peca=1;break;}
	}

	if((i==17)&&(peca==0))
	{	reiSo=1;return;} 


	peca=0;
	for(i=18;i<32;i++)
	{
		p=getPos(e,i);
		//printf("p.l de %d vale %d",i,p.l);
		if(p.l!=-1)
		{	peca=1;break;}
	}

	if((i==32)&&(peca==0))
	{	reiSo=1;return;}
}

//Estado Inicial
estado *constroiEstadoInicial() 
{
	estado* e = (estado*)malloc(sizeof(estado));
	e->h=0;
	e->filhos = constroiLista();	

	int i=0;int j=0;
	for(i;i<8;i++)
		for(j;j<8;j++)
			e->tab[i][j]=0;

	e->prom1=0;
	e->prom2=0;
	rodadas=0;
	e->reiP1Mexeu=0;  
	e->reiP2Mexeu=0;
	e->torre1P1Mexeu=0;
	e->torre1P2Mexeu=0;
	e->torre2P1Mexeu=0;
	e->torre2P2Mexeu=0;


	e->tab[0][3]=1;
	setPos(&e->reiP1,0,3);
	e->tab[0][4]=2;
	setPos(&e->damaP1,0,4);

	e->tab[0][0]=3;
	setPos(&e->torre1P1,0,0);
	e->tab[0][7]=4;
	setPos(&e->torre2P1,0,7);

	e->tab[0][1]=5;
	setPos(&e->cavalo1P1,0,1);
	e->tab[0][6]=6;
	setPos(&e->cavalo2P1,0,6);
	
	e->tab[0][2]=7;
	setPos(&e->bispo1P1,0,2);
	e->tab[0][5]=8;
	setPos(&e->bispo2P1,0,5);
	

	for(i=1;i<=8;i++)
	{
		e->tab[1][i-1]=8+i;
		setPos(&e->peaoP1[i-1],1,i-1);
	}


	e->tab[7][3]=17;
	setPos(&e->reiP2,7,3);
	e->tab[7][4]=18;
	setPos(&e->damaP2,7,4);

	e->tab[7][0]=19;
	setPos(&e->torre1P2,7,0);
	e->tab[7][7]=20;
	setPos(&e->torre2P2,7,7);

	e->tab[7][1]=21;
	setPos(&e->cavalo1P2,7,1);
	e->tab[7][6]=22;
	setPos(&e->cavalo2P2,7,6);
	
	e->tab[7][2]=23;
	setPos(&e->bispo1P2,7,2);
	e->tab[7][5]=24;
	setPos(&e->bispo2P2,7,5);
	

	for(i=1;i<=8;i++)
	{
		e->tab[6][i-1]=24+i;
		setPos(&e->peaoP2[i-1],6,i-1);
	}
	return e;	
}

estado *constroiEstadoInicialErro() 
{
	estado* e = (estado*)malloc(sizeof(estado));
	e->h=0;
	e->filhos = constroiLista();	

	int i=0;int j=0;
	for(i;i<8;i++)
		for(j;j<8;j++)
			e->tab[i][j]=0;

	e->prom1=0;
	e->prom2=0;
	rodadas=10;
	momentoJogo=3;
	e->reiP1Mexeu=1;  
	e->reiP2Mexeu=0;
	e->torre1P1Mexeu=0;
	e->torre1P2Mexeu=0;
	e->torre2P1Mexeu=0;
	e->torre2P2Mexeu=0;


	e->tab[0][4]=1;
	setPos(&e->reiP1,0,4);
	//e->tab[0][4]=2;
	setPos(&e->damaP1,-1,-1);

	e->tab[0][0]=3;
	setPos(&e->torre1P1,0,0);
	e->tab[0][7]=4;
	setPos(&e->torre2P1,0,7);

	e->tab[2][2]=5;
	setPos(&e->cavalo1P1,2,2);
	e->tab[0][6]=6;
	setPos(&e->cavalo2P1,0,6);
	
	e->tab[4][6]=7;
	setPos(&e->bispo1P1,4,6);
	e->tab[0][5]=8;
	setPos(&e->bispo2P1,0,5);
	

	for(i=1;i<=8;i++)
	{
		if(i==1)
		{
			e->tab[2][i-1]=8+i;
			setPos(&e->peaoP1[i-1],2,i-1);			
		}
		else if(i==3)
		{
			e->tab[4][i-1]=8+i;
			setPos(&e->peaoP1[i-1],4,i-1);
		}
		else if(i==4)
		{
			e->tab[4][i-1]=8+i;
			setPos(&e->peaoP1[i-1],4,i-1);
		}
		else if((i==5))
		{
			//e->tab[1][i-1]=8+i;
			setPos(&e->peaoP1[i-1],-1,-1);
		}
		else{
			e->tab[1][i-1]=8+i;
			setPos(&e->peaoP1[i-1],1,i-1);
		}
	}

	e->tab[7][3]=17;
	setPos(&e->reiP2,7,3);
	e->tab[7][4]=18;
	setPos(&e->damaP2,7,4);

	e->tab[7][0]=19;
	setPos(&e->torre1P2,7,0);
	e->tab[7][7]=20;
	setPos(&e->torre2P2,7,7);

	//e->tab[7][1]=21;
	setPos(&e->cavalo1P2,-1,-1);
	e->tab[3][4]=22;
	setPos(&e->cavalo2P2,3,4);
	
	e->tab[2][3]=23;
	setPos(&e->bispo1P2,2,3);
	e->tab[7][5]=24;
	setPos(&e->bispo2P2,7,5);


	for(i=1;i<=8;i++)
	{
		if(i==4)
		{
			e->tab[5][3]=24+i;
			setPos(&e->peaoP2[i-1],5,3);
		}
		else if((i==6))
		{
			//e->tab[5][3]=24+i;
			setPos(&e->peaoP2[i-1],-1,-1);
		}
		else
		{
			e->tab[6][i-1]=24+i;
			setPos(&e->peaoP2[i-1],6,i-1);
		}
	}
	return e;	
}

estado *constroiEstadoInicialTeste() 
{
	estado* e = (estado*)malloc(sizeof(estado));
	e->h=0;
	e->filhos = constroiLista();	

	int i=0;int j=0;
	for(i;i<8;i++)
		for(j;j<8;j++)
			e->tab[i][j]=0;

	rodadas=1;momentoJogo=3;
	e->reiP1Mexeu=1;  
	e->reiP2Mexeu=1;
	e->torre1P1Mexeu=1;
	e->torre1P2Mexeu=1;
	e->torre2P1Mexeu=1;
	e->torre2P2Mexeu=1;


	e->tab[0][1]=1;
	setPos(&e->reiP1,0,1);
	//e->tab[0][4]=2;
	setPos(&e->damaP1,-1,-1);

	e->tab[2][2]=3;
	setPos(&e->torre1P1,2,2);
	e->tab[1][1]=4;
	setPos(&e->torre2P1,1,1);

	e->tab[1][0]=5;
	setPos(&e->cavalo1P1,1,0);
//	e->tab[0][6]=6;
	setPos(&e->cavalo2P1,-1,-1);
	
	//e->tab[0][2]=7;
	setPos(&e->bispo1P1,-1,-1);
	//e->tab[0][5]=8;
	setPos(&e->bispo2P1,-1,-1);
	

	for(i=1;i<=8;i++)
	{
		if((i>=3)&&(i<7))
		{
			//e->tab[4][i-1]=8+i;
			setPos(&e->peaoP1[i-1],-1,-1);
		}
		else if((i==1)||(i==2))
		{
			e->tab[3][i-1]=8+i;
			setPos(&e->peaoP1[i-1],3,i-1);	
		}
		else
		{
			e->tab[1][i-1]=8+i;
			setPos(&e->peaoP1[i-1],1,i-1);
		}
	}


	e->tab[7][1]=17;
	setPos(&e->reiP2,7,1);
//	e->tab[7][4]=18;
	setPos(&e->damaP2,-1,-1);

	e->tab[7][0]=19;
	setPos(&e->torre1P2,7,0);
	e->tab[7][2]=20;
	setPos(&e->torre2P2,7,2);

//	e->tab[7][1]=21;
	setPos(&e->cavalo1P2,-1,-1);
	e->tab[5][5]=22;
	setPos(&e->cavalo2P2,5,5);
	
	//e->tab[7][2]=23;
	setPos(&e->bispo1P2,-1,-1);
	//e->tab[7][5]=24;
	setPos(&e->bispo2P2,-1,-1);
	

	for(i=1;i<=8;i++)
	{
		if(i==1)
		{
			e->tab[5][0]=24+i;
			setPos(&e->peaoP2[i-1],5,0);
		}
		if(i==2)
		{
			e->tab[5][2]=24+i;
			setPos(&e->peaoP2[i-1],5,2);
		}
		else if(i==8)
		{
			e->tab[5][7]=24+i;
			setPos(&e->peaoP2[i-1],5,7);
		}
		else if(i==7)
		{
			e->tab[6][6]=24+i;
			setPos(&e->peaoP2[i-1],6,6);
		}
		else
		{
		//	e->tab[6][i-1]=24+i;
			setPos(&e->peaoP2[i-1],-1,-1);
		}
	}
	return e;	
}

estado *constroiEstadoInicialTeste2() 
{
	estado* e = (estado*)malloc(sizeof(estado));
	e->h=0;
	e->filhos = constroiLista();	

	int i=0;int j=0;
	for(i;i<8;i++)
		for(j;j<8;j++)
			e->tab[i][j]=0;

	rodadas=1;momentoJogo=3;
	e->reiP1Mexeu=1;  
	e->reiP2Mexeu=1;
	e->torre1P1Mexeu=1;
	e->torre1P2Mexeu=1;
	e->torre2P1Mexeu=1;
	e->torre2P2Mexeu=1;


	e->tab[0][5]=1;
	setPos(&e->reiP1,0,5);
	//e->tab[0][4]=2;
	setPos(&e->damaP1,-1,-1);

	//e->tab[2][2]=3;
	setPos(&e->torre1P1,-1,-1);
	//e->tab[1][1]=4;
	setPos(&e->torre2P1,-1,-1);

	//e->tab[1][0]=5;
	setPos(&e->cavalo1P1,-1,-1);
//	e->tab[0][6]=6;
	setPos(&e->cavalo2P1,-1,-1);
	
	//e->tab[0][2]=7;
	setPos(&e->bispo1P1,-1,-1);
	//e->tab[0][5]=8;
	setPos(&e->bispo2P1,-1,-1);
	

	for(i=1;i<=8;i++)
	{
		if((i==6))
		{
			e->tab[5][i-1]=8+i;
			setPos(&e->peaoP1[i-1],5,i-1);
		}
		else if((i==8))
		{
			e->tab[2][i-1]=8+i;
			setPos(&e->peaoP1[i-1],2,i-1);	
		}
		else
		{
			//e->tab[1][i-1]=8+i;
			setPos(&e->peaoP1[i-1],-1,-1);
		}
	}


	e->tab[6][2]=17;
	setPos(&e->reiP2,6,2);
//	e->tab[7][4]=18;
	setPos(&e->damaP2,-1,-1);

	e->tab[4][2]=19;
	setPos(&e->torre1P2,4,2);
	//e->tab[7][2]=20;
	setPos(&e->torre2P2,-1,-1);

//	e->tab[7][1]=21;
	setPos(&e->cavalo1P2,-1,-1);
	//e->tab[5][5]=22;
	setPos(&e->cavalo2P2,-1,-1);
	
	//e->tab[7][2]=23;
	setPos(&e->bispo1P2,-1,-1);
	//e->tab[7][5]=24;
	setPos(&e->bispo2P2,-1,-1);
	

	for(i=1;i<=8;i++)
	{
		if(i==1)
		{
			e->tab[6][i-1]=24+i;
			setPos(&e->peaoP2[i-1],6,i-1);
		}
		else if(i==8)
		{
			e->tab[2][5]=24+i;
			setPos(&e->peaoP2[i-1],2,5);
		}
		else
		{
		//	e->tab[6][i-1]=24+i;
			setPos(&e->peaoP2[i-1],-1,-1);
		}
	}
	e->prom1 = 0b00100000;
	e->prom2 = 0b00000000;
	return e;	
}

estado *constroiEstadoInicialTeste3() 
{
	estado* e = (estado*)malloc(sizeof(estado));
	e->h=0;
	e->filhos = constroiLista();	

	int i=0;int j=0;
	for(i;i<8;i++)
		for(j;j<8;j++)
			e->tab[i][j]=0;

	rodadas=1;momentoJogo=3;
	e->reiP1Mexeu=1;  
	e->reiP2Mexeu=1;
	e->torre1P1Mexeu=1;
	e->torre1P2Mexeu=1;
	e->torre2P1Mexeu=1;
	e->torre2P2Mexeu=1;


	e->tab[0][5]=1;
	setPos(&e->reiP1,0,5);
	//e->tab[0][4]=2;
	setPos(&e->damaP1,-1,-1);

	//e->tab[2][2]=3;
	setPos(&e->torre1P1,-1,-1);
	//e->tab[1][1]=4;
	setPos(&e->torre2P1,-1,-1);

	//e->tab[1][0]=5;
	setPos(&e->cavalo1P1,-1,-1);
//	e->tab[0][6]=6;
	setPos(&e->cavalo2P1,-1,-1);
	
	//e->tab[0][2]=7;
	setPos(&e->bispo1P1,-1,-1);
	//e->tab[0][5]=8;
	setPos(&e->bispo2P1,-1,-1);
	

	for(i=1;i<=8;i++)
	{
		//if((i==6))
		//{
		//	e->tab[5][i-1]=8+i;
		//	setPos(&e->peaoP1[i-1],5,i-1);
		//}
		if((i==8))
		{
			e->tab[5][7]=8+i;
			setPos(&e->peaoP1[i-1],5,7);	
		}
		else
		{
			//e->tab[1][i-1]=8+i;
			setPos(&e->peaoP1[i-1],-1,-1);
		}
	}


	e->tab[4][3]=17;
	setPos(&e->reiP2,4,3);
//	e->tab[7][4]=18;
	setPos(&e->damaP2,-1,-1);

	//e->tab[4][2]=19;
	setPos(&e->torre1P2,-1,-1);
	//e->tab[7][2]=20;
	setPos(&e->torre2P2,-1,-1);

//	e->tab[7][1]=21;
	setPos(&e->cavalo1P2,-1,-1);
	//e->tab[5][5]=22;
	setPos(&e->cavalo2P2,-1,-1);
	
	//e->tab[7][2]=23;
	setPos(&e->bispo1P2,-1,-1);
	//e->tab[7][5]=24;
	setPos(&e->bispo2P2,-1,-1);
	

	for(i=1;i<=8;i++)
	{
		if(i==1)
		{
			e->tab[5][i-1]=24+i;
			setPos(&e->peaoP2[i-1],5,i-1);
		}
		else if(i==8)
		{
			e->tab[1][5]=24+i;
			setPos(&e->peaoP2[i-1],1,5);
		}
		else
		{
		//	e->tab[6][i-1]=24+i;
			setPos(&e->peaoP2[i-1],-1,-1);
		}
	}
	e->prom1 = 0b00000000;
	e->prom2 = 0b00000000;
	return e;	
}




estado *constroiEstadoInicial5() 
{
	estado* e = (estado*)malloc(sizeof(estado));
	e->h=0;
	e->filhos = constroiLista();	

	int i=0;int j=0;
	for(i;i<8;i++)
		for(j;j<8;j++)
			e->tab[i][j]=0;

	e->reiP1Mexeu=0;  
	e->reiP2Mexeu=0;
	e->torre1P1Mexeu=0;
	e->torre1P2Mexeu=0;
	e->torre2P1Mexeu=0;
	e->torre2P2Mexeu=0;


	e->tab[3][6]=1;
	setPos(&e->reiP1,3,6);
	e->tab[6][7]=2;
	setPos(&e->damaP1,6,7);

	e->tab[0][0]=3;
	setPos(&e->torre1P1,0,0);
	//e->tab[0][7]=4;
	setPos(&e->torre2P1,-1,-1);

	//e->tab[0][1]=5;
	setPos(&e->cavalo1P1,-1,-1);
	//e->tab[0][6]=6;
	setPos(&e->cavalo2P1,-1,-1);
	
	e->tab[0][2]=7;
	setPos(&e->bispo1P1,0,2);
	//e->tab[0][5]=8;
	setPos(&e->bispo2P1,-1,-1);
	

	/*for(i=1;i<=8;i++)
	{
		e->tab[1][i-1]=8+i;
		setPos(&e->peaoP1[i-1],1,i-1);
	}*/

	e->tab[2][0]=9;
	setPos(&e->peaoP1[0],2,0);

	
	e->tab[1][1]=10;
	setPos(&e->peaoP1[1],1,1);

	e->tab[4][1]=11;
	setPos(&e->peaoP1[2],4,1);

	setPos(&e->peaoP1[3],-1,-1);

	e->tab[5][3]=13;
	setPos(&e->peaoP1[4],5,3);

	e->tab[2][5]=14;
	setPos(&e->peaoP1[5],2,5);

	setPos(&e->peaoP1[6],-1,-1);
	setPos(&e->peaoP1[7],-1,-1);



	e->tab[6][5]=17;
	setPos(&e->reiP2,6,5);
	e->tab[1][6]=18;
	setPos(&e->damaP2,1,6);

	e->tab[7][0]=19;
	setPos(&e->torre1P2,7,0);
	//e->tab[7][7]=20;
	setPos(&e->torre2P2,-1,-1);

	e->tab[7][1]=21;
	setPos(&e->cavalo1P2,7,1);
	//e->tab[7][6]=22;
	setPos(&e->cavalo2P2,-1,-1);
	
	//e->tab[7][2]=23;
	setPos(&e->bispo1P2,-1,-1);
	//e->tab[7][5]=24;
	setPos(&e->bispo2P2,-1,-1);
	

	for(i=1;i<=8;i++)
	{
		if((i==1)||(i==4)||(i==7)){
			e->tab[6][i-1]=24+i;
			setPos(&e->peaoP2[i-1],6,i-1);
		}
		else if((i==6)||(i==8)){
			e->tab[5][i-1]=24+i;
			setPos(&e->peaoP2[i-1],5,i-1);
		}
		else if(i==5){
			e->tab[4][i-1]=24+i;
			setPos(&e->peaoP2[i-1],4,i-1);
		}
		else
		{
			setPos(&e->peaoP2[i-1],-1,-1);
		}
		
	}
	return e;	
}


estado *constroiEstadoInicial4() 
{
	estado* e = (estado*)malloc(sizeof(estado));
	e->h=0;
	e->filhos = constroiLista();	

	int i=0;int j=0;
	for(i;i<8;i++)
		for(j;j<8;j++)
			e->tab[i][j]=0;

	e->reiP1Mexeu=0;  
	e->reiP2Mexeu=1;
	e->torre1P1Mexeu=0;
	e->torre1P2Mexeu=0;
	e->torre2P1Mexeu=0;
	e->torre2P2Mexeu=0;


	e->tab[0][3]=1;
	setPos(&e->reiP1,0,3);
	e->tab[7][4]=2;
	setPos(&e->damaP1,7,4);

	e->tab[0][0]=3;
	setPos(&e->torre1P1,0,0);
	e->tab[0][7]=4;
	setPos(&e->torre2P1,0,7);

	e->tab[0][1]=5;
	setPos(&e->cavalo1P1,0,1);
	e->tab[0][6]=6;
	setPos(&e->cavalo2P1,0,6);
	
	e->tab[0][2]=7;
	setPos(&e->bispo1P1,0,2);
	e->tab[0][5]=8;
	setPos(&e->bispo2P1,0,5);
	

	for(i=1;i<=8;i++)
	{
		if(i==4)
		{
			setPos(&e->peaoP1[i-1],-1,-1);
		}
		else if(i==5)
		{
			setPos(&e->peaoP1[i-1],-1,-1);
		}
		else 
		{
			e->tab[1][i-1]=8+i;
			setPos(&e->peaoP1[i-1],1,i-1);
		}
	}

	e->tab[6][2]=17;
	setPos(&e->reiP2,6,2);
	//e->tab[7][4]=18;
	setPos(&e->damaP2,-1,-1);

	e->tab[7][0]=19;
	setPos(&e->torre1P2,7,0);
	e->tab[7][7]=20;
	setPos(&e->torre2P2,7,7);

	//e->tab[7][1]=21;
	setPos(&e->cavalo1P2,-1,-1);
	e->tab[7][6]=22;
	setPos(&e->cavalo2P2,7,6);
	
	e->tab[7][2]=23;
	setPos(&e->bispo1P2,7,2);
	e->tab[5][3]=24;
	setPos(&e->bispo2P2,5,3);
	

	for(i=1;i<=8;i++)
	{
		if(i==3)
		{
			e->tab[4][3]=24+i;
			setPos(&e->peaoP2[i-1],4,3);			
		}
		else if(i==5)
		{
			//e->tab[4][3]=24+i;
			setPos(&e->peaoP2[i-1],-1,-1);			
		}
	
		else
		{
			e->tab[6][i-1]=24+i;
			setPos(&e->peaoP2[i-1],6,i-1);
		}
	}
	return e;	
}

estado *constroiEstadoInicial6() 
{
	estado* e = (estado*)malloc(sizeof(estado));
	e->h=0;
	e->filhos = constroiLista();	

	int i=0;int j=0;
	for(i;i<8;i++)
		for(j;j<8;j++)
			e->tab[i][j]=0;

	e->reiP1Mexeu=0;  
	e->reiP2Mexeu=1;
	e->torre1P1Mexeu=0;
	e->torre1P2Mexeu=0;
	e->torre2P1Mexeu=0;
	e->torre2P2Mexeu=1;


	e->tab[0][3]=1;
	setPos(&e->reiP1,0,3);
	e->tab[2][2]=2;
	setPos(&e->damaP1,2,2);

	e->tab[0][0]=3;
	setPos(&e->torre1P1,0,0);
	e->tab[0][7]=4;
	setPos(&e->torre2P1,0,7);

	//e->tab[0][1]=5;
	setPos(&e->cavalo1P1,-1,-1);
	e->tab[4][4]=6;
	setPos(&e->cavalo2P1,4,4);
	
	//e->tab[0][2]=7;
	setPos(&e->bispo1P1,-1,-1);
	e->tab[2][3]=8;
	setPos(&e->bispo2P1,2,3);
	

	for(i=1;i<=8;i++)
	{
		if(i==1)
		{
			e->tab[2][i-1]=8+i;
			setPos(&e->peaoP1[i-1],2,i-1);
		}
		else if(i==4)
		{
			e->tab[3][i-1]=8+i;
			setPos(&e->peaoP1[i-1],3,i-1);		
		}
		else if((i==5)||(i==6))
		{
			setPos(&e->peaoP1[i-1],-1,-1);
		}
		else
		{	e->tab[1][i-1]=8+i;
			setPos(&e->peaoP1[i-1],1,i-1);	
		}
	}

	e->tab[7][3]=17;
	setPos(&e->reiP2,7,3);
	e->tab[2][4]=18;
	setPos(&e->damaP2,2,4);

	e->tab[7][0]=19;
	setPos(&e->torre1P2,7,0);
	e->tab[6][7]=20;
	setPos(&e->torre2P2,6,7);

	e->tab[5][2]=21;
	setPos(&e->cavalo1P2,5,2);
	e->tab[7][6]=22;
	setPos(&e->cavalo2P2,7,6);
	
	e->tab[7][2]=23;
	setPos(&e->bispo1P2,7,2);
	//e->tab[5][3]=24;
	setPos(&e->bispo2P2,-1,-1);
	

	for(i=1;i<=8;i++)
	{
		if(i==1)
		{
			e->tab[5][i-1]=24+i;
			setPos(&e->peaoP2[i-1],5,i-1);			
		}
		else if(i==5)
		{
			e->tab[5][4]=24+i;
			setPos(&e->peaoP2[i-1],5,4);			
		}
	
		else if(i==6)
		{
			e->tab[4][6]=24+i;
			setPos(&e->peaoP2[i-1],4,6);
		}

		else if(i==8)
		{
			e->tab[5][i-1]=24+i;
			setPos(&e->peaoP2[i-1],5,i-1);	
		}
		else if((i==2)||(i==3)||(i==4))
		{
			e->tab[6][i-1]=24+i;
			setPos(&e->peaoP2[i-1],6,i-1);		
		}
		else setPos(&e->peaoP2[i-1],-1,-1);
	}
	return e;	
}

estado *constroiEstadoInicial7() 
{
	estado* e = (estado*)malloc(sizeof(estado));
	e->h=0;
	e->filhos = constroiLista();	

	int i=0;int j=0;
	for(i;i<8;i++)
		for(j;j<8;j++)
			e->tab[i][j]=0;

	e->reiP1Mexeu=1;  
	e->reiP2Mexeu=1;
	e->torre1P1Mexeu=1;
	e->torre1P2Mexeu=1;
	e->torre2P1Mexeu=1;
	e->torre2P2Mexeu=1;


	e->tab[0][7]=1;
	setPos(&e->reiP1,0,7);
	//e->tab[2][2]=2;
	setPos(&e->damaP1,-1,-1);

	e->tab[0][6]=3;
	setPos(&e->torre1P1,0,6);
	//e->tab[0][7]=4;
	setPos(&e->torre2P1,-1,-1);

	//e->tab[0][1]=5;
	setPos(&e->cavalo1P1,-1,-1);
	//e->tab[4][4]=6;
	setPos(&e->cavalo2P1,-1,-1);
	
	//e->tab[0][2]=7;
	setPos(&e->bispo1P1,-1,-1);
	//e->tab[2][3]=8;
	setPos(&e->bispo2P1,-1,-1);
	

	for(i=1;i<=8;i++)
	{
		if(i==1)
		{
			e->tab[2][i-1]=8+i;
			setPos(&e->peaoP1[i-1],2,i-1);
		}
		else if((i==4)||(i==5)||(i==6))
		{
			setPos(&e->peaoP1[i-1],-1,-1);
		}
		else if((i==7)||(i==8))
		{
			e->tab[2][i-1]=8+i;
			setPos(&e->peaoP1[i-1],2,i-1);
		}
		else
		{	e->tab[1][i-1]=8+i;
			setPos(&e->peaoP1[i-1],1,i-1);	
		}
	}

	e->tab[6][7]=17;
	setPos(&e->reiP2,6,7);
	//e->tab[2][4]=18;
	setPos(&e->damaP2,-1,-1);

	e->tab[7][7]=19;
	setPos(&e->torre1P2,7,7);
	//e->tab[6][7]=20;
	setPos(&e->torre2P2,-1,-1);

	//e->tab[5][2]=21;
	setPos(&e->cavalo1P2,-1,-1);
	//e->tab[7][6]=22;
	setPos(&e->cavalo2P2,-1,-1);
	
	//e->tab[7][2]=23;
	setPos(&e->bispo1P2,-1,-1);
	//e->tab[5][3]=24;
	setPos(&e->bispo2P2,-1,-1);
	

	for(i=1;i<=8;i++)
	{
		if(i==1)
		{
			e->tab[4][i-1]=24+i;
			setPos(&e->peaoP2[i-1],4,i-1);			
		}
		else if((i==2)||(i==3))
		{
			e->tab[5][i-1]=24+i;
			setPos(&e->peaoP2[i-1],5,i-1);		
		}
		else if(i==4)
		{
			e->tab[4][4]=24+i;
			setPos(&e->peaoP2[i-1],4,4);		
		}
		else if(i==5)
		{
			e->tab[5][i-1]=24+i;
			setPos(&e->peaoP2[i-1],5,i-1);			
		}
	
		else if(i==6)
		{
			e->tab[4][6]=24+i;
			setPos(&e->peaoP2[i-1],4,6);
		}

		else if(i==8)
		{
			e->tab[5][i-1]=24+i;
			setPos(&e->peaoP2[i-1],5,i-1);	
		}

		else setPos(&e->peaoP2[i-1],-1,-1);
	}
	return e;	
}

estado *constroiEstadoInicial8() 
{
	estado* e = (estado*)malloc(sizeof(estado));
	e->h=0;
	e->filhos = constroiLista();	

	int i=0;int j=0;
	for(i;i<8;i++)
		for(j;j<8;j++)
			e->tab[i][j]=0;

	e->reiP1Mexeu=1;  
	e->reiP2Mexeu=1;
	e->torre1P1Mexeu=1;
	e->torre1P2Mexeu=1;
	e->torre2P1Mexeu=1;
	e->torre2P2Mexeu=1;

	rodadas=1;
	e->tab[3][4]=1;
	setPos(&e->reiP1,3,4);
	//e->tab[2][2]=2;
	setPos(&e->damaP1,-1,-1);

	//e->tab[0][0]=3;
	setPos(&e->torre1P1,-1,-1);
	//e->tab[0][7]=4;
	setPos(&e->torre2P1,-1,-1);

	//e->tab[0][1]=5;
	setPos(&e->cavalo1P1,-1,-1);
	//e->tab[4][4]=6;
	setPos(&e->cavalo2P1,-1,-1);
	
	//e->tab[0][2]=7;
	setPos(&e->bispo1P1,-1,-1);
	//e->tab[2][3]=8;
	setPos(&e->bispo2P1,-1,-1);
	

	for(i=1;i<=8;i++)
		setPos(&e->peaoP1[i-1],-1,-1);


	e->prom1=0;
	e->prom2=0;
	e->tab[6][7]=17;
	setPos(&e->reiP2,6,7);
	//e->tab[7][7]=18;
	setPos(&e->damaP2,-1,-1);

	e->tab[7][7]=19;
	setPos(&e->torre1P2,7,7);
	//e->tab[6][5]=20;
	setPos(&e->torre2P2,-1,-1);

	//e->tab[5][2]=21;
	setPos(&e->cavalo1P2,-1,-1);
	//e->tab[7][6]=22;
	setPos(&e->cavalo2P2,-1,-1);
	
	//e->tab[7][2]=23;
	setPos(&e->bispo1P2,-1,-1);
	//e->tab[5][3]=24;
	setPos(&e->bispo2P2,-1,-1);
	

	for(i=1;i<=8;i++)
	{
		setPos(&e->peaoP2[i-1],-1,-1);
	}
	return e;	
}


estado *constroiEstadoInicial3() 
{
	estado* e = (estado*)malloc(sizeof(estado));
	e->h=0;
	e->filhos = constroiLista();	

	int i=0;int j=0;
	for(i;i<8;i++)
		for(j;j<8;j++)
			e->tab[i][j]=0;

	e->reiP1Mexeu=0;  
	e->reiP2Mexeu=0;
	e->torre1P1Mexeu=0;
	e->torre1P2Mexeu=0;
	e->torre2P1Mexeu=0;
	e->torre2P2Mexeu=0;


	e->tab[0][3]=1;
	setPos(&e->reiP1,0,3);
	e->tab[0][4]=2;
	setPos(&e->damaP1,0,4);

	e->tab[0][0]=3;
	setPos(&e->torre1P1,0,0);
	e->tab[0][7]=4;
	setPos(&e->torre2P1,0,7);

	e->tab[4][3]=5;
	setPos(&e->cavalo1P1,4,3);
	e->tab[2][5]=6;
	setPos(&e->cavalo2P1,2,5);
	
	//e->tab[0][2]=7;
	setPos(&e->bispo1P1,-1,-1);
	e->tab[2][3]=8;
	setPos(&e->bispo2P1,2,3);
	

	for(i=1;i<=8;i++)
	{
		if(i==3)
		{
			e->tab[3][i-1]=8+i;
			setPos(&e->peaoP1[i-1],3,i-1);	
		}
		else if(i==4)
		{
			//e->tab[3][i-1]=8+i;
			setPos(&e->peaoP1[i-1],-1,-1);	
		}
		else if(i==5)
		{
			e->tab[3][i-1]=8+i;
			setPos(&e->peaoP1[i-1],3,i-1);	
		}
		else
		{
			e->tab[1][i-1]=8+i;
			setPos(&e->peaoP1[i-1],1,i-1);
		}
	}


	e->tab[7][3]=17;
	setPos(&e->reiP2,7,3);
	e->tab[3][0]=18;
	setPos(&e->damaP2,3,0);

	e->tab[7][0]=19;
	setPos(&e->torre1P2,7,0);
	e->tab[7][7]=20;
	setPos(&e->torre2P2,7,7);

	e->tab[7][1]=21;
	setPos(&e->cavalo1P2,7,1);
	//e->tab[7][6]=22;
	setPos(&e->cavalo2P2,-1,-1);
	
	e->tab[5][6]=23;
	setPos(&e->bispo1P2,5,6);
	e->tab[5][5]=24;
	setPos(&e->bispo2P2,5,5);
	

	for(i=1;i<=8;i++)
	{
		if(i==3)
		{
			//e->tab[3][i-1]=8+i;
			setPos(&e->peaoP2[i-1],-1,-1);	
		}
		else if(i==4)
		{
			e->tab[5][i-1]=24+i;
			setPos(&e->peaoP2[i-1],5,i-1);	
		}
		else if(i==5)
		{
			e->tab[4][i-1]=24+i;
			setPos(&e->peaoP2[i-1],4,i-1);	
		}
		else
		{
			e->tab[6][i-1]=24+i;
			setPos(&e->peaoP2[i-1],6,i-1);
		}
	}
	return e;	
}





estado *constroiEstado() 
{
	estado* e = (estado*)malloc(sizeof(estado));
	e->h=0;
	e->filhos = constroiLista();	

	int i=0;int j=0;
	for(i;i<8;i++)
		for(j;j<8;j++)
			e->tab[i][j]=0;

	e->reiP1Mexeu=0;  
	e->reiP2Mexeu=0;
	e->torre1P1Mexeu=0;
	e->torre1P2Mexeu=0;
	e->torre2P1Mexeu=0;
	e->torre2P2Mexeu=0;


	e->tab[1][5]=1;
	setPos(&e->reiP1,1,5);

	setPos(&e->damaP1,-1,-1);

	e->tab[7][6]=3;
	setPos(&e->torre1P1,7,6);

	setPos(&e->torre2P1,-1,-1);

	setPos(&e->cavalo1P1,-1,-1);
	setPos(&e->cavalo2P1,-1,-1);
	
	setPos(&e->bispo1P1,-1,-1);
	setPos(&e->bispo2P1,-1,-1);
	
	e->tab[1][2]=9;
	setPos(&e->peaoP1[0],1,2);
	for(i=1;i<8;i++)
		setPos(&e->peaoP1[i],-1,-1);


	//e->tab[7][3]=17;
	//setPos(&e->reiP2,7,3);
	//e->tab[3][4]=17;
	//setPos(&e->reiP2,3,4);
	e->tab[1][7]=17;
	setPos(&e->reiP2,1,7);

	setPos(&e->damaP2,-1,-1);

	e->tab[6][6]=19;
	setPos(&e->torre1P2,6,6);

	e->tab[0][2]=20;
	setPos(&e->torre2P2,0,2);
	

	setPos(&e->cavalo1P2,-1,-1);
	setPos(&e->cavalo2P2,-1,-1);
	
	setPos(&e->bispo1P2,-1,-1);
	setPos(&e->bispo2P2,-1,-1);
	
	e->tab[6][1]=25;
	setPos(&e->peaoP2[0],6,1);

	for(i=1;i<8;i++)
		setPos(&e->peaoP2[i],-1,-1);

	return e;	
}

estado *constroiEstado1() 
{
	estado* e = (estado*)malloc(sizeof(estado));
	e->h=0;
	e->filhos = constroiLista();	

	int i=0;int j=0;
	for(i;i<8;i++)
		for(j;j<8;j++)
			e->tab[i][j]=0;

	e->reiP1Mexeu=0;  
	e->reiP2Mexeu=0;
	e->torre1P1Mexeu=0;
	e->torre1P2Mexeu=0;
	e->torre2P1Mexeu=0;
	e->torre2P2Mexeu=0;


	e->tab[0][0]=1;
	setPos(&e->reiP1,0,0);

	setPos(&e->damaP1,-1,-1);

	//e->tab[7][6]=3;
	setPos(&e->torre1P1,-1,-1);

	setPos(&e->torre2P1,-1,-1);

	setPos(&e->cavalo1P1,-1,-1);
	setPos(&e->cavalo2P1,-1,-1);
	
	setPos(&e->bispo1P1,-1,-1);
	setPos(&e->bispo2P1,-1,-1);
	
	e->tab[3][2]=9;
	setPos(&e->peaoP1[0],3,2);
	for(i=1;i<8;i++)
		setPos(&e->peaoP1[i],-1,-1);


	//e->tab[7][3]=17;
	//setPos(&e->reiP2,7,3);
	//e->tab[3][4]=17;
	//setPos(&e->reiP2,3,4);
	e->tab[1][7]=17;
	setPos(&e->reiP2,1,7);

	setPos(&e->damaP2,-1,-1);

	e->tab[7][6]=19;
	setPos(&e->torre1P2,7,6);

	e->tab[1][3]=20;
	setPos(&e->torre2P2,1,3);
	

	setPos(&e->cavalo1P2,-1,-1);
	setPos(&e->cavalo2P2,-1,-1);
	
	setPos(&e->bispo1P2,-1,-1);
	setPos(&e->bispo2P2,-1,-1);
	
	e->tab[6][1]=25;
	setPos(&e->peaoP2[0],6,1);

	for(i=1;i<8;i++)
		setPos(&e->peaoP2[i],-1,-1);

	return e;	
}

estado *constroiEstadoApartirDe(estado *pai)
{
	estado * e = (estado*)malloc(sizeof(estado));
	e->h=0;
	e->filhos = constroiLista();	

	int i=0;int j=0;
	for(i;i<8;i++)
		for(j;j<8;j++)
			e->tab[i][j]=0;

	e->reiP1Mexeu=pai->reiP1Mexeu;  
	e->reiP2Mexeu=pai->reiP2Mexeu;
	e->torre1P1Mexeu=pai->torre1P1Mexeu;
	e->torre1P2Mexeu=pai->torre1P2Mexeu;
	e->torre2P1Mexeu=pai->torre2P1Mexeu;
	e->torre2P2Mexeu=pai->torre2P2Mexeu;
	e->prom1=pai->prom1;
	e->prom2=pai->prom2;

	for(i=0;i<8;i++)
		for(j=0;j<8;j++)
			e->tab[i][j]=pai->tab[i][j];


	setPos(&e->reiP1,pai->reiP1.l,pai->reiP1.c);
	e->tab[pai->reiP1.l][pai->reiP1.c] = 1;
	setPos(&e->damaP1,pai->damaP1.l,pai->damaP1.c);
	e->tab[pai->damaP1.l][pai->damaP1.c] = 2;
	setPos(&e->torre1P1,pai->torre1P1.l,pai->torre1P1.c);
	e->tab[pai->torre1P1.l][pai->torre1P1.c] = 3;
	setPos(&e->torre2P1,pai->torre2P1.l,pai->torre2P1.c);
	e->tab[pai->torre2P1.l][pai->torre2P1.c] = 4;
	setPos(&e->bispo1P1,pai->bispo1P1.l,pai->bispo1P1.c);
	e->tab[pai->bispo1P1.l][pai->bispo1P1.c] = 7;
	setPos(&e->bispo2P1,pai->bispo2P1.l,pai->bispo2P1.c);
	e->tab[pai->bispo2P1.l][pai->bispo2P1.c] = 8;
	setPos(&e->cavalo1P1,pai->cavalo1P1.l,pai->cavalo1P1.c);
	e->tab[pai->cavalo1P1.l][pai->cavalo1P1.c] = 5;
	setPos(&e->cavalo2P1,pai->cavalo2P1.l,pai->cavalo2P1.c);
	e->tab[pai->cavalo2P1.l][pai->cavalo2P1.c] = 6;

	for(i=0;i<8;i++){
		setPos(&e->peaoP1[i],pai->peaoP1[i].l,pai->peaoP1[i].c);
		e->tab[pai->peaoP1[i].l][pai->peaoP1[i].c] = 9+i;
	}	

	setPos(&e->reiP2,pai->reiP2.l,pai->reiP2.c);
	e->tab[pai->reiP2.l][pai->reiP2.c] = 17;

	setPos(&e->damaP2,pai->damaP2.l,pai->damaP2.c);
	e->tab[pai->damaP2.l][pai->damaP2.c] = 18;

	setPos(&e->torre1P2,pai->torre1P2.l,pai->torre1P2.c);
	e->tab[pai->torre1P2.l][pai->torre1P2.c] = 19;
	setPos(&e->torre2P2,pai->torre2P2.l,pai->torre2P2.c);
	e->tab[pai->torre2P2.l][pai->torre2P2.c] = 20;

	setPos(&e->bispo1P2,pai->bispo1P2.l,pai->bispo1P2.c);
	e->tab[pai->bispo1P2.l][pai->bispo1P2.c] = 23;	
	setPos(&e->bispo2P2,pai->bispo2P2.l,pai->bispo2P2.c);
	e->tab[pai->bispo2P2.l][pai->bispo2P2.c] = 24;
	setPos(&e->cavalo1P2,pai->cavalo1P2.l,pai->cavalo1P2.c);
	e->tab[pai->cavalo1P2.l][pai->cavalo1P2.c] = 21;
	setPos(&e->cavalo2P2,pai->cavalo2P2.l,pai->cavalo2P2.c);
	e->tab[pai->cavalo2P2.l][pai->cavalo2P2.c] = 22;

	for(i=0;i<8;i++){
		setPos(&e->peaoP2[i],pai->peaoP2[i].l,pai->peaoP2[i].c);
		e->tab[pai->peaoP2[i].l][pai->peaoP2[i].c] = 25+i;
	}

	return e;	
}

void promovePeaoP1(estado *novo,int posic)
{
	if(posic==0)
		novo->prom1 = novo->prom1|0b00000001;
	else if(posic==1)
		novo->prom1 = novo->prom1|0b00000010;
	else if(posic==2)
		novo->prom1 = novo->prom1|0b00000100;
	else if(posic==3)
		novo->prom1 = novo->prom1|0b00001000;
	else if(posic==4)
		novo->prom1 = novo->prom1|0b00010000;
	else if(posic==5)
		novo->prom1 = novo->prom1|0b00100000;
	else if(posic==6)
		novo->prom1 = novo->prom1|0b01000000; 
	else if(posic==7)
		novo->prom1 = novo->prom1|0b10000000; 
}

void promovePeaoP2(estado *novo,int posic)
{
	if(posic==0)
		novo->prom2 = novo->prom2|0b00000001;
	else if(posic==1)
		novo->prom2 = novo->prom2|0b00000010;
	else if(posic==2)
		novo->prom2 = novo->prom2|0b00000100;
	else if(posic==3)
		novo->prom2 = novo->prom2|0b00001000;
	else if(posic==4)
		novo->prom2 = novo->prom2|0b00010000;
	else if(posic==5)
		novo->prom2 = novo->prom2|0b00100000;
	else if(posic==6)
		novo->prom2 = novo->prom2|0b01000000; 
	else if(posic==7)
		novo->prom2 = novo->prom2|0b10000000; 	
}

int promovidoP1(estado *novo,int posic)
{
	byte result=0;

	if(posic==0)
		result = novo->prom1&0b00000001;
	else if(posic==1)
		result = novo->prom1&0b00000010;
	else if(posic==2)
		result = novo->prom1&0b00000100;
	else if(posic==3)
		result = novo->prom1&0b00001000;
	else if(posic==4)
		result = novo->prom1&0b00010000;
	else if(posic==5)
		result = novo->prom1&0b00100000;
	else if(posic==6)
		result = novo->prom1&0b01000000; 
	else if(posic==7)
		result = novo->prom1&0b10000000; 

	//if(result!=0) printf("Peao promovido\n");
	return result;
}

int promovidoP2(estado *novo,int posic)
{
	byte result=0;

	if(posic==0)
		result = novo->prom2&0b00000001;
	else if(posic==1)
		result = novo->prom2&0b00000010;
	else if(posic==2)
		result = novo->prom2&0b00000100;
	else if(posic==3)
		result = novo->prom2&0b00001000;
	else if(posic==4)
		result = novo->prom2&0b00010000;
	else if(posic==5)
		result = novo->prom2&0b00100000;
	else if(posic==6)
		result = novo->prom2&0b01000000; 
	else if(posic==7)
		result = novo->prom2&0b10000000; 

	return result;
}

int nenhumaPromocao(estado *e,int jogador)
{
	if(jogador==1)
	{
		int i;
		for(i=0;i<8;i++)
		{
			if(e->peaoP1[i].l==-1)
				if(promovidoP1(e,i))
					return 0;
		}
	}
	else
	{
		int i;
		for(i=0;i<8;i++)
		{
			if(e->peaoP2[i].l==-1)
				if(promovidoP2(e,i))
					return 0;
		}
	}

	return 1;
}

void adicionaFilhos(int jogador,byte peca,pos p,estado *e)
{
	int l,c;
	l= p.l;

	c = p.c;

	
	
	if(jogador==1)
	{
		switch(peca)
		{	
			case 1: //movimento de rei
			{	
				if((!e->reiP1Mexeu)&&(!e->torre1P1Mexeu))
				{
					if((posicaoNaoAtacada(l,c,e,jogador))&&
					(posicaoNaoAtacada(0,0,e,jogador)))
					{
						if((posicaoNaoAtacada(0,2,e,jogador))&&
						(posicaoNaoAtacada(0,1,e,jogador)))
						{
							if((e->tab[0][2]==0)&&(e->tab[0][1]==0))
							{
								estado *novo = constroiEstadoApartirDe(e);
								setPos(&novo->reiP1,0,1);
								setPos(&novo->torre1P1,0,2);
								novo->tab[l][c]=0;
								novo->tab[0][1]=1;
								novo->tab[0][0]=0;
								novo->tab[0][2]=3;
								novo->reiP1Mexeu=1;
								novo->torre1P1Mexeu=1;
								insereLista(e->filhos,novo);
							}
						
						}
						
					}
				}

				if((!e->reiP1Mexeu)&&(!e->torre2P1Mexeu))
				{
					if((posicaoNaoAtacada(l,c,e,jogador))&&
					(posicaoNaoAtacada(0,7,e,jogador)))
					{
						if((posicaoNaoAtacada(0,4,e,jogador))&&
						(posicaoNaoAtacada(0,5,e,jogador)))
						{
							if((e->tab[0][4]==0)&&(e->tab[0][5]==0)&&(e->tab[0][6]==0))
							{
								estado *novo = constroiEstadoApartirDe(e);
								setPos(&novo->reiP1,0,5);
								setPos(&novo->torre2P1,0,4);
								novo->tab[l][c]=0;
								novo->tab[0][5]=1;
								novo->tab[0][7]=0;
								novo->tab[0][4]=4;
								novo->reiP1Mexeu=1;
								novo->torre2P1Mexeu=1;
								insereLista(e->filhos,novo);
							}
						
						}
						
					}
				}				
				int nl=l+1;

				if( (nl<8)&&( (e->tab[nl][c]==0) || (e->tab[nl][c]>16) ) )
				{
					if(e->tab[nl][c]==0)
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->reiP1,nl,c);
						novo->tab[l][c]=0;
						novo->tab[nl][c]=1;
						novo->reiP1Mexeu=1;
						if(posicaoNaoAtacada(nl,c,novo,jogador))
							insereLista(e->filhos,novo);
						else free(novo);
					}
					else
					{
						
						estado *novo = constroiEstadoApartirDe(e);

						pos *adversario = getPosA(novo,novo->tab[nl][c]);
						
						setPos(adversario,-1,-1);
						setPos(&novo->reiP1,nl,c);
						novo->tab[l][c]=0;
						novo->tab[nl][c]=1;
						novo->reiP1Mexeu=1;

						if(posicaoNaoAtacada(nl,c,novo,jogador))
							insereLista(e->filhos,novo);
						else free(novo);
					}
					
				}
				nl=l-1;
				if( (nl>-1)&&( (e->tab[nl][c]==0) || (e->tab[nl][c]>16) ) )
				{
					
					if(e->tab[nl][c]==0)
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->reiP1,nl,c);
						novo->tab[l][c]=0;
						novo->tab[nl][c]=1;
						novo->reiP1Mexeu=1;
						if(posicaoNaoAtacada(nl,c,novo,jogador))
							insereLista(e->filhos,novo);
						else free(novo);
					}
					else
					{
						estado *novo = constroiEstadoApartirDe(e);
						//pos adversario = getPos(novo,novo->tab[nl][c]);  
						setPos(getPosA(novo,novo->tab[nl][c]),-1,-1);
						setPos(&novo->reiP1,nl,c);
						novo->tab[l][c]=0;
						novo->tab[nl][c]=1;
						novo->reiP1Mexeu=1;
						if(posicaoNaoAtacada(nl,c,novo,jogador))
							insereLista(e->filhos,novo);
						else free(novo);
					}
					
				}

				int nc=c+1;
				if( (nc<8)&&( (e->tab[l][nc]==0) || (e->tab[l][nc]>16) ) )
				{
					if(e->tab[l][nc]==0)
					{
						estado *novo = constroiEstadoApartirDe(e);

						setPos(&novo->reiP1,l,nc);
						novo->tab[l][c]=0;
						novo->tab[l][nc]=1;
						novo->reiP1Mexeu=1;
						if(posicaoNaoAtacada(l,nc,novo,jogador))
							insereLista(e->filhos,novo);
						else free(novo);
					}
					else
					{
						estado *novo = constroiEstadoApartirDe(e);
						//pos adversario = getPos(novo,novo->tab[l][nc]);
						setPos(getPosA(novo,novo->tab[l][nc]),-1,-1);
						setPos(&novo->reiP1,l,nc);
						novo->tab[l][c]=0;
						novo->tab[l][nc]=1;
						novo->reiP1Mexeu=1;
						if(posicaoNaoAtacada(l,nc,novo,jogador))
							insereLista(e->filhos,novo);
						else free(novo);
					}
					
				}
				nc = c-1;
				if( (nc>-1)&&( (e->tab[l][nc]==0) || (e->tab[l][nc]>16) ) )
				{
					if(e->tab[l][nc]==0)
					{
						estado *novo = constroiEstadoApartirDe(e);

						setPos(&novo->reiP1,l,nc);
						novo->tab[l][c]=0;
						novo->tab[l][nc]=1;
						novo->reiP1Mexeu=1;
						if(posicaoNaoAtacada(l,nc,novo,jogador))
							insereLista(e->filhos,novo);
						else free(novo);
					}
					else
					{
						estado *novo = constroiEstadoApartirDe(e);
						//pos adversario = getPos(novo,novo->tab[l][nc]);
						setPos(getPosA(novo,novo->tab[l][nc]),-1,-1);
						setPos(&novo->reiP1,l,nc);
						novo->tab[l][c]=0;
						novo->tab[l][nc]=1;
						novo->reiP1Mexeu=1;
						if(posicaoNaoAtacada(l,nc,novo,jogador))
							insereLista(e->filhos,novo);
						else free(novo);
					}	
				}
				nl=l-1;
				nc=c-1;	
				if( (nc>-1)&&(nl>-1)&&( (e->tab[nl][nc]==0) || (e->tab[nl][nc]>16) ) )
				{
					if(e->tab[nl][nc]==0)
					{
						estado *novo = constroiEstadoApartirDe(e);

						setPos(&novo->reiP1,nl,nc);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=1;
						novo->reiP1Mexeu=1;
						if(posicaoNaoAtacada(nl,nc,novo,jogador))
							insereLista(e->filhos,novo);
						else free(novo);
					}
					else
					{
						estado *novo = constroiEstadoApartirDe(e);
						//pos adversario = getPos(novo,novo->tab[nl][nc]);
						setPos(getPosA(novo,novo->tab[nl][nc]),-1,-1);
						setPos(&novo->reiP1,nl,nc);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=1;
						novo->reiP1Mexeu=1;
						if(posicaoNaoAtacada(nl,nc,novo,jogador))
							insereLista(e->filhos,novo);
						else free(novo);
					}	
				}
				nl=l+1;
				nc=c-1;	
				if( (nc>-1)&&(nl<8)&&( (e->tab[nl][nc]==0) || (e->tab[nl][nc]>16) ) )
				{
					if(e->tab[nl][nc]==0)
					{
						estado *novo = constroiEstadoApartirDe(e);

						setPos(&novo->reiP1,nl,nc);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=1;
						novo->reiP1Mexeu=1;
						if(posicaoNaoAtacada(nl,nc,novo,jogador))
							insereLista(e->filhos,novo);
						else free(novo);
					}
					else
					{
						estado *novo = constroiEstadoApartirDe(e);
						//pos adversario = getPos(novo,novo->tab[nl][nc]);
						setPos(getPosA(novo,novo->tab[nl][nc]),-1,-1);
						setPos(&novo->reiP1,nl,nc);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=1;
						novo->reiP1Mexeu=1;
						if(posicaoNaoAtacada(nl,nc,novo,jogador))
							insereLista(e->filhos,novo);
						else free(novo);
					}	
				}
				nl=l-1;
				nc=c+1;	
				if( (nc<8)&&(nl>-1)&&( (e->tab[nl][nc]==0) || (e->tab[nl][nc]>16) ) )
				{
					if(e->tab[nl][nc]==0)
					{
						estado *novo = constroiEstadoApartirDe(e);

						setPos(&novo->reiP1,nl,nc);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=1;
						novo->reiP1Mexeu=1;
						if(posicaoNaoAtacada(nl,nc,novo,jogador))
							insereLista(e->filhos,novo);
						else free(novo);
					}
					else
					{
						estado *novo = constroiEstadoApartirDe(e);
						//pos adversario = getPos(novo,novo->tab[nl][nc]);
						setPos(getPosA(novo,novo->tab[nl][nc]),-1,-1);
						setPos(&novo->reiP1,nl,nc);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=1;
						novo->reiP1Mexeu=1;
						if(posicaoNaoAtacada(nl,nc,novo,jogador))
							insereLista(e->filhos,novo);
						else free(novo);
					}	
				}
				nl=l+1;
				nc=c+1;	
				if( (nc<8)&&(nl<8)&&( (e->tab[nl][nc]==0) || (e->tab[nl][nc]>16) ))
				{
					if(e->tab[nl][nc]==0)
					{
						estado *novo = constroiEstadoApartirDe(e);

						setPos(&novo->reiP1,nl,nc);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=1;
						novo->reiP1Mexeu=1;
						if(posicaoNaoAtacada(nl,nc,novo,jogador)) 
							insereLista(e->filhos,novo);
						else free(novo);
					}
					else
					{
						estado *novo = constroiEstadoApartirDe(e);
						//pos adversario = getPos(novo,novo->tab[nl][nc]);
						setPos(getPosA(novo,novo->tab[nl][nc]),-1,-1);
						setPos(&novo->reiP1,nl,nc);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=1;
						novo->reiP1Mexeu=1;
						if(posicaoNaoAtacada(nl,nc,novo,jogador)) 
							insereLista(e->filhos,novo);
						else free(novo);
					}	
				}		 
			}
			case 2:	//Movimento de  damaP1
			{
				pos damaPos = getPos(e,peca);
				int nl;
				int nc;
				if((damaPos.l!=-1)&&(peca==2))
				{
					nl=l+1;
					nc=c+1;
					while((nl<8)&&(nc<8)&&(e->tab[nl][nc]==0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->damaP1,nl,nc);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=peca;

						pos rei = novo->reiP1;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
						nl++;nc++;
					}
					if((nl<8)&&(nc<8)&&(e->tab[nl][nc]>16))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->damaP1,nl,nc);
						//pos adversario = getPos(novo,novo->tab[nl][c]);
						setPos(getPosA(novo,novo->tab[nl][nc]),-1,-1);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=peca;

						pos rei = novo->reiP1;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
					}
					nl=l+1;
					nc=c-1;
					while((nl<8)&&(nc>=0)&&(e->tab[nl][nc]==0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->damaP1,nl,nc);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=peca;

						pos rei = novo->reiP1;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
						nl++;nc--;
					}
					if((nl<8)&&(nc>=0)&&(e->tab[nl][nc]>16))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->damaP1,nl,nc);
						//pos adversario = getPos(novo,novo->tab[nl][c]);
						setPos(getPosA(novo,novo->tab[nl][nc]),-1,-1);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=peca;

						pos rei = novo->reiP1;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
					}
					nl=l-1;
					nc=c-1;
					while((nl>=0)&&(nc>=0)&&(e->tab[nl][nc]==0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->damaP1,nl,nc);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=peca;

						pos rei = novo->reiP1;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
						nl--;nc--;
					}
					if((nl>=0)&&(nc>=0)&&(e->tab[nl][nc]>16))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->damaP1,nl,nc);
						//pos adversario = getPos(novo,novo->tab[nl][c]);
						setPos(getPosA(novo,novo->tab[nl][nc]),-1,-1);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=peca;

						pos rei = novo->reiP1;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
					}
					nl=l-1;
					nc=c+1;
					while((nl>=0)&&(nc<8)&&(e->tab[nl][nc]==0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->damaP1,nl,nc);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=peca;

						pos rei = novo->reiP1;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
						nl--;nc++;
					}
					if((nl>=0)&&(nc<8)&&(e->tab[nl][nc]>16))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->damaP1,nl,nc);
						//pos adversario = getPos(novo,novo->tab[nl][c]);
						setPos(getPosA(novo,novo->tab[nl][nc]),-1,-1);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=peca;

						pos rei = novo->reiP1;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
					}
					nl=l+1;
					while((nl<8)&&(e->tab[nl][c]==0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->damaP1,nl,c);
						novo->tab[l][c]=0;
						novo->tab[nl][c]=peca;

						pos rei = novo->reiP1;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
						nl++;
					}
					if((nl<8)&&(e->tab[nl][c]>16))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->damaP1,nl,c);
						//pos adversario = getPos(novo,novo->tab[nl][c]);
						setPos(getPosA(novo,novo->tab[nl][c]),-1,-1);
						novo->tab[l][c]=0;
						novo->tab[nl][c]=peca;

						pos rei = novo->reiP1;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
					}
					nl=l-1;
					while((nl>-1)&&(e->tab[nl][c]==0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->damaP1,nl,c);
						novo->tab[l][c]=0;
						novo->tab[nl][c]=peca;

						pos rei = novo->reiP1;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
						nl--;
					}

					if((nl>-1)&&(e->tab[nl][c]>16))
					{
						estado *novo = constroiEstadoApartirDe(e);
						//pos adversario = getPos(novo,novo->tab[nl][c]);
						setPos(getPosA(novo,novo->tab[nl][c]),-1,-1);
				
						setPos(&novo->damaP1,nl,c);
						novo->tab[l][c]=0;
						novo->tab[nl][c]=peca;

						pos rei = novo->reiP1;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
					}

					nc=c+1;
					while((nc<8)&&(e->tab[l][nc]==0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->damaP1,l,nc);
						novo->tab[l][c]=0;
						novo->tab[l][nc]=peca;

						pos rei = novo->reiP1;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
						nc++;
					}
					if((nc<8)&&(e->tab[l][nc]>16))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->damaP1,l,nc);
						//pos adversario = getPos(novo,novo->tab[l][nc]);
						setPos(getPosA(novo,novo->tab[l][nc]),-1,-1);
						novo->tab[l][c]=0;
						novo->tab[l][nc]=peca;

						pos rei = novo->reiP1;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
					}

					nc=c-1;
					while((nc>-1)&&(e->tab[l][nc]==0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->damaP1,l,nc);
						novo->tab[l][c]=0;
						novo->tab[l][nc]=peca;

						pos rei = novo->reiP1;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
						nc--;
					}
					if((nc>-1)&&(e->tab[l][nc]>16))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->damaP1,l,nc);
						//pos adversario = getPos(novo,novo->tab[l][nc]);
						setPos(getPosA(novo,novo->tab[l][nc]),-1,-1);
						novo->tab[l][c]=0;
						novo->tab[l][nc]=peca;

						pos rei = novo->reiP1;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
					}
		
				}
			}
			case 3:	//Movimento de torre
			{
				pos torrePos = getPos(e,peca);
				if((torrePos.l!=-1)&&(peca==3))
				{
					int nl=l;
					nl=l+1;
					while((nl<8)&&(e->tab[nl][c]==0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->torre1P1,nl,c);
						novo->tab[l][c]=0;
						novo->tab[nl][c]=peca;
						novo->torre1P1Mexeu=1;
						pos rei = novo->reiP1;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
						nl++;
					}
					if((nl<8)&&(e->tab[nl][c]>16))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->torre1P1,nl,c);
						//pos adversario = getPos(novo,novo->tab[nl][c]);
						setPos(getPosA(novo,novo->tab[nl][c]),-1,-1);
						novo->tab[l][c]=0;
						novo->tab[nl][c]=peca;
						novo->torre1P1Mexeu=1;
						pos rei = novo->reiP1;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
					}
					nl=l-1;
					while((nl>-1)&&(e->tab[nl][c]==0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->torre1P1,nl,c);
						novo->tab[l][c]=0;
						novo->tab[nl][c]=peca;
						novo->torre1P1Mexeu=1;
						pos rei = novo->reiP1;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
						nl--;
					}

					if((nl>-1)&&(e->tab[nl][c]>16))
					{
						estado *novo = constroiEstadoApartirDe(e);
						//pos adversario = getPos(novo,novo->tab[nl][c]);

						
						setPos(getPosA(novo,novo->tab[nl][c]),-1,-1);
				
						setPos(&novo->torre1P1,nl,c);
						novo->tab[l][c]=0;
						novo->tab[nl][c]=peca;
						novo->torre1P1Mexeu=1;
						pos rei = novo->reiP1;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
					}

					int nc=c;
					nc=c+1;
					while((nc<8)&&(e->tab[l][nc]==0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->torre1P1,l,nc);
						novo->tab[l][c]=0;
						novo->tab[l][nc]=peca;
						novo->torre1P1Mexeu=1;
						pos rei = novo->reiP1;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
						nc++;
					}
					if((nc<8)&&(e->tab[l][nc]>16))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->torre1P1,l,nc);
						//pos adversario = getPos(novo,novo->tab[l][nc]);
						setPos(getPosA(novo,novo->tab[l][nc]),-1,-1);
						novo->tab[l][c]=0;
						novo->tab[l][nc]=peca;
						novo->torre1P1Mexeu=1;
						pos rei = novo->reiP1;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
					}

					nc=c-1;
					while((nc>-1)&&(e->tab[l][nc]==0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->torre1P1,l,nc);
						novo->tab[l][c]=0;
						novo->tab[l][nc]=peca;
						novo->torre1P1Mexeu=1;
						pos rei = novo->reiP1;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
						nc--;
					}
					if((nc>-1)&&(e->tab[l][nc]>16))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->torre1P1,l,nc);
						//pos adversario = getPos(novo,novo->tab[l][nc]);
						setPos(getPosA(novo,novo->tab[l][nc]),-1,-1);
						novo->tab[l][c]=0;
						novo->tab[l][nc]=peca;
						novo->torre1P1Mexeu=1;
						pos rei = novo->reiP1;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
					}

					

				}
			}
			case 4:	//Movimento de torre
			{
				pos torrePos = getPos(e,peca);
				if((torrePos.l!=-1)&&(peca==4))
				{
					int nl=l;
					nl=l+1;
					while((nl<8)&&(e->tab[nl][c]==0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->torre2P1,nl,c);
						novo->tab[l][c]=0;
						novo->tab[nl][c]=peca;
						novo->torre2P1Mexeu=1;
						pos rei = novo->reiP1;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
						nl++;
					}
					if((nl<8)&&(e->tab[nl][c]>16))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->torre2P1,nl,c);
						//pos adversario = getPos(novo,novo->tab[nl][c]);
						setPos(getPosA(novo,novo->tab[nl][c]),-1,-1);
						novo->tab[l][c]=0;
						novo->tab[nl][c]=peca;
						novo->torre2P1Mexeu=1;
						pos rei = novo->reiP1;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
					}
					nl=l-1;
					while((nl>-1)&&(e->tab[nl][c]==0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->torre2P1,nl,c);
						novo->tab[l][c]=0;
						novo->tab[nl][c]=peca;
						novo->torre2P1Mexeu=1;
						pos rei = novo->reiP1;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
						nl--;
					}

					if((nl>-1)&&(e->tab[nl][c]>16))
					{
						estado *novo = constroiEstadoApartirDe(e);
						//pos adversario = getPos(novo,novo->tab[nl][c]);

						
						setPos(getPosA(novo,novo->tab[nl][c]),-1,-1);
				
						setPos(&novo->torre2P1,nl,c);
						novo->tab[l][c]=0;
						novo->tab[nl][c]=peca;
						novo->torre2P1Mexeu=1;
						pos rei = novo->reiP1;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
					}

					int nc=c;
					nc=c+1;
					while((nc<8)&&(e->tab[l][nc]==0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->torre2P1,l,nc);
						novo->tab[l][c]=0;
						novo->tab[l][nc]=peca;
						novo->torre2P1Mexeu=1;
						pos rei = novo->reiP1;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
						nc++;
					}
					if((nc<8)&&(e->tab[l][nc]>16))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->torre2P1,l,nc);
						//pos adversario = getPos(novo,novo->tab[l][nc]);
						setPos(getPosA(novo,novo->tab[l][nc]),-1,-1);
						novo->tab[l][c]=0;
						novo->tab[l][nc]=peca;
						novo->torre2P1Mexeu=1;
						pos rei = novo->reiP1;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
					}

					nc=c-1;
					while((nc>-1)&&(e->tab[l][nc]==0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->torre2P1,l,nc);
						novo->tab[l][c]=0;
						novo->tab[l][nc]=peca;
						novo->torre2P1Mexeu=1;
						pos rei = novo->reiP1;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
						nc--;
					}
					if((nc>-1)&&(e->tab[l][nc]>16))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->torre2P1,l,nc);
						//pos adversario = getPos(novo,novo->tab[l][nc]);
						setPos(getPosA(novo,novo->tab[l][nc]),-1,-1);
						novo->tab[l][c]=0;
						novo->tab[l][nc]=peca;
						novo->torre2P1Mexeu=1;
						pos rei = novo->reiP1;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
					}
				}
			}
			case 5:	//Movimento de Cavalo
			{
				pos cavaloPos = getPos(e,peca);
				if((cavaloPos.l!=-1)&&(peca==5))
				{
					
					int nl=l+2;
					int nc=c-1;
					if((nl<8)&&(nc>=0)&&(e->tab[nl][nc]==0))
					{
						
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->cavalo1P1,nl,nc);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=peca;

						pos rei = novo->reiP1;

						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
						{	insereLista(e->filhos,novo);}
						else
							free(novo);
						
						
					}
					
					if((nl<8)&&(nc>=0)&&(e->tab[nl][nc]>16))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->cavalo1P1,nl,nc);
						
						setPos(getPosA(novo,novo->tab[nl][nc]),-1,-1);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=peca;

						pos rei = novo->reiP1;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
					}
					nl=l+2;
					nc=c+1;
					if((nl<8)&&(nc<8)&&(e->tab[nl][nc]==0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->cavalo1P1,nl,nc);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=peca;

						pos rei = novo->reiP1;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);

					}
					if((nl<8)&&(nc<8)&&(e->tab[nl][nc]>16))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->cavalo1P1,nl,nc);
						
						setPos(getPosA(novo,novo->tab[nl][nc]),-1,-1);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=peca;

						pos rei = novo->reiP1;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
					}
					nl=l-2;
					nc=c+1;
					if((nl>=0)&&(nc<8)&&(e->tab[nl][nc]==0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->cavalo1P1,nl,nc);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=peca;

						pos rei = novo->reiP1;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);

					}
					if((nl>=0)&&(nc<8)&&(e->tab[nl][nc]>16))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->cavalo1P1,nl,nc);
						
						setPos(getPosA(novo,novo->tab[nl][nc]),-1,-1);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=peca;

						pos rei = novo->reiP1;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
					}
					nl=l-2;
					nc=c-1;
					if((nl>=0)&&(nc>=0)&&(e->tab[nl][nc]==0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->cavalo1P1,nl,nc);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=peca;

						pos rei = novo->reiP1;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);

					}
					if((nl>=0)&&(nc>=0)&&(e->tab[nl][nc]>16))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->cavalo1P1,nl,nc);
						
						setPos(getPosA(novo,novo->tab[nl][nc]),-1,-1);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=peca;

						pos rei = novo->reiP1;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
					}
					nl=l+1;
					nc=c-2;
					if((nl<8)&&(nc>=0)&&(e->tab[nl][nc]==0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->cavalo1P1,nl,nc);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=peca;

						pos rei = novo->reiP1;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);

					}
					if((nl<8)&&(nc>=0)&&(e->tab[nl][nc]>16))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->cavalo1P1,nl,nc);
						
						setPos(getPosA(novo,novo->tab[nl][nc]),-1,-1);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=peca;

						pos rei = novo->reiP1;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
					}
					nl=l-1;
					nc=c-2;
					if((nl>=0)&&(nc>=0)&&(e->tab[nl][nc]==0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->cavalo1P1,nl,nc);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=peca;

						pos rei = novo->reiP1;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);

					}
					if((nl>=0)&&(nc>=0)&&(e->tab[nl][nc]>16))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->cavalo1P1,nl,nc);
						
						setPos(getPosA(novo,novo->tab[nl][nc]),-1,-1);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=peca;

						pos rei = novo->reiP1;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
					}
					nl=l+1;
					nc=c+2;
					if((nl<8)&&(nc<8)&&(e->tab[nl][nc]==0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->cavalo1P1,nl,nc);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=peca;

						pos rei = novo->reiP1;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);

					}
					if((nl<8)&&(nc<8)&&(e->tab[nl][nc]>16))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->cavalo1P1,nl,nc);
						
						setPos(getPosA(novo,novo->tab[nl][nc]),-1,-1);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=peca;

						pos rei = novo->reiP1;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
					}
					nl=l-1;
					nc=c+2;
					if((nl>=0)&&(nc<8)&&(e->tab[nl][nc]==0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->cavalo1P1,nl,nc);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=peca;

						pos rei = novo->reiP1;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);

					}
					if((nl>=0)&&(nc<8)&&(e->tab[nl][nc]>16))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->cavalo1P1,nl,nc);
						
						setPos(getPosA(novo,novo->tab[nl][nc]),-1,-1);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=peca;

						pos rei = novo->reiP1;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
					}
				}
			}
			case 6:	//Movimento de Cavalo 2
			{
				pos cavaloPos = getPos(e,peca);
				if((cavaloPos.l!=-1)&&(peca==6))
				{
					int nl=l+2;
					int nc=c-1;
					if((nl<8)&&(nc>=0)&&(e->tab[nl][nc]==0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->cavalo2P1,nl,nc);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=peca;

						pos rei = novo->reiP1;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);

					}
					if((nl<8)&&(nc>=0)&&(e->tab[nl][nc]>16))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->cavalo2P1,nl,nc);
						
						setPos(getPosA(novo,novo->tab[nl][nc]),-1,-1);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=peca;

						pos rei = novo->reiP1;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
					}
					nl=l+2;
					nc=c+1;
					if((nl<8)&&(nc<8)&&(e->tab[nl][nc]==0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->cavalo2P1,nl,nc);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=peca;

						pos rei = novo->reiP1;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);

					}
					if((nl<8)&&(nc<8)&&(e->tab[nl][nc]>16))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->cavalo2P1,nl,nc);
						
						setPos(getPosA(novo,novo->tab[nl][nc]),-1,-1);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=peca;

						pos rei = novo->reiP1;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
					}
					nl=l-2;
					nc=c+1;
					if((nl>=0)&&(nc<8)&&(e->tab[nl][nc]==0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->cavalo2P1,nl,nc);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=peca;

						pos rei = novo->reiP1;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);

					}
					if((nl>=0)&&(nc<8)&&(e->tab[nl][nc]>16))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->cavalo2P1,nl,nc);
						
						setPos(getPosA(novo,novo->tab[nl][nc]),-1,-1);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=peca;

						pos rei = novo->reiP1;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
					}
					nl=l-2;
					nc=c-1;
					if((nl>=0)&&(nc>=0)&&(e->tab[nl][nc]==0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->cavalo2P1,nl,nc);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=peca;

						pos rei = novo->reiP1;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);

					}
					if((nl>=0)&&(nc>=0)&&(e->tab[nl][nc]>16))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->cavalo2P1,nl,nc);
						
						setPos(getPosA(novo,novo->tab[nl][nc]),-1,-1);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=peca;

						pos rei = novo->reiP1;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
					}
					nl=l+1;
					nc=c-2;
					if((nl<8)&&(nc>=0)&&(e->tab[nl][nc]==0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->cavalo2P1,nl,nc);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=peca;

						pos rei = novo->reiP1;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);

					}
					if((nl<8)&&(nc>=0)&&(e->tab[nl][nc]>16))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->cavalo2P1,nl,nc);
						
						setPos(getPosA(novo,novo->tab[nl][nc]),-1,-1);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=peca;

						pos rei = novo->reiP1;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
					}
					nl=l-1;
					nc=c-2;
					if((nl>=0)&&(nc>=0)&&(e->tab[nl][nc]==0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->cavalo2P1,nl,nc);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=peca;

						pos rei = novo->reiP1;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);

					}
					if((nl>=0)&&(nc>=0)&&(e->tab[nl][nc]>16))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->cavalo2P1,nl,nc);
						
						setPos(getPosA(novo,novo->tab[nl][nc]),-1,-1);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=peca;

						pos rei = novo->reiP1;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
					}
					nl=l+1;
					nc=c+2;
					if((nl<8)&&(nc<8)&&(e->tab[nl][nc]==0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->cavalo2P1,nl,nc);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=peca;

						pos rei = novo->reiP1;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);

					}
					if((nl<8)&&(nc<8)&&(e->tab[nl][nc]>16))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->cavalo2P1,nl,nc);
						
						setPos(getPosA(novo,novo->tab[nl][nc]),-1,-1);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=peca;

						pos rei = novo->reiP1;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
					}
					nl=l-1;
					nc=c+2;
					if((nl>=0)&&(nc<8)&&(e->tab[nl][nc]==0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->cavalo2P1,nl,nc);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=peca;

						pos rei = novo->reiP1;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);

					}
					if((nl>=0)&&(nc<8)&&(e->tab[nl][nc]>16))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->cavalo2P1,nl,nc);
						
						setPos(getPosA(novo,novo->tab[nl][nc]),-1,-1);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=peca;

						pos rei = novo->reiP1;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
					}
				}
			}
			case 7:	//Movimento de  bispo1
			{
				pos bispoPos = getPos(e,peca);
				int nl;
				int nc;
				if((bispoPos.l!=-1)&&(peca==7))
				{
					nl=l+1;
					nc=c+1;
					while((nl<8)&&(nc<8)&&(e->tab[nl][nc]==0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->bispo1P1,nl,nc);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=peca;

						pos rei = novo->reiP1;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
						nl++;nc++;
					}
					if((nl<8)&&(nc<8)&&(e->tab[nl][nc]>16))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->bispo1P1,nl,nc);
						//pos adversario = getPos(novo,novo->tab[nl][c]);
						setPos(getPosA(novo,novo->tab[nl][nc]),-1,-1);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=peca;

						pos rei = novo->reiP1;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
					}
					nl=l+1;
					nc=c-1;
					while((nl<8)&&(nc>=0)&&(e->tab[nl][nc]==0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->bispo1P1,nl,nc);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=peca;

						pos rei = novo->reiP1;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
						nl++;nc--;
					}
					if((nl<8)&&(nc>=0)&&(e->tab[nl][nc]>16))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->bispo1P1,nl,nc);
						//pos adversario = getPos(novo,novo->tab[nl][c]);
						setPos(getPosA(novo,novo->tab[nl][nc]),-1,-1);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=peca;

						pos rei = novo->reiP1;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
					}
					nl=l-1;
					nc=c-1;
					while((nl>=0)&&(nc>=0)&&(e->tab[nl][nc]==0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->bispo1P1,nl,nc);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=peca;

						pos rei = novo->reiP1;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
						nl--;nc--;
					}
					if((nl>=0)&&(nc>=0)&&(e->tab[nl][nc]>16))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->bispo1P1,nl,nc);
						//pos adversario = getPos(novo,novo->tab[nl][c]);
						setPos(getPosA(novo,novo->tab[nl][nc]),-1,-1);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=peca;

						pos rei = novo->reiP1;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
					}
					nl=l-1;
					nc=c+1;
					while((nl>=0)&&(nc<8)&&(e->tab[nl][nc]==0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->bispo1P1,nl,nc);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=peca;

						pos rei = novo->reiP1;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
						nl--;nc++;
					}
					if((nl>=0)&&(nc<8)&&(e->tab[nl][nc]>16))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->bispo1P1,nl,nc);
						//pos adversario = getPos(novo,novo->tab[nl][c]);
						setPos(getPosA(novo,novo->tab[nl][nc]),-1,-1);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=peca;

						pos rei = novo->reiP1;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
					}		
				}
			}
			case 8:	//Movimento de  bispo2
			{
				pos bispoPos = getPos(e,peca);
				int nl;
				int nc;
				if((bispoPos.l!=-1)&&(peca==8))
				{
					nl=l+1;
					nc=c+1;
					while((nl<8)&&(nc<8)&&(e->tab[nl][nc]==0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->bispo2P1,nl,nc);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=peca;

						pos rei = novo->reiP1;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
						nl++;nc++;
					}
					if((nl<8)&&(nc<8)&&(e->tab[nl][nc]>16))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->bispo2P1,nl,nc);
						//pos adversario = getPos(novo,novo->tab[nl][c]);
						setPos(getPosA(novo,novo->tab[nl][nc]),-1,-1);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=peca;

						pos rei = novo->reiP1;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
					}
					nl=l+1;
					nc=c-1;
					while((nl<8)&&(nc>=0)&&(e->tab[nl][nc]==0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->bispo2P1,nl,nc);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=peca;

						pos rei = novo->reiP1;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
						nl++;nc--;
					}
					if((nl<8)&&(nc>=0)&&(e->tab[nl][nc]>16))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->bispo2P1,nl,nc);
						//pos adversario = getPos(novo,novo->tab[nl][c]);
						setPos(getPosA(novo,novo->tab[nl][nc]),-1,-1);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=peca;

						pos rei = novo->reiP1;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
					}
					nl=l-1;
					nc=c-1;
					while((nl>=0)&&(nc>=0)&&(e->tab[nl][nc]==0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->bispo2P1,nl,nc);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=peca;

						pos rei = novo->reiP1;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
						nl--;nc--;
					}
					if((nl>=0)&&(nc>=0)&&(e->tab[nl][nc]>16))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->bispo2P1,nl,nc);
						//pos adversario = getPos(novo,novo->tab[nl][c]);
						setPos(getPosA(novo,novo->tab[nl][nc]),-1,-1);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=peca;

						pos rei = novo->reiP1;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
					}
					nl=l-1;
					nc=c+1;
					while((nl>=0)&&(nc<8)&&(e->tab[nl][nc]==0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->bispo2P1,nl,nc);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=peca;

						pos rei = novo->reiP1;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
						nl--;nc++;
					}
					if((nl>=0)&&(nc<8)&&(e->tab[nl][nc]>16))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->bispo2P1,nl,nc);
						//pos adversario = getPos(novo,novo->tab[nl][c]);
						setPos(getPosA(novo,novo->tab[nl][nc]),-1,-1);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=peca;

						pos rei = novo->reiP1;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
					}		
				}
			}
			//case '9':
			default :  //movimento de peoes
			{
				int posic = peca-9;
				pos peaoPos = getPos(e,peca);
				if((peca>8)&&(peca<17)&&(peaoPos.l!=-1))
				{
					int nl=l+1;
					if((nl<8)&&(e->tab[nl][c]==0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->peaoP1[posic],nl,c);
						novo->tab[l][c]=0;
						novo->tab[nl][c]=peca;
						if(nl==7)
							promovePeaoP1(novo,posic);
						pos rei = novo->reiP1;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);		
					}

					nl=l+2;
					if((l==1)&&(e->tab[nl][c]==0)&&(e->tab[nl-1][c]==0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->peaoP1[posic],nl,c);
						novo->tab[l][c]=0;
						novo->tab[nl][c]=peca;
						pos rei = novo->reiP1;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);		
					}
										
					nl=l+1;
					int nc=c+1;
					if((nl<8)&&(nc<8)&&(e->tab[nl][nc]>16))
					{

						estado *novo = constroiEstadoApartirDe(e);
						//pos adversario = getPos(novo,novo->tab[nl][nc]);
						setPos(getPosA(novo,novo->tab[nl][nc]),-1,-1);
						setPos(&novo->peaoP1[posic],nl,nc);						
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=peca;
						if(nl==7)
							promovePeaoP1(novo,posic);
						pos rei = novo->reiP1;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);		
					}
	
					nl=l+1;
					nc=c-1;
					if((nl<8)&&(nc>-1)&&(e->tab[nl][nc]>16))
					{

						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->peaoP1[posic],nl,nc);
						//pos adversario = getPos(novo,novo->tab[nl][nc]);
						setPos(getPosA(novo,novo->tab[nl][nc]),-1,-1);
						setPos(&novo->peaoP1[posic],nl,nc);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=peca;
						if(nl==7)
							promovePeaoP1(novo,posic);
						pos rei = novo->reiP1;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);		
					}					
				}
			}
				
		}	
	}	
	else	
	{

		peca = peca-16;
		//printf("Peca = %d\n",peca);
		//int k;
		//scanf("%d",&k);
		switch(peca)
		{	
			
			//printf("Adicionando filho para Jogador = %d , peca = %d\n", jogador,peca);
			
			case 1:
			{	
				if((!e->reiP2Mexeu)&&(!e->torre1P2Mexeu))
				{
					if((posicaoNaoAtacada(l,c,e,jogador))&&
					(posicaoNaoAtacada(7,0,e,jogador)))
					{
						if((posicaoNaoAtacada(7,2,e,jogador))&&
						(posicaoNaoAtacada(7,1,e,jogador)))
						{
							if((e->tab[7][2]==0)&&(e->tab[7][1]==0))
							{
								estado *novo = constroiEstadoApartirDe(e);
								setPos(&novo->reiP2,7,1);
								setPos(&novo->torre1P2,7,2);
								novo->tab[l][c]=0;
								novo->tab[7][1]=17;
								novo->tab[7][0]=0;
								novo->tab[7][2]=19;
								novo->reiP2Mexeu=1;
								novo->torre1P2Mexeu=1;
								insereLista(e->filhos,novo);
							}
						
						}
						
					}
				}

				if((!e->reiP2Mexeu)&&(!e->torre2P2Mexeu))
				{
					if((posicaoNaoAtacada(l,c,e,jogador))&&
					(posicaoNaoAtacada(7,7,e,jogador)))
					{
						if((posicaoNaoAtacada(7,4,e,jogador))&&
						(posicaoNaoAtacada(7,5,e,jogador)))
						{
							if((e->tab[7][4]==0)&&(e->tab[7][5]==0)&&(e->tab[7][6]==0))
							{
								estado *novo = constroiEstadoApartirDe(e);
								setPos(&novo->reiP2,7,5);
								setPos(&novo->torre2P2,7,4);
								novo->tab[l][c]=0;
								novo->tab[7][5]=17;
								novo->tab[7][7]=0;
								novo->tab[7][4]=20;
								novo->reiP2Mexeu=1;
								novo->torre2P2Mexeu=1;
								insereLista(e->filhos,novo);
							}
						
						}
						
					}
				}
				int nl=l+1;
				if( (nl<8)&&( (e->tab[nl][c]==0) || (e->tab[nl][c]<=16) ) )
				{
					if(e->tab[nl][c]==0)
					{
						estado *novo = constroiEstadoApartirDe(e);

						setPos(&novo->reiP2,nl,c);
						novo->tab[l][c]=0;
						novo->tab[nl][c]=17;
						novo->reiP2Mexeu=1;
						if(posicaoNaoAtacada(nl,c,novo,jogador))
							insereLista(e->filhos,novo);
						else free(novo);
					}
					else
					{
						estado *novo = constroiEstadoApartirDe(e);
						//pos adversario = getPos(novo,novo->tab[nl][c]);
						setPos(getPosA(novo,novo->tab[nl][c]),-1,-1);
						setPos(&novo->reiP2,nl,c);
						novo->tab[l][c]=0;
						novo->tab[nl][c]=17;
						novo->reiP2Mexeu=1;
						if(posicaoNaoAtacada(nl,c,novo,jogador))
							insereLista(e->filhos,novo);
						else free(novo);
					}
				}
				nl=l-1;
				if( (nl>-1)&&( (e->tab[nl][c]==0) || (e->tab[nl][c]<=16) ))
				{
					if(e->tab[nl][c]==0)
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->reiP2,nl,c);
						novo->tab[l][c]=0;
						novo->tab[nl][c]=17;
						novo->reiP2Mexeu=1;
						if(posicaoNaoAtacada(nl,c,novo,jogador)) 
							insereLista(e->filhos,novo);
						else free(novo);
					}
					else
					{
						estado *novo = constroiEstadoApartirDe(e);
						//pos adversario = getPos(novo,novo->tab[nl][c]);
						setPos(getPosA(novo,novo->tab[nl][c]),-1,-1);
						setPos(&novo->reiP2,nl,c);
						novo->tab[l][c]=0;
						novo->tab[nl][c]=17;
						novo->reiP2Mexeu=1;
						if(posicaoNaoAtacada(nl,c,novo,jogador)) 
							insereLista(e->filhos,novo);
						else free(novo);
					}
				}
				int nc = c+1;
				if( (nc<8)&&( (e->tab[l][nc]==0) || (e->tab[l][nc]<=16) ) )
				{
					if(e->tab[l][nc]==0)
					{
						estado *novo = constroiEstadoApartirDe(e);

						setPos(&novo->reiP2,l,nc);
						novo->tab[l][c]=0;
						novo->tab[l][nc]=17;
						novo->reiP2Mexeu=1;
						if(posicaoNaoAtacada(l,nc,novo,jogador))
							insereLista(e->filhos,novo);
						else free(novo);
					}
					else
					{
						estado *novo = constroiEstadoApartirDe(e);
						//pos adversario = getPos(novo,novo->tab[l][nc]);
						setPos(getPosA(novo,novo->tab[l][nc]),-1,-1);
						setPos(&novo->reiP2,l,nc);
						novo->tab[l][c]=0;
						novo->tab[l][nc]=17;
						novo->reiP2Mexeu=1;
						if(posicaoNaoAtacada(l,nc,novo,jogador))
							insereLista(e->filhos,novo);
						else free(novo);
					}
				}
				nc=c-1;
				if( (nc>-1)&&( (e->tab[l][nc]==0) || (e->tab[l][nc]<=16) ) )
				{
					if(e->tab[l][nc]==0)
					{
						estado *novo = constroiEstadoApartirDe(e);

						setPos(&novo->reiP2,l,nc);
						novo->tab[l][c]=0;
						novo->tab[l][nc]=17;
						novo->reiP2Mexeu=1;
						if(posicaoNaoAtacada(l,nc,novo,jogador))
							insereLista(e->filhos,novo);
						else free(novo);
					}
					else
					{
						estado *novo = constroiEstadoApartirDe(e);
						//pos adversario = getPos(novo,novo->tab[l][nc]);
						setPos(getPosA(novo,novo->tab[l][nc]),-1,-1);
						setPos(&novo->reiP2,l,nc);
						novo->tab[l][c]=0;
						novo->tab[l][nc]=17;
						novo->reiP2Mexeu=1;
						if(posicaoNaoAtacada(l,nc,novo,jogador))
							insereLista(e->filhos,novo);
						else free(novo);
					}
				}
				nl=l-1;
				nc=c-1;	
				if( (nc>-1)&&(nl>-1)&&( (e->tab[nl][nc]==0) || (e->tab[nl][nc]<=16) ) )
				{
					if(e->tab[nl][nc]==0)
					{
						estado *novo = constroiEstadoApartirDe(e);

						setPos(&novo->reiP2,nl,nc);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=17;
						novo->reiP2Mexeu=1;
						if(posicaoNaoAtacada(nl,nc,novo,jogador))
							insereLista(e->filhos,novo);
						else free(novo);
					}
					else
					{
						estado *novo = constroiEstadoApartirDe(e);
						//pos adversario = getPos(novo,novo->tab[nl][nc]);
						setPos(getPosA(novo,novo->tab[nl][nc]),-1,-1);
						setPos(&novo->reiP2,nl,nc);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=17;
						novo->reiP2Mexeu=1;
						if(posicaoNaoAtacada(nl,nc,novo,jogador))
							insereLista(e->filhos,novo);
						else free(novo);
					}	
				}
				nl=l+1;
				nc=c-1;	
				if( (nc>-1)&&(nl<8)&&( (e->tab[nl][nc]==0) || (e->tab[nl][nc]<=16) ) )
				{
					if(e->tab[nl][nc]==0)
					{
						estado *novo = constroiEstadoApartirDe(e);

						setPos(&novo->reiP2,nl,nc);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=17;
						novo->reiP2Mexeu=1;
						if(posicaoNaoAtacada(nl,nc,novo,jogador))
							insereLista(e->filhos,novo);
						else free(novo);
					}
					else
					{
						estado *novo = constroiEstadoApartirDe(e);
						//pos adversario = getPos(novo,novo->tab[nl][nc]);
						setPos(getPosA(novo,novo->tab[nl][nc]),-1,-1);
						setPos(&novo->reiP2,nl,nc);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=17;
						novo->reiP2Mexeu=1;
						if(posicaoNaoAtacada(nl,nc,novo,jogador))
							insereLista(e->filhos,novo);
						else free(novo);
					}	
				}
				nl=l-1;
				nc=c+1;	
				if( (nc<8)&&(nl>-1)&&( (e->tab[nl][nc]==0) || (e->tab[nl][nc]<=16) ) )
				{
					if(e->tab[nl][nc]==0)
					{
						estado *novo = constroiEstadoApartirDe(e);

						setPos(&novo->reiP2,nl,nc);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=17;
						novo->reiP2Mexeu=1;
						if(posicaoNaoAtacada(nl,nc,novo,jogador))
							insereLista(e->filhos,novo);
						else free(novo);
					}
					else
					{
						estado *novo = constroiEstadoApartirDe(e);
						//pos adversario = getPos(novo,novo->tab[nl][nc]);
						setPos(getPosA(novo,novo->tab[nl][nc]),-1,-1);
						setPos(&novo->reiP2,nl,nc);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=17;
						novo->reiP2Mexeu=1;
						if(posicaoNaoAtacada(nl,nc,novo,jogador))
							insereLista(e->filhos,novo);
						else free(novo);
					}	
				}
				nl=l+1;
				nc=c+1;	
				if( (nc<8)&&(nl<8)&&( (e->tab[nl][nc]==0) || (e->tab[nl][nc]<=16) ) )
				{
					if(e->tab[nl][nc]==0)
					{
						estado *novo = constroiEstadoApartirDe(e);

						setPos(&novo->reiP2,nl,nc);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=17;
						novo->reiP2Mexeu=1;
						if(posicaoNaoAtacada(nl,nc,novo,jogador))
							insereLista(e->filhos,novo);
						else free(novo);
					}
					else
					{
						estado *novo = constroiEstadoApartirDe(e);
						//pos adversario = getPos(novo,novo->tab[nl][nc]);
						setPos(getPosA(novo,novo->tab[nl][nc]),-1,-1);
						setPos(&novo->reiP2,nl,nc);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=17;
						novo->reiP2Mexeu=1;
						if(posicaoNaoAtacada(nl,nc,novo,jogador))
							insereLista(e->filhos,novo);
						else free(novo);
					}	
				}							 
			}
			case 2:	//Movimento de  damaP2
			{
				pos damaPos = getPos(e,peca+16);
				int nl;
				int nc;
				if((damaPos.l!=-1)&&(peca==2))
				{
					nl=l+1;
					nc=c+1;
					while((nl<8)&&(nc<8)&&(e->tab[nl][nc]==0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->damaP2,nl,nc);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=18;

						pos rei = novo->reiP2;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
						nl++;nc++;
					}
					if((nl<8)&&(nc<8)&&(e->tab[nl][nc]<=16)&&(e->tab[nl][nc]>0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->damaP2,nl,nc);
						//pos adversario = getPos(novo,novo->tab[nl][c]);
						setPos(getPosA(novo,novo->tab[nl][nc]),-1,-1);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=18;

						pos rei = novo->reiP2;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
					}
					nl=l+1;
					nc=c-1;
					while((nl<8)&&(nc>=0)&&(e->tab[nl][nc]==0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->damaP2,nl,nc);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=18;

						pos rei = novo->reiP2;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
						nl++;nc--;
					}
					if((nl<8)&&(nc>=0)&&(e->tab[nl][nc]<=16)&&(e->tab[nl][nc]>0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->damaP2,nl,nc);
						//pos adversario = getPos(novo,novo->tab[nl][c]);
						setPos(getPosA(novo,novo->tab[nl][nc]),-1,-1);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=18;

						pos rei = novo->reiP2;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
					}
					nl=l-1;
					nc=c-1;
					while((nl>=0)&&(nc>=0)&&(e->tab[nl][nc]==0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->damaP2,nl,nc);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=18;

						pos rei = novo->reiP2;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
						nl--;nc--;
					}
					if((nl>=0)&&(nc>=0)&&(e->tab[nl][nc]<=16)&&(e->tab[nl][nc]>0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->damaP2,nl,nc);
						//pos adversario = getPos(novo,novo->tab[nl][c]);
						setPos(getPosA(novo,novo->tab[nl][nc]),-1,-1);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=18;

						pos rei = novo->reiP2;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
					}
					nl=l-1;
					nc=c+1;
					while((nl>=0)&&(nc<8)&&(e->tab[nl][nc]==0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->damaP2,nl,nc);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=18;

						pos rei = novo->reiP2;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
						nl--;nc++;
					}
					if((nl>=0)&&(nc<8)&&(e->tab[nl][nc]<=16)&&(e->tab[nl][nc]>0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->damaP2,nl,nc);
						//pos adversario = getPos(novo,novo->tab[nl][c]);
						setPos(getPosA(novo,novo->tab[nl][nc]),-1,-1);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=18;

						pos rei = novo->reiP2;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
					}
					nl=l+1;
					while((nl<8)&&(e->tab[nl][c]==0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->damaP2,nl,c);
						novo->tab[l][c]=0;
						novo->tab[nl][c]=18;

						pos rei = novo->reiP2;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
						nl++;
					}
					if((nl<8)&&(e->tab[nl][c]<=16)&&(e->tab[nl][c]>0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->damaP2,nl,c);
						//pos adversario = getPos(novo,novo->tab[nl][c]);
						setPos(getPosA(novo,novo->tab[nl][c]),-1,-1);
						novo->tab[l][c]=0;
						novo->tab[nl][c]=18;

						pos rei = novo->reiP2;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
					}
					nl=l-1;
					while((nl>-1)&&(e->tab[nl][c]==0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->damaP2,nl,c);
						novo->tab[l][c]=0;
						novo->tab[nl][c]=18;

						pos rei = novo->reiP2;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
						nl--;
					}

					if((nl>-1)&&(e->tab[nl][c]<=16)&&(e->tab[nl][c]>0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						//pos adversario = getPos(novo,novo->tab[nl][c]);
						setPos(getPosA(novo,novo->tab[nl][c]),-1,-1);
				
						setPos(&novo->damaP2,nl,c);
						novo->tab[l][c]=0;
						novo->tab[nl][c]=18;

						pos rei = novo->reiP2;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
					}

					nc=c+1;
					while((nc<8)&&(e->tab[l][nc]==0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->damaP2,l,nc);
						novo->tab[l][c]=0;
						novo->tab[l][nc]=18;

						pos rei = novo->reiP2;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
						nc++;
					}
					if((nc<8)&&(e->tab[l][nc]<=16)&&(e->tab[l][nc]>0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->damaP2,l,nc);
						//pos adversario = getPos(novo,novo->tab[l][nc]);
						setPos(getPosA(novo,novo->tab[l][nc]),-1,-1);
						novo->tab[l][c]=0;
						novo->tab[l][nc]=18;

						pos rei = novo->reiP2;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
					}

					nc=c-1;
					while((nc>-1)&&(e->tab[l][nc]==0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->damaP2,l,nc);
						novo->tab[l][c]=0;
						novo->tab[l][nc]=18;

						pos rei = novo->reiP2;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
						nc--;
					}
					if((nc>-1)&&(e->tab[l][nc]<=16)&&(e->tab[l][nc]>0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->damaP2,l,nc);
						//pos adversario = getPos(novo,novo->tab[l][nc]);
						setPos(getPosA(novo,novo->tab[l][nc]),-1,-1);
						novo->tab[l][c]=0;
						novo->tab[l][nc]=18;

						pos rei = novo->reiP2;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
					}
		
				}
			}
			case 3:	//Movimento de torre
			{
				pos torrePos = getPos(e,peca+16);
				if((torrePos.l!=-1)&&(peca==3))
				{
					//printf("l=%d c=%d\n",l,c);
					int nl=l;
					nl=l+1;
					while((nl<8)&&(e->tab[nl][c]==0))
					{
						//printf("Achou1\n");
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->torre1P2,nl,c);
						novo->tab[l][c]=0;
						novo->tab[nl][c]=19;
						novo->torre1P2Mexeu=1;
						pos rei = novo->reiP2;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
						nl++;
					}
					if((nl<8)&&(e->tab[nl][c]<17)&&(e->tab[nl][c]>0))
					{
						//printf("Achou1 captura\n");
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->torre1P2,nl,c);
						//pos adversario = getPos(novo,novo->tab[nl][c]);
						setPos(getPosA(novo,novo->tab[nl][c]),-1,-1);
						novo->tab[l][c]=0;
						novo->tab[nl][c]=19;
						novo->torre1P2Mexeu=1;
						pos rei = novo->reiP2;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
					}
					nl=l-1;
					while((nl>-1)&&(e->tab[nl][c]==0))
					{
						//printf("Achou2 \n");
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->torre1P2,nl,c);
						novo->tab[l][c]=0;
						novo->tab[nl][c]=19;
						novo->torre1P2Mexeu=1;
						pos rei = novo->reiP2;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
						nl--;
					}
					if((nl>-1)&&(e->tab[nl][c]<17)&&(e->tab[nl][c]>0))
					{
						//printf("Achou2 captura\n");
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->torre1P2,nl,c);
						//pos adversario = getPos(novo,novo->tab[nl][c]);
						setPos(getPosA(novo,novo->tab[nl][c]),-1,-1);
						novo->tab[l][c]=0;
						novo->torre1P2Mexeu=1;
						novo->tab[nl][c]=19;
						pos rei = novo->reiP2;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
					}

					int nc=c;
					nc=c+1;
					while((nc<8)&&(e->tab[l][nc]==0))
					{
						//printf("Achou3 \n");
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->torre1P2,l,nc);
						novo->tab[l][c]=0;
						novo->tab[l][nc]=19;
						novo->torre1P2Mexeu=1;
						pos rei = novo->reiP2;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
						nc++;
					}
					if((nc<8)&&(e->tab[l][nc]<17)&&(e->tab[l][nc]>0))
					{
						//printf("Achou3 captura\n");
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->torre1P2,l,nc);
						//pos adversario = getPos(novo,novo->tab[l][nc]);
						setPos(getPosA(novo,novo->tab[l][nc]),-1,-1);
						novo->tab[l][c]=0;
						novo->tab[l][nc]=19;
						novo->torre1P2Mexeu=1;
						pos rei = novo->reiP2;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
					}

					nc=c-1;
					while((nc>-1)&&(e->tab[l][nc]==0))
					{
						//printf("Achou4\n");
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->torre1P2,l,nc);
						novo->tab[l][c]=0;
						novo->tab[l][nc]=19;
						novo->torre1P2Mexeu=1;
						pos rei = novo->reiP2;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
						nc--;
					}
					if((nc>-1)&&(e->tab[l][nc]<17)&&(e->tab[l][nc]>0))
					{
						//printf("Achou4 captura\n");
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->torre1P2,l,nc);
						//pos adversario = getPos(novo,novo->tab[l][nc]);
						setPos(getPosA(novo,novo->tab[l][nc]),-1,-1);
						novo->tab[l][c]=0;
						novo->tab[l][nc]=19;
						novo->torre1P2Mexeu=1;
						pos rei = novo->reiP2;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
					}
				}
			}
			case 4:	//Movimento de torre
			{
				pos torrePos = getPos(e,peca+16);
				if((torrePos.l!=-1)&&(peca==4))
				{
					int nl=l;
					nl=l+1;
					while((nl<8)&&(e->tab[nl][c]==0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->torre2P2,nl,c);
						novo->tab[l][c]=0;
						novo->tab[nl][c]=20;
						novo->torre2P2Mexeu=1;
						pos rei = novo->reiP2;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
						nl++;
					}
					if((nl<8)&&(e->tab[nl][c]<17)&&(e->tab[nl][c]>0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->torre2P2,nl,c);
						//pos adversario = getPos(novo,novo->tab[nl][c]);
						setPos(getPosA(novo,novo->tab[nl][c]),-1,-1);
						novo->tab[l][c]=0;
						novo->tab[nl][c]=20;
						novo->torre2P2Mexeu=1;
						pos rei = novo->reiP2;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
					}
					nl=l-1;
					while((nl>-1)&&(e->tab[nl][c]==0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->torre2P2,nl,c);
						novo->tab[l][c]=0;
						novo->tab[nl][c]=20;
						novo->torre2P2Mexeu=1;
						pos rei = novo->reiP2;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
						nl--;
					}
					if((nl>-1)&&(e->tab[nl][c]<17)&&(e->tab[nl][c]>0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->torre2P2,nl,c);
						//pos adversario = getPos(novo,novo->tab[nl][c]);
						setPos(getPosA(novo,novo->tab[nl][c]),-1,-1);
						novo->tab[l][c]=0;
						novo->torre2P2Mexeu=1;
						novo->tab[nl][c]=20;
						pos rei = novo->reiP2;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
					}

					int nc=c;
					nc=c+1;
					while((nc<8)&&(e->tab[l][nc]==0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->torre2P2,l,nc);
						novo->tab[l][c]=0;
						novo->tab[l][nc]=20;
						novo->torre2P2Mexeu=1;
						pos rei = novo->reiP2;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
						nc++;
					}
					if((nc<8)&&(e->tab[l][nc]<17)&&(e->tab[l][nc]>0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->torre2P2,l,nc);
						//pos adversario = getPos(novo,novo->tab[l][nc]);
						setPos(getPosA(novo,novo->tab[l][nc]),-1,-1);
						novo->tab[l][c]=0;
						novo->tab[l][nc]=20;
						novo->torre2P2Mexeu=1;
						pos rei = novo->reiP2;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
					}

					nc=c-1;
					while((nc>-1)&&(e->tab[l][nc]==0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->torre2P2,l,nc);
						novo->tab[l][c]=0;
						novo->tab[l][nc]=20;
						novo->torre2P2Mexeu=1;
						pos rei = novo->reiP2;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
						nc--;
					}
					if((nc>-1)&&(e->tab[l][nc]<17)&&(e->tab[l][nc]>0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->torre2P2,l,nc);
						//pos adversario = getPos(novo,novo->tab[l][nc]);
						setPos(getPosA(novo,novo->tab[l][nc]),-1,-1);
						novo->tab[l][c]=0;
						novo->tab[l][nc]=20;
						novo->torre2P2Mexeu=1;
						pos rei = novo->reiP2;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
					}

					

				}
			}
			case 5:	//Movimento de Cavalo
			{
				pos cavaloPos = getPos(e,peca+16);
				if((cavaloPos.l!=-1)&&(peca==5))
				{
					int nl=l+2;
					int nc=c-1;
					if((nl<8)&&(nc>=0)&&(e->tab[nl][nc]==0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->cavalo1P2,nl,nc);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=21;

						pos rei = novo->reiP2;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);

					}
					if((nl<8)&&(nc>=0)&&(e->tab[nl][nc]<=16)&&(e->tab[nl][nc]>0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->cavalo1P2,nl,nc);
						
						setPos(getPosA(novo,novo->tab[nl][nc]),-1,-1);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=21;

						pos rei = novo->reiP2;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
					}
					nl=l+2;
					nc=c+1;
					if((nl<8)&&(nc<8)&&(e->tab[nl][nc]==0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->cavalo1P2,nl,nc);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=21;

						pos rei = novo->reiP2;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);

					}
					if((nl<8)&&(nc<8)&&(e->tab[nl][nc]<=16)&&(e->tab[nl][nc]>0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->cavalo1P2,nl,nc);
						
						setPos(getPosA(novo,novo->tab[nl][nc]),-1,-1);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=21;

						pos rei = novo->reiP2;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
					}
					nl=l-2;
					nc=c+1;
					if((nl>=0)&&(nc<8)&&(e->tab[nl][nc]==0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->cavalo1P2,nl,nc);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=21;

						pos rei = novo->reiP2;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);

					}
					if((nl>=0)&&(nc<8)&&(e->tab[nl][nc]<=16)&&(e->tab[nl][nc]>0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->cavalo1P2,nl,nc);
						
						setPos(getPosA(novo,novo->tab[nl][nc]),-1,-1);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=21;

						pos rei = novo->reiP2;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
					}
					nl=l-2;
					nc=c-1;
					if((nl>=0)&&(nc>=0)&&(e->tab[nl][nc]==0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->cavalo1P2,nl,nc);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=21;

						pos rei = novo->reiP2;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);

					}
					if((nl>=0)&&(nc>=0)&&(e->tab[nl][nc]<=16)&&(e->tab[nl][nc]>0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->cavalo1P2,nl,nc);
						
						setPos(getPosA(novo,novo->tab[nl][nc]),-1,-1);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=21;

						pos rei = novo->reiP2;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
					}
					nl=l+1;
					nc=c-2;
					if((nl<8)&&(nc>=0)&&(e->tab[nl][nc]==0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->cavalo1P2,nl,nc);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=21;

						pos rei = novo->reiP2;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);

					}
					if((nl<8)&&(nc>=0)&&(e->tab[nl][nc]<=16)&&(e->tab[nl][nc]>0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->cavalo1P2,nl,nc);
						
						setPos(getPosA(novo,novo->tab[nl][nc]),-1,-1);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=21;

						pos rei = novo->reiP2;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
					}
					nl=l-1;
					nc=c-2;
					if((nl>=0)&&(nc>=0)&&(e->tab[nl][nc]==0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->cavalo1P2,nl,nc);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=21;

						pos rei = novo->reiP2;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);

					}
					if((nl>=0)&&(nc>=0)&&(e->tab[nl][nc]<=16)&&(e->tab[nl][nc]>0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->cavalo1P2,nl,nc);
						
						setPos(getPosA(novo,novo->tab[nl][nc]),-1,-1);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=21;

						pos rei = novo->reiP2;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
					}
					nl=l+1;
					nc=c+2;
					if((nl<8)&&(nc<8)&&(e->tab[nl][nc]==0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->cavalo1P2,nl,nc);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=21;

						pos rei = novo->reiP2;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);

					}
					if((nl<8)&&(nc<8)&&(e->tab[nl][nc]<=16)&&(e->tab[nl][nc]>0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->cavalo1P2,nl,nc);
						
						setPos(getPosA(novo,novo->tab[nl][nc]),-1,-1);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=21;

						pos rei = novo->reiP2;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
					}
					nl=l-1;
					nc=c+2;
					if((nl>=0)&&(nc<8)&&(e->tab[nl][nc]==0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->cavalo1P2,nl,nc);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=21;

						pos rei = novo->reiP2;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);

					}
					if((nl>=0)&&(nc<8)&&(e->tab[nl][nc]<=16)&&(e->tab[nl][nc]>0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->cavalo1P2,nl,nc);
						
						setPos(getPosA(novo,novo->tab[nl][nc]),-1,-1);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=21;

						pos rei = novo->reiP2;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
					}
				}
			}
			case 6:	//Movimento de Cavalo 2
			{
				pos cavaloPos = getPos(e,peca+16);
				if((cavaloPos.l!=-1)&&(peca==6))
				{
					int nl=l+2;
					int nc=c-1;
					if((nl<8)&&(nc>=0)&&(e->tab[nl][nc]==0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->cavalo2P2,nl,nc);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=22;

						pos rei = novo->reiP2;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);

					}
					if((nl<8)&&(nc>=0)&&(e->tab[nl][nc]<=16)&&(e->tab[nl][nc]>0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->cavalo2P2,nl,nc);
						
						setPos(getPosA(novo,novo->tab[nl][nc]),-1,-1);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=22;

						pos rei = novo->reiP2;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
					}
					nl=l+2;
					nc=c+1;
					if((nl<8)&&(nc<8)&&(e->tab[nl][nc]==0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->cavalo2P2,nl,nc);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=22;

						pos rei = novo->reiP2;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);

					}
					if((nl<8)&&(nc<8)&&(e->tab[nl][nc]<=16)&&(e->tab[nl][nc]>0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->cavalo2P2,nl,nc);
						
						setPos(getPosA(novo,novo->tab[nl][nc]),-1,-1);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=22;

						pos rei = novo->reiP2;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
					}
					nl=l-2;
					nc=c+1;
					if((nl>=0)&&(nc<8)&&(e->tab[nl][nc]==0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->cavalo2P2,nl,nc);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=22;

						pos rei = novo->reiP2;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);

					}
					if((nl>=0)&&(nc<8)&&(e->tab[nl][nc]<=16)&&(e->tab[nl][nc]>0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->cavalo2P2,nl,nc);
						
						setPos(getPosA(novo,novo->tab[nl][nc]),-1,-1);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=22;

						pos rei = novo->reiP2;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
					}
					nl=l-2;
					nc=c-1;
					if((nl>=0)&&(nc>=0)&&(e->tab[nl][nc]==0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->cavalo2P2,nl,nc);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=22;

						pos rei = novo->reiP2;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);

					}
					if((nl>=0)&&(nc>=0)&&(e->tab[nl][nc]<=16)&&(e->tab[nl][nc]>0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->cavalo2P2,nl,nc);
						
						setPos(getPosA(novo,novo->tab[nl][nc]),-1,-1);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=22;

						pos rei = novo->reiP2;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
					}
					nl=l+1;
					nc=c-2;
					if((nl<8)&&(nc>=0)&&(e->tab[nl][nc]==0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->cavalo2P2,nl,nc);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=22;

						pos rei = novo->reiP2;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);

					}
					if((nl<8)&&(nc>=0)&&(e->tab[nl][nc]<=16)&&(e->tab[nl][nc]>0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->cavalo2P2,nl,nc);
						
						setPos(getPosA(novo,novo->tab[nl][nc]),-1,-1);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=22;

						pos rei = novo->reiP2;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
					}
					nl=l-1;
					nc=c-2;
					if((nl>=0)&&(nc>=0)&&(e->tab[nl][nc]==0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->cavalo2P2,nl,nc);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=22;

						pos rei = novo->reiP2;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);

					}
					if((nl>=0)&&(nc>=0)&&(e->tab[nl][nc]<=16)&&(e->tab[nl][nc]>0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->cavalo2P2,nl,nc);
						
						setPos(getPosA(novo,novo->tab[nl][nc]),-1,-1);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=22;

						pos rei = novo->reiP2;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
					}
					nl=l+1;
					nc=c+2;
					if((nl<8)&&(nc<8)&&(e->tab[nl][nc]==0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->cavalo2P2,nl,nc);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=22;

						pos rei = novo->reiP2;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);

					}
					if((nl<8)&&(nc<8)&&(e->tab[nl][nc]<=16)&&(e->tab[nl][nc]>0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->cavalo2P2,nl,nc);
						
						setPos(getPosA(novo,novo->tab[nl][nc]),-1,-1);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=22;

						pos rei = novo->reiP2;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
					}
					nl=l-1;
					nc=c+2;
					if((nl>=0)&&(nc<8)&&(e->tab[nl][nc]==0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->cavalo2P2,nl,nc);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=22;

						pos rei = novo->reiP2;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);

					}
					if((nl>=0)&&(nc<8)&&(e->tab[nl][nc]<=16)&&(e->tab[nl][nc]>0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->cavalo2P2,nl,nc);
						
						setPos(getPosA(novo,novo->tab[nl][nc]),-1,-1);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=22;

						pos rei = novo->reiP2;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
					}
				}
			}
			case 7:	//Movimento de  bispo1
			{
				pos bispoPos = getPos(e,peca+16);
				int nl;
				int nc;
				if((bispoPos.l!=-1)&&(peca==7))
				{
					nl=l+1;
					nc=c+1;
					while((nl<8)&&(nc<8)&&(e->tab[nl][nc]==0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->bispo1P2,nl,nc);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=23;

						pos rei = novo->reiP2;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
						nl++;nc++;
					}
					if((nl<8)&&(nc<8)&&(e->tab[nl][nc]<=16)&&(e->tab[nl][nc]>0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->bispo1P2,nl,nc);
						//pos adversario = getPos(novo,novo->tab[nl][c]);
						setPos(getPosA(novo,novo->tab[nl][nc]),-1,-1);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=23;

						pos rei = novo->reiP2;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
					}
					nl=l+1;
					nc=c-1;
					while((nl<8)&&(nc>=0)&&(e->tab[nl][nc]==0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->bispo1P2,nl,nc);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=23;

						pos rei = novo->reiP2;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
						nl++;nc--;
					}
					if((nl<8)&&(nc>=0)&&(e->tab[nl][nc]<=16)&&(e->tab[nl][nc]>0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->bispo1P2,nl,nc);
						//pos adversario = getPos(novo,novo->tab[nl][c]);
						setPos(getPosA(novo,novo->tab[nl][nc]),-1,-1);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=23;

						pos rei = novo->reiP2;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
					}
					nl=l-1;
					nc=c-1;
					while((nl>=0)&&(nc>=0)&&(e->tab[nl][nc]==0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->bispo1P2,nl,nc);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=23;

						pos rei = novo->reiP2;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
						nl--;nc--;
					}
					if((nl>=0)&&(nc>=0)&&(e->tab[nl][nc]<=16)&&(e->tab[nl][nc]>0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->bispo1P2,nl,nc);
						//pos adversario = getPos(novo,novo->tab[nl][c]);
						setPos(getPosA(novo,novo->tab[nl][nc]),-1,-1);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=23;

						pos rei = novo->reiP2;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
					}
					nl=l-1;
					nc=c+1;
					while((nl>=0)&&(nc<8)&&(e->tab[nl][nc]==0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->bispo1P2,nl,nc);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=23;

						pos rei = novo->reiP2;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
						nl--;nc++;
					}
					if((nl>=0)&&(nc<8)&&(e->tab[nl][nc]<=16)&&(e->tab[nl][nc]>0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->bispo1P2,nl,nc);
						//pos adversario = getPos(novo,novo->tab[nl][c]);
						setPos(getPosA(novo,novo->tab[nl][nc]),-1,-1);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=23;

						pos rei = novo->reiP2;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
					}		
				}
			}
			case 8:	//Movimento de  bispo2
			{
				pos bispoPos = getPos(e,peca+16);
				int nl;
				int nc;
				if((bispoPos.l!=-1)&&(peca==8))
				{
					nl=l+1;
					nc=c+1;
					while((nl<8)&&(nc<8)&&(e->tab[nl][nc]==0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->bispo2P2,nl,nc);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=24;

						pos rei = novo->reiP2;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
						nl++;nc++;
					}
					if((nl<8)&&(nc<8)&&(e->tab[nl][nc]<=16)&&(e->tab[nl][nc]>0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->bispo2P2,nl,nc);
						//pos adversario = getPos(novo,novo->tab[nl][c]);
						setPos(getPosA(novo,novo->tab[nl][nc]),-1,-1);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=24;

						pos rei = novo->reiP2;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
					}
					nl=l+1;
					nc=c-1;
					while((nl<8)&&(nc>=0)&&(e->tab[nl][nc]==0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->bispo2P2,nl,nc);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=24;

						pos rei = novo->reiP2;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
						nl++;nc--;
					}
					if((nl<8)&&(nc>=0)&&(e->tab[nl][nc]<=16)&&(e->tab[nl][nc]>0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->bispo2P2,nl,nc);
						//pos adversario = getPos(novo,novo->tab[nl][c]);
						setPos(getPosA(novo,novo->tab[nl][nc]),-1,-1);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=24;

						pos rei = novo->reiP2;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
					}
					nl=l-1;
					nc=c-1;
					while((nl>=0)&&(nc>=0)&&(e->tab[nl][nc]==0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->bispo2P2,nl,nc);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=24;

						pos rei = novo->reiP2;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
						nl--;nc--;
					}
					if((nl>=0)&&(nc>=0)&&(e->tab[nl][nc]<=16)&&(e->tab[nl][nc]>0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->bispo2P2,nl,nc);
						//pos adversario = getPos(novo,novo->tab[nl][c]);
						setPos(getPosA(novo,novo->tab[nl][nc]),-1,-1);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=24;

						pos rei = novo->reiP2;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
					}
					nl=l-1;
					nc=c+1;
					while((nl>=0)&&(nc<8)&&(e->tab[nl][nc]==0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->bispo2P2,nl,nc);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=24;

						pos rei = novo->reiP2;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
						nl--;nc++;
					}
					if((nl>=0)&&(nc<8)&&(e->tab[nl][nc]<=16)&&(e->tab[nl][nc]>0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->bispo2P2,nl,nc);
						//pos adversario = getPos(novo,novo->tab[nl][c]);
						setPos(getPosA(novo,novo->tab[nl][nc]),-1,-1);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=24;

						pos rei = novo->reiP2;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);
					}		
				}
			}
			//case '9':
			default: //Movimento de peoes
			{
				int posic = peca-9;
				peca=peca+16;
				pos peaoPos = getPos(e,peca);
				
				if((peca>24)&&(peca<33)&&(peaoPos.l!=-1))
				{
					int nl=l-1;
					if((nl>-1)&&(e->tab[nl][c]==0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->peaoP2[posic],nl,c);
						novo->tab[l][c]=0;
						novo->tab[nl][c]=peca;
						pos rei = novo->reiP2;
						if(nl==0)
							promovePeaoP2(novo,posic);
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);		
					}

					nl=l-2;
					if((l==6)&&(e->tab[nl][c]==0)&&(e->tab[nl+1][c]==0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->peaoP2[posic],nl,c);
						novo->tab[l][c]=0;
						novo->tab[nl][c]=peca;
						pos rei = novo->reiP2;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);		
					}
										
					nl=l-1;
					int nc=c+1;
					if((nl>-1)&&(nc<8)&&(e->tab[nl][nc]<17)&&(e->tab[nl][nc]>0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->peaoP2[posic],nl,nc);
						//pos adversario = getPos(novo,novo->tab[nl][nc]);
						setPos(getPosA(novo,novo->tab[nl][nc]),-1,-1);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=peca;
						if(nl==0)
							promovePeaoP2(novo,posic);
						pos rei = novo->reiP2;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);		
					}
	
					nl=l-1;
					nc=c-1;
					if((nl>-1)&&(nc>-1)&&(e->tab[nl][nc]<17)&&(e->tab[nl][nc]>0))
					{
						estado *novo = constroiEstadoApartirDe(e);
						setPos(&novo->peaoP2[posic],nl,nc);
						//pos adversario = getPos(novo,novo->tab[nl][nc]);
						setPos(getPosA(novo,novo->tab[nl][nc]),-1,-1);
						novo->tab[l][c]=0;
						novo->tab[nl][nc]=peca;
						if(nl==0)
							promovePeaoP2(novo,posic);
						pos rei = novo->reiP2;
						if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
							insereLista(e->filhos,novo);
						else
							free(novo);		
					}
				}
			}
			
		}
	}	
}


void adicionaFilhosPeaoPromovidoP1(int jogador,byte peca,pos p,estado *e)
{
	int l,c;
	l= p.l;
	c = p.c;
	pos damaPos = getPos(e,peca);
	int nl;
	int nc;
	if(damaPos.l!=-1)
	{
		nl=l+1;
		nc=c+1;
		while((nl<8)&&(nc<8)&&(e->tab[nl][nc]==0))
		{
			estado *novo = constroiEstadoApartirDe(e);
			setPos(&novo->peaoP1[peca-9],nl,nc);
			novo->tab[l][c]=0;
			novo->tab[nl][nc]=peca;

			pos rei = novo->reiP1;
			if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
				insereLista(e->filhos,novo);
			else
				free(novo);
			nl++;nc++;
		}
		if((nl<8)&&(nc<8)&&(e->tab[nl][nc]>16))
		{
			estado *novo = constroiEstadoApartirDe(e);
			setPos(&novo->peaoP1[peca-9],nl,nc);
			//pos adversario = getPos(novo,novo->tab[nl][c]);
			setPos(getPosA(novo,novo->tab[nl][nc]),-1,-1);
			novo->tab[l][c]=0;
			novo->tab[nl][nc]=peca;

			pos rei = novo->reiP1;
			if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
				insereLista(e->filhos,novo);
			else
				free(novo);
		}
		nl=l+1;
		nc=c-1;
		while((nl<8)&&(nc>=0)&&(e->tab[nl][nc]==0))
		{
			estado *novo = constroiEstadoApartirDe(e);
			setPos(&novo->peaoP1[peca-9],nl,nc);
			novo->tab[l][c]=0;
			novo->tab[nl][nc]=peca;

			pos rei = novo->reiP1;
			if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
				insereLista(e->filhos,novo);
			else
				free(novo);
			nl++;nc--;
		}
		if((nl<8)&&(nc>=0)&&(e->tab[nl][nc]>16))
		{
			estado *novo = constroiEstadoApartirDe(e);
			setPos(&novo->peaoP1[peca-9],nl,nc);
			//pos adversario = getPos(novo,novo->tab[nl][c]);
			setPos(getPosA(novo,novo->tab[nl][nc]),-1,-1);
			novo->tab[l][c]=0;
			novo->tab[nl][nc]=peca;

			pos rei = novo->reiP1;
			if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
				insereLista(e->filhos,novo);
			else
				free(novo);
		}
		nl=l-1;
		nc=c-1;
		while((nl>=0)&&(nc>=0)&&(e->tab[nl][nc]==0))
		{
			estado *novo = constroiEstadoApartirDe(e);
			setPos(&novo->peaoP1[peca-9],nl,nc);
			novo->tab[l][c]=0;
			novo->tab[nl][nc]=peca;

			pos rei = novo->reiP1;
			if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
				insereLista(e->filhos,novo);
			else
				free(novo);
			nl--;nc--;
		}
		if((nl>=0)&&(nc>=0)&&(e->tab[nl][nc]>16))
		{
			estado *novo = constroiEstadoApartirDe(e);
			setPos(&novo->peaoP1[peca-9],nl,nc);
			//pos adversario = getPos(novo,novo->tab[nl][c]);
			setPos(getPosA(novo,novo->tab[nl][nc]),-1,-1);
			novo->tab[l][c]=0;
			novo->tab[nl][nc]=peca;

			pos rei = novo->reiP1;
			if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
				insereLista(e->filhos,novo);
			else
				free(novo);
		}
		nl=l-1;
		nc=c+1;
		while((nl>=0)&&(nc<8)&&(e->tab[nl][nc]==0))
		{
			estado *novo = constroiEstadoApartirDe(e);
			setPos(&novo->peaoP1[peca-9],nl,nc);
			novo->tab[l][c]=0;
			novo->tab[nl][nc]=peca;

			pos rei = novo->reiP1;
			if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
				insereLista(e->filhos,novo);
			else
				free(novo);
			nl--;nc++;
		}
		if((nl>=0)&&(nc<8)&&(e->tab[nl][nc]>16))
		{
			estado *novo = constroiEstadoApartirDe(e);
			setPos(&novo->peaoP1[peca-9],nl,nc);
			//pos adversario = getPos(novo,novo->tab[nl][c]);
			setPos(getPosA(novo,novo->tab[nl][nc]),-1,-1);
			novo->tab[l][c]=0;
			novo->tab[nl][nc]=peca;

			pos rei = novo->reiP1;
			if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
				insereLista(e->filhos,novo);
			else
				free(novo);
		}
		nl=l+1;
		while((nl<8)&&(e->tab[nl][c]==0))
		{
			estado *novo = constroiEstadoApartirDe(e);
			setPos(&novo->peaoP1[peca-9],nl,c);
			novo->tab[l][c]=0;
			novo->tab[nl][c]=peca;

			pos rei = novo->reiP1;
			if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
				insereLista(e->filhos,novo);
			else
				free(novo);
			nl++;
		}
		if((nl<8)&&(e->tab[nl][c]>16))
		{
			estado *novo = constroiEstadoApartirDe(e);
			setPos(&novo->peaoP1[peca-9],nl,c);
			//pos adversario = getPos(novo,novo->tab[nl][c]);
			setPos(getPosA(novo,novo->tab[nl][c]),-1,-1);
			novo->tab[l][c]=0;
			novo->tab[nl][c]=peca;

			pos rei = novo->reiP1;
			if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
				insereLista(e->filhos,novo);
			else
				free(novo);
		}
		nl=l-1;
		while((nl>-1)&&(e->tab[nl][c]==0))
		{
			estado *novo = constroiEstadoApartirDe(e);
			setPos(&novo->peaoP1[peca-9],nl,c);
			novo->tab[l][c]=0;
			novo->tab[nl][c]=peca;

			pos rei = novo->reiP1;
			if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
				insereLista(e->filhos,novo);
			else
				free(novo);
			nl--;
		}

		if((nl>-1)&&(e->tab[nl][c]>16))
		{
			estado *novo = constroiEstadoApartirDe(e);
			//pos adversario = getPos(novo,novo->tab[nl][c]);
			setPos(getPosA(novo,novo->tab[nl][c]),-1,-1);
	
			setPos(&novo->peaoP1[peca-9],nl,c);
			novo->tab[l][c]=0;
			novo->tab[nl][c]=peca;

			pos rei = novo->reiP1;
			if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
				insereLista(e->filhos,novo);
			else
				free(novo);
		}

		nc=c+1;
		while((nc<8)&&(e->tab[l][nc]==0))
		{
			estado *novo = constroiEstadoApartirDe(e);
			setPos(&novo->peaoP1[peca-9],l,nc);
			novo->tab[l][c]=0;
			novo->tab[l][nc]=peca;

			pos rei = novo->reiP1;
			if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
				insereLista(e->filhos,novo);
			else
				free(novo);
			nc++;
		}
		if((nc<8)&&(e->tab[l][nc]>16))
		{
			estado *novo = constroiEstadoApartirDe(e);
			setPos(&novo->peaoP1[peca-9],l,nc);
			//pos adversario = getPos(novo,novo->tab[l][nc]);
			setPos(getPosA(novo,novo->tab[l][nc]),-1,-1);
			novo->tab[l][c]=0;
			novo->tab[l][nc]=peca;

			pos rei = novo->reiP1;
			if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
				insereLista(e->filhos,novo);
			else
				free(novo);
		}

		nc=c-1;
		while((nc>-1)&&(e->tab[l][nc]==0))
		{
			estado *novo = constroiEstadoApartirDe(e);
			setPos(&novo->peaoP1[peca-9],l,nc);
			novo->tab[l][c]=0;
			novo->tab[l][nc]=peca;

			pos rei = novo->reiP1;
			if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
				insereLista(e->filhos,novo);
			else
				free(novo);
			nc--;
		}
		if((nc>-1)&&(e->tab[l][nc]>16))
		{
			estado *novo = constroiEstadoApartirDe(e);
			setPos(&novo->peaoP1[peca-9],l,nc);
			//pos adversario = getPos(novo,novo->tab[l][nc]);
			setPos(getPosA(novo,novo->tab[l][nc]),-1,-1);
			novo->tab[l][c]=0;
			novo->tab[l][nc]=peca;

			pos rei = novo->reiP1;
			if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
				insereLista(e->filhos,novo);
			else
				free(novo);
		}

	}
}

int tiu = 0;

void adicionaFilhosPeaoPromovidoP2(int jogador,byte peca,pos p,estado *e)
{
	int l,c;
	l= p.l;
	c = p.c;
	pos damaPos = getPos(e,peca);
	int nl;
	int nc;

	if(damaPos.l!=-1)
	{
		nl=l+1;
		nc=c+1;
		while((nl<8)&&(nc<8)&&(e->tab[nl][nc]==0))
		{
			estado *novo = constroiEstadoApartirDe(e);
			setPos(&novo->peaoP2[peca-25],nl,nc);
			novo->tab[l][c]=0;
			novo->tab[nl][nc]=peca;

			pos rei = novo->reiP2;
			if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
				insereLista(e->filhos,novo);
			else
				free(novo);
			nl++;nc++;
		}
		if((nl<8)&&(nc<8)&&(e->tab[nl][nc]<17)&&(e->tab[nl][nc]>0))
		{
			estado *novo = constroiEstadoApartirDe(e);
			setPos(&novo->peaoP2[peca-25],nl,nc);
			//pos adversario = getPos(novo,novo->tab[nl][c]);
			setPos(getPosA(novo,novo->tab[nl][nc]),-1,-1);
			novo->tab[l][c]=0;
			novo->tab[nl][nc]=peca;

			pos rei = novo->reiP2;
			if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
				insereLista(e->filhos,novo);
			else
				free(novo);
		}
		nl=l+1;
		nc=c-1;
		while((nl<8)&&(nc>=0)&&(e->tab[nl][nc]==0))
		{
			estado *novo = constroiEstadoApartirDe(e);
			setPos(&novo->peaoP2[peca-25],nl,nc);
			novo->tab[l][c]=0;
			novo->tab[nl][nc]=peca;

			pos rei = novo->reiP2;
			if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
				insereLista(e->filhos,novo);
			else
				free(novo);
			nl++;nc--;
		}
		if((nl<8)&&(nc>=0)&&(e->tab[nl][nc]<17)&&(e->tab[nl][nc]>0))
		{
			estado *novo = constroiEstadoApartirDe(e);
			setPos(&novo->peaoP2[peca-25],nl,nc);
			//pos adversario = getPos(novo,novo->tab[nl][c]);
			setPos(getPosA(novo,novo->tab[nl][nc]),-1,-1);
			novo->tab[l][c]=0;
			novo->tab[nl][nc]=peca;

			pos rei = novo->reiP2;
			if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
				insereLista(e->filhos,novo);
			else
				free(novo);
		}
		nl=l-1;
		nc=c-1;
		while((nl>=0)&&(nc>=0)&&(e->tab[nl][nc]==0))
		{
			estado *novo = constroiEstadoApartirDe(e);
			setPos(&novo->peaoP2[peca-25],nl,nc);
			novo->tab[l][c]=0;
			novo->tab[nl][nc]=peca;

			pos rei = novo->reiP2;
			if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
				insereLista(e->filhos,novo);
			else
				free(novo);
			nl--;nc--;
		}
		if((nl>=0)&&(nc>=0)&&(e->tab[nl][nc]<17)&&(e->tab[nl][nc]>0))
		{
			estado *novo = constroiEstadoApartirDe(e);
			setPos(&novo->peaoP2[peca-25],nl,nc);
			//pos adversario = getPos(novo,novo->tab[nl][c]);
			setPos(getPosA(novo,novo->tab[nl][nc]),-1,-1);
			novo->tab[l][c]=0;
			novo->tab[nl][nc]=peca;

			pos rei = novo->reiP2;
			if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
				insereLista(e->filhos,novo);
			else
				free(novo);
		}
		nl=l-1;
		nc=c+1;
		while((nl>=0)&&(nc<8)&&(e->tab[nl][nc]==0))
		{
			estado *novo = constroiEstadoApartirDe(e);
			setPos(&novo->peaoP2[peca-25],nl,nc);
			novo->tab[l][c]=0;
			novo->tab[nl][nc]=peca;

			pos rei = novo->reiP2;
			if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
				insereLista(e->filhos,novo);
			else
				free(novo);
			nl--;nc++;
		}
		if((nl>=0)&&(nc<8)&&(e->tab[nl][nc]<17)&&(e->tab[nl][nc]>0))
		{
			estado *novo = constroiEstadoApartirDe(e);
			setPos(&novo->peaoP2[peca-25],nl,nc);
			//pos adversario = getPos(novo,novo->tab[nl][c]);
			setPos(getPosA(novo,novo->tab[nl][nc]),-1,-1);
			novo->tab[l][c]=0;
			novo->tab[nl][nc]=peca;

			pos rei = novo->reiP2;
			if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
				insereLista(e->filhos,novo);
			else
				free(novo);
		}
		nl=l+1;
		while((nl<8)&&(e->tab[nl][c]==0))
		{
			estado *novo = constroiEstadoApartirDe(e);
			setPos(&novo->peaoP2[peca-25],nl,c);
			novo->tab[l][c]=0;
			novo->tab[nl][c]=peca;

			pos rei = novo->reiP2;
			if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
				insereLista(e->filhos,novo);
			else
				free(novo);
			nl++;
		}
		if((nl<8)&&(e->tab[nl][c]<17)&&(e->tab[nl][c]>0))
		{
			estado *novo = constroiEstadoApartirDe(e);
			setPos(&novo->peaoP2[peca-25],nl,c);
			//pos adversario = getPos(novo,novo->tab[nl][c]);
			setPos(getPosA(novo,novo->tab[nl][c]),-1,-1);
			novo->tab[l][c]=0;
			novo->tab[nl][c]=peca;

			pos rei = novo->reiP2;
			if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
				insereLista(e->filhos,novo);
			else
				free(novo);
		}
		nl=l-1;
		while((nl>-1)&&(e->tab[nl][c]==0))
		{
			estado *novo = constroiEstadoApartirDe(e);
			setPos(&novo->peaoP2[peca-25],nl,c);
			novo->tab[l][c]=0;
			novo->tab[nl][c]=peca;

			pos rei = novo->reiP2;
			if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
				insereLista(e->filhos,novo);
			else
				free(novo);
			nl--;
		}

		if((nl>-1)&&(e->tab[nl][c]<17)&&(e->tab[nl][c]>0))
		{
			estado *novo = constroiEstadoApartirDe(e);
			//pos adversario = getPos(novo,novo->tab[nl][c]);
			setPos(getPosA(novo,novo->tab[nl][c]),-1,-1);
	
			setPos(&novo->peaoP2[peca-25],nl,c);
			novo->tab[l][c]=0;
			novo->tab[nl][c]=peca;

			pos rei = novo->reiP2;
			if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
				insereLista(e->filhos,novo);
			else
				free(novo);
		}

		nc=c+1;
		while((nc<8)&&(e->tab[l][nc]==0))
		{
			estado *novo = constroiEstadoApartirDe(e);
			setPos(&novo->peaoP2[peca-25],l,nc);
			novo->tab[l][c]=0;
			novo->tab[l][nc]=peca;

			pos rei = novo->reiP2;
			if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
				insereLista(e->filhos,novo);
			else
				free(novo);
			nc++;
		}
		if((nc<8)&&(e->tab[l][nc]<17)&&(e->tab[l][nc]>0))
		{
			estado *novo = constroiEstadoApartirDe(e);
			setPos(&novo->peaoP2[peca-25],l,nc);
			//pos adversario = getPos(novo,novo->tab[l][nc]);
			setPos(getPosA(novo,novo->tab[l][nc]),-1,-1);
			novo->tab[l][c]=0;
			novo->tab[l][nc]=peca;

			pos rei = novo->reiP2;
			if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
				insereLista(e->filhos,novo);
			else
				free(novo);
		}

		nc=c-1;
		while((nc>-1)&&(e->tab[l][nc]==0))
		{
			estado *novo = constroiEstadoApartirDe(e);
			setPos(&novo->peaoP2[peca-25],l,nc);
			novo->tab[l][c]=0;
			novo->tab[l][nc]=peca;

			pos rei = novo->reiP2;
			if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
				insereLista(e->filhos,novo);
			else
				free(novo);
			nc--;
		}
		if((nc>-1)&&(e->tab[l][nc]<17)&&(e->tab[l][nc]>0))
		{
			estado *novo = constroiEstadoApartirDe(e);
			setPos(&novo->peaoP2[peca-25],l,nc);
			//pos adversario = getPos(novo,novo->tab[l][nc]);
			setPos(getPosA(novo,novo->tab[l][nc]),-1,-1);
			novo->tab[l][c]=0;
			novo->tab[l][nc]=peca;

			pos rei = novo->reiP2;
			if(posicaoNaoAtacada(rei.l,rei.c,novo,jogador))
				insereLista(e->filhos,novo);
			else
				free(novo);
		}

	}
}



int npecas(estado *e,int jogador)
{
	int n=0;int i=0;
	if(jogador==1)
	{
		if(e->reiP1.l!=-1)
			n++;
		if(e->damaP1.l!=-1)
			n++;
		if(e->cavalo1P1.l!=-1)
			n++;
		if(e->cavalo2P1.l!=-1)
			n++;
		if(e->bispo1P1.l!=-1)
			n++;
		if(e->bispo2P1.l!=-1)
			n++;
		if(e->torre1P1.l!=-1)
			n++;
		if(e->torre2P1.l!=-1)
			n++;
		
		for(i=0;i<8;i++)
			if(e->peaoP1[i].l!=-1)
				n++;	

		return n;		
	}
	else
	{
		if(e->reiP2.l!=-1)
			n++;
		if(e->damaP2.l!=-1)
			n++;
		if(e->cavalo1P2.l!=-1)
			n++;
		if(e->cavalo2P2.l!=-1)
			n++;
		if(e->bispo1P2.l!=-1)
			n++;
		if(e->bispo2P2.l!=-1)
			n++;
		if(e->torre1P2.l!=-1)
			n++;
		if(e->torre2P2.l!=-1)
			n++;
		
		for(i=0;i<8;i++)
			if(e->peaoP2[i].l!=-1)
				n++;	

		return n;		
	}
}

int nAmeacas(estado *e,int jogador)
{
	int n=0;int i=0;
	if(jogador==1)
	{
		if((e->reiP1.l!=-1)&&( !posicaoNaoAtacada(e->reiP1.l,e->reiP1.c,e,jogador) ))
			n++;
		if((e->damaP1.l!=-1)&&( !posicaoNaoAtacada(e->damaP1.l,e->damaP1.c,e,jogador) ))
			n++;
		if((e->cavalo1P1.l!=-1)&&( !posicaoNaoAtacada(e->cavalo1P1.l,e->cavalo1P1.c,e,jogador) ))
			n++;
		if((e->cavalo2P1.l!=-1)&&( !posicaoNaoAtacada(e->cavalo2P1.l,e->cavalo2P1.c,e,jogador) ))
			n++;
		if((e->bispo1P1.l!=-1)&&( !posicaoNaoAtacada(e->bispo1P1.l,e->bispo1P1.c,e,jogador) ))
			n++;
		if((e->bispo2P1.l!=-1)&&( !posicaoNaoAtacada(e->bispo2P1.l,e->bispo2P1.c,e,jogador) ))
			n++;
		if((e->torre1P1.l!=-1)&&( !posicaoNaoAtacada(e->torre1P1.l,e->torre1P1.c,e,jogador) ))
			n++;
		if((e->torre2P1.l!=-1)&&( !posicaoNaoAtacada(e->torre2P1.l,e->torre2P1.c,e,jogador) ))
			n++;
		
		for(i=0;i<8;i++)
			if((e->peaoP1[i].l!=-1)&&( !posicaoNaoAtacada(e->peaoP1[i].l,e->peaoP1[i].c,e,jogador) ))
				n++;	

		return n;		
	}
	else
	{
		if((e->reiP2.l!=-1)&&( !posicaoNaoAtacada(e->reiP2.l,e->reiP2.c,e,jogador) ))
			n++;
		if((e->damaP2.l!=-1)&&( !posicaoNaoAtacada(e->damaP2.l,e->damaP2.c,e,jogador) ))
			n++;
		if((e->cavalo1P2.l!=-1)&&( !posicaoNaoAtacada(e->cavalo1P2.l,e->cavalo1P2.c,e,jogador) ))
			n++;
		if((e->cavalo2P2.l!=-1)&&( !posicaoNaoAtacada(e->cavalo2P2.l,e->cavalo2P2.c,e,jogador) ))
			n++;
		if((e->bispo1P2.l!=-1)&&( !posicaoNaoAtacada(e->bispo1P2.l,e->bispo1P2.c,e,jogador) ))
			n++;
		if((e->bispo2P2.l!=-1)&&( !posicaoNaoAtacada(e->bispo2P2.l,e->bispo2P2.c,e,jogador) ))
			n++;
		if((e->torre1P2.l!=-1)&&( !posicaoNaoAtacada(e->torre1P2.l,e->torre1P2.c,e,jogador) ))
			n++;
		if((e->torre2P2.l!=-1)&&( !posicaoNaoAtacada(e->torre2P2.l,e->torre2P2.c,e,jogador) ))
			n++;
		
		for(i=0;i<8;i++)
			if((e->peaoP2[i].l!=-1)&&( !posicaoNaoAtacada(e->peaoP2[i].l,e->peaoP2[i].c,e,jogador) ))
				n++;	

		return n;		
	}
}

int pecaAmeacada(estado *e,int jogador)
{
	int i=0;
	if(jogador==1)
	{
		if((e->reiP1.l!=-1)&&( !posicaoNaoAtacada(e->reiP1.l,e->reiP1.c,e,jogador) ))
			return e->tab[e->reiP1.l][e->reiP1.c];
		if((e->damaP1.l!=-1)&&( !posicaoNaoAtacada(e->damaP1.l,e->damaP1.c,e,jogador) ))
			return e->tab[e->damaP1.l][e->damaP1.c];
		if((e->torre1P1.l!=-1)&&( !posicaoNaoAtacada(e->torre1P1.l,e->torre1P1.c,e,jogador) ))
			return e->tab[e->torre1P1.l][e->torre1P1.c];
		if((e->torre2P1.l!=-1)&&( !posicaoNaoAtacada(e->torre2P1.l,e->torre2P1.c,e,jogador) ))
			return e->tab[e->torre2P1.l][e->torre2P1.c];
		if((e->cavalo1P1.l!=-1)&&( !posicaoNaoAtacada(e->cavalo1P1.l,e->cavalo1P1.c,e,jogador) ))
			return e->tab[e->cavalo1P1.l][e->cavalo1P1.c];
		if((e->cavalo2P1.l!=-1)&&( !posicaoNaoAtacada(e->cavalo2P1.l,e->cavalo2P1.c,e,jogador) ))
			return e->tab[e->cavalo2P1.l][e->cavalo2P1.c];
		if((e->bispo1P1.l!=-1)&&( !posicaoNaoAtacada(e->bispo1P1.l,e->bispo1P1.c,e,jogador) ))
			return e->tab[e->bispo1P1.l][e->bispo1P1.c];
		if((e->bispo2P1.l!=-1)&&( !posicaoNaoAtacada(e->bispo2P1.l,e->bispo2P1.c,e,jogador) ))
			return e->tab[e->bispo2P1.l][e->bispo2P1.c];

		
		for(i=0;i<8;i++)
			if((e->peaoP1[i].l!=-1)&&( !posicaoNaoAtacada(e->peaoP1[i].l,e->peaoP1[i].c,e,jogador) ))
				return e->tab[e->peaoP1[i].l][e->peaoP1[i].c];	
	
		return 0;		
	}
	else
	{
		if((e->reiP2.l!=-1)&&( !posicaoNaoAtacada(e->reiP2.l,e->reiP2.c,e,jogador) ))
			return e->tab[e->reiP2.l][e->reiP2.c];
		if((e->damaP2.l!=-1)&&( !posicaoNaoAtacada(e->damaP2.l,e->damaP2.c,e,jogador) ))
			return e->tab[e->damaP2.l][e->damaP2.c];
		if((e->torre1P2.l!=-1)&&( !posicaoNaoAtacada(e->torre1P2.l,e->torre1P2.c,e,jogador) ))
			return e->tab[e->torre1P2.l][e->torre1P2.c];
		if((e->torre2P2.l!=-1)&&( !posicaoNaoAtacada(e->torre2P2.l,e->torre2P2.c,e,jogador) ))
			return e->tab[e->torre2P2.l][e->torre2P2.c];
		if((e->cavalo1P2.l!=-1)&&( !posicaoNaoAtacada(e->cavalo1P2.l,e->cavalo1P2.c,e,jogador) ))
			return e->tab[e->cavalo1P2.l][e->cavalo1P2.c];
		if((e->cavalo2P2.l!=-1)&&( !posicaoNaoAtacada(e->cavalo2P2.l,e->cavalo2P2.c,e,jogador) ))
			return e->tab[e->cavalo2P2.l][e->cavalo2P2.c];
		if((e->bispo1P2.l!=-1)&&( !posicaoNaoAtacada(e->bispo1P2.l,e->bispo1P2.c,e,jogador) ))
			return e->tab[e->bispo1P2.l][e->bispo1P2.c];
		if((e->bispo2P2.l!=-1)&&( !posicaoNaoAtacada(e->bispo2P2.l,e->bispo2P2.c,e,jogador) ))
			return e->tab[e->bispo2P2.l][e->bispo2P2.c];

		
		for(i=0;i<8;i++)
			if((e->peaoP2[i].l!=-1)&&( !posicaoNaoAtacada(e->peaoP2[i].l,e->peaoP2[i].c,e,jogador) ))
				return e->tab[e->peaoP2[i].l][e->peaoP2[i].c];		

		return 0;		
	}
}



void ordenaFilhos(estado *e,int jogador)
{
	if(e->filhos->tam==0)
		return;
	if(jogador==1)
	{
		lista *ordenada = constroiLista();
		int *jaOrdenado = (int *)calloc(e->filhos->tam,sizeof(int));


		int nPecasCorrente = npecas(e,2);
		int nAmeacasCorrente = nAmeacas(e,2);

		int nPecasFilho; int nAmeacasFilho;
		int i=0;estado *filho;no *aux=e->filhos->head;
		for(aux;aux;aux=aux->prox)
		{
			
			filho = aux->o;
			nPecasFilho = npecas(filho,2);
			if(nPecasFilho<nPecasCorrente)
			{
				insereLista(ordenada,filho);
				jaOrdenado[i]=1;
			}
			i++;
		}

		i=0;aux=e->filhos->head;
		for(aux;aux;aux=aux->prox)
		{
			filho = aux->o;
			nAmeacasFilho = nAmeacas(filho,2);
			if(nAmeacasFilho>nAmeacasCorrente)
			{
				if(!jaOrdenado[i])
				{
					insereLista(ordenada,filho);
					jaOrdenado[i]=1;
				}
			}
			i++;
		}			

		i=0;aux=e->filhos->head;
		for(aux;aux;aux=aux->prox)
		{
			filho = aux->o;
			if(!jaOrdenado[i])
			{
					insereLista(ordenada,filho);
					jaOrdenado[i]=1;
			}
			i++;
		}	

		lista *laux = e->filhos; 
		e->filhos = ordenada;
		//free(laux);
		free(jaOrdenado);			
	}
	
	else
	{
		lista *ordenada = constroiLista();
		int *jaOrdenado = (int *)calloc(e->filhos->tam,sizeof(int));


		int nPecasCorrente = npecas(e,1);
		int nAmeacasCorrente = nAmeacas(e,1);

		int nPecasFilho;int nAmeacasFilho;
		int i=0;estado *filho;no *aux=e->filhos->head;
		for(aux;aux;aux=aux->prox)
		{
			estado *filho = aux->o;
			nPecasFilho = npecas(filho,1);
			if(nPecasFilho<nPecasCorrente)
			{
				insereLista(ordenada,filho);
				jaOrdenado[i]=1;
			}
			i++;
		}

		i=0;aux=e->filhos->head;
		for(aux;aux;aux=aux->prox)
		{
			filho = aux->o;
			nAmeacasFilho = nAmeacas(filho,1);
			if(nAmeacasFilho>nAmeacasCorrente)
			{
				if(!jaOrdenado[i])
				{
					insereLista(ordenada,filho);
					jaOrdenado[i]=1;
				}
			}
			i++;
		}			

		i=0;aux=e->filhos->head;
		for(aux;aux;aux=aux->prox)
		{
			filho=aux->o;
			if(!jaOrdenado[i])
			{
					insereLista(ordenada,filho);
					jaOrdenado[i]=1;
			}
			i++;
		}	

		lista *laux = e->filhos; 
		e->filhos = ordenada;
		//free(laux);
		free(jaOrdenado);
	}
	
}

void constroiFilhos(estado *e,int jogador)
{
	int peca;
	pos p;

	if(jogador==1)
	{
		for(peca=16;peca>0;peca--)
		{
			

			p = getPos(e,peca);
			if(p.l!=-1)
			{	
				if((peca>8)&&(peca<=16)&&(promovidoP1(e,peca-9)))
				{	adicionaFilhosPeaoPromovidoP1(jogador,peca,p,e);}
				else
					adicionaFilhos(jogador,peca,p,e);
			}
		}
				
	}
	
	else
	{
		for(peca=32;peca>16;peca--)
		{
			p = getPos(e,peca);
			
			if(p.l!=-1)
			{
				if((peca>24)&&(peca<=32)&&(promovidoP2(e,peca-25)))
				{	adicionaFilhosPeaoPromovidoP2(jogador,peca,p,e);}
				else
					adicionaFilhos(jogador,peca,p,e);
			}
		}
	}

	ordenaFilhos(e,jogador);
}


void informaEstado(estado *e)
{
	int peca;
	pos p;
	
	for(peca=1;peca<33;peca++)
	{
		p = getPos(e,peca);
		if(p.l!=-1)
		{
			printf("Peca = %d , l = %d c = %d\n",peca,p.l,p.c);
		}
	}
		
}

int contraAtaquePeao(int l,int c,estado *e,int jogador)
{
	int ataque = 0;
	if(jogador==1)
	{
		int nl=l+1;
		int nc=c+1; 
		if((nl<8)&&(nc<8)&&(e->tab[nl][nc]>24)&&(e->tab[nl][nc]<=32)
		&&(!promovidoP2(e,25-e->tab[nl][nc])))
			ataque=10;	
		
		nc=c-1;
		if((nl<8)&&(nc>-1)&&(e->tab[nl][nc]>24)&&(e->tab[nl][nc]<=32)
		&&(!promovidoP2(e,25-e->tab[nl][nc])))
			ataque=10;	
		
	}
	else
	{
		int nl=l-1;
		int nc=c+1; 
		if((nl>-1)&&(nc<8)&&(e->tab[nl][nc]>8)&&(e->tab[nl][nc]<=16)
		&&(!promovidoP1(e,9-e->tab[nl][nc])))
			ataque=10;	
		
		nc=c-1;
		if((nl>-1)&&(nc>-1)&&(e->tab[nl][nc]>8)&&(e->tab[nl][nc]<=16)
		&&(!promovidoP1(e,9-e->tab[nl][nc])))
			ataque=10;
	}

	return ataque;
}

int ataquePeao(int l,int c,estado *e,int jogador)
{
	int ataque = 0;
	if(jogador==1)
	{
		int nl=l+1;
		int nc=c+1; 
		if((nl<8)&&(nc<8)&&(e->tab[nl][nc]>24)&&(e->tab[nl][nc]<=32))
			ataque=1;	
		
		nc=c-1;
		if((nl<8)&&(nc>-1)&&(e->tab[nl][nc]>24)&&(e->tab[nl][nc]<=32))
			ataque=1;	
		
	}
	else
	{
		int nl=l-1;
		int nc=c+1; 
		if((nl>-1)&&(nc<8)&&(e->tab[nl][nc]>8)&&(e->tab[nl][nc]<=16))
			ataque=1;	
		
		nc=c-1;
		if((nl>-1)&&(nc>-1)&&(e->tab[nl][nc]>8)&&(e->tab[nl][nc]<=16))
			ataque=1;
	}


	return ataque;
}


int pecaPromovidaP1(estado *e,int peca)
{
	if((peca>8)&&(peca<17))
	{
		int posic = peca-9;
		if(promovidoP1(e,posic))
			return 1;
	}

	return 0;
}

int pecaPromovidaP2(estado *e,int peca)
{
	if((peca>24)&&(peca<33))
	{
		int posic = peca-25;
		if(promovidoP2(e,posic))
			return 1;
	}

	return 0;
}

int contraAtaqueDiagonal(int l,int c,estado *e,int jogador)
{
	int ataque=0;
	if(jogador==1)
	{
		int nl=l+1;
		int nc =c+1;
		//printf("%d %d %d atacado por %d\n",l,nl,c,e->tab[nl][c]);
		while((nl<8)&&(nc<8)&&(e->tab[nl][nc]==0))
		{	nl++;nc++;}
		
		if((nl<8)&&(nc<8)&&( (e->tab[nl][nc]==18)||(e->tab[nl][nc]==23)||(e->tab[nl][nc]==24)
		||(pecaPromovidaP2(e,e->tab[nl][nc]) ) ) )
		{	
			if((e->tab[nl][nc]==23)||(e->tab[nl][nc]==24))	
				return 30;
			else ataque = 90;
		}
		nl=l-1;
		nc =c+1;
		while((nl>-1)&&(nc<8)&&(e->tab[nl][nc]==0))
		{	nl--;nc++;}
		
		if((nl>-1)&&(nc<8)&&( (e->tab[nl][nc]==18)||(e->tab[nl][nc]==23)||(e->tab[nl][nc]==24)
		||(pecaPromovidaP2(e,e->tab[nl][nc]) ) ) )
		{
			if((e->tab[nl][nc]==23)||(e->tab[nl][nc]==24))	
				return 30;
			else ataque = 90;
		}
		
		nl=l-1;
		nc=c-1;
		while((nc>-1)&&(nl>-1)&&(e->tab[nl][nc]==0))
		{	nc--;nl--;}
		
		if((nc>-1)&&(nl>-1)&&( (e->tab[nl][nc]==18)||(e->tab[nl][nc]==23)||(e->tab[nl][nc]==24)
		||(pecaPromovidaP2(e,e->tab[nl][nc]) ) ) )
		{
			if((e->tab[nl][nc]==23)||(e->tab[nl][nc]==24))	
				return 30;
			else ataque = 90;
		}
		nl=l+1;
		nc=c-1;
		while((nc>-1)&&(nl<8)&&(e->tab[nl][nc]==0))
		{	nc--;nl++;}
		
		if((nc>-1)&&(nl<8)&&( (e->tab[nl][nc]==18)||(e->tab[nl][nc]==23)||(e->tab[nl][nc]==24)
		||(pecaPromovidaP2(e,e->tab[nl][nc]) ) ) )
		{		
			if((e->tab[nl][nc]==23)||(e->tab[nl][nc]==24))	
				return 30;
			else ataque = 90;	
		}
		
	}
	else
	{
		int nl=l+1;
		int nc =c+1;
		//printf("%d %d %d atacado por %d\n",l,nl,c,e->tab[nl][c]);
		while((nl<8)&&(nc<8)&&(e->tab[nl][nc]==0))
		{	nl++;nc++;}
		
		if((nl<8)&&(nc<8)&&( (e->tab[nl][nc]==2)||(e->tab[nl][nc]==7)||(e->tab[nl][nc]==8)
		||(pecaPromovidaP1(e,e->tab[nl][nc]) ) ) )
		{	
			if((e->tab[nl][nc]==7)||(e->tab[nl][nc]==8))	
				return 30;
			else ataque = 90;	
		}
		nl=l-1;
		nc =c+1;
		while((nl>-1)&&(nc<8)&&(e->tab[nl][nc]==0))
		{	nl--;nc++;}
		
		if((nl>-1)&&(nc<8)&&( (e->tab[nl][nc]==2)||(e->tab[nl][nc]==7)||(e->tab[nl][nc]==8)
		||(pecaPromovidaP1(e,e->tab[nl][nc]) ) ) )
		{
			if((e->tab[nl][nc]==7)||(e->tab[nl][nc]==8))	
				return 30;
			else ataque = 90;
		}
		
		nl=l-1;
		nc=c-1;
		while((nc>-1)&&(nl>-1)&&(e->tab[nl][nc]==0))
		{	nc--;nl--;}
		
		if((nc>-1)&&(nl>-1)&&( (e->tab[nl][nc]==2)||(e->tab[nl][nc]==7)||(e->tab[nl][nc]==8)
		||(pecaPromovidaP1(e,e->tab[nl][nc]) ) ) )
		{
			if((e->tab[nl][nc]==7)||(e->tab[nl][nc]==8))	
				return 30;
			else ataque = 90;
		}
		nl=l+1;
		nc=c-1;
		while((nc>-1)&&(nl<8)&&(e->tab[nl][nc]==0))
		{	nc--;nl++;}
		
		if((nc>-1)&&(nl<8)&&( (e->tab[nl][nc]==2)||(e->tab[nl][nc]==7)||(e->tab[nl][nc]==8)
		||(pecaPromovidaP1(e,e->tab[nl][nc]) ) ) )
		{		
			if((e->tab[nl][nc]==7)||(e->tab[nl][nc]==8))	
				return 30;
			else ataque = 90;			
		}	
	}
	return ataque;
}


int ataqueDiagonal(int l,int c,estado *e,int jogador)
{
	int ataque=0;
	if(jogador==1)
	{
		int nl=l+1;
		int nc =c+1;
		//printf("%d %d %d atacado por %d\n",l,nl,c,e->tab[nl][c]);
		while((nl<8)&&(nc<8)&&(e->tab[nl][nc]==0))
		{	nl++;nc++;}
		
		if((nl<8)&&(nc<8)&&( (e->tab[nl][nc]==18)||(e->tab[nl][nc]==23)||(e->tab[nl][nc]==24)
		||(pecaPromovidaP2(e,e->tab[nl][nc]) ) ) )
		{	
			return 1;	
		}
		nl=l-1;
		nc =c+1;
		while((nl>-1)&&(nc<8)&&(e->tab[nl][nc]==0))
		{	nl--;nc++;}
		
		if((nl>-1)&&(nc<8)&&( (e->tab[nl][nc]==18)||(e->tab[nl][nc]==23)||(e->tab[nl][nc]==24)
		||(pecaPromovidaP2(e,e->tab[nl][nc]) ) ) )
		{
			return 1;
		}
		
		nl=l-1;
		nc=c-1;
		while((nc>-1)&&(nl>-1)&&(e->tab[nl][nc]==0))
		{	nc--;nl--;}
		
		if((nc>-1)&&(nl>-1)&&( (e->tab[nl][nc]==18)||(e->tab[nl][nc]==23)||(e->tab[nl][nc]==24)
		||(pecaPromovidaP2(e,e->tab[nl][nc]) ) ) )
		{
			return 1;
		}
		nl=l+1;
		nc=c-1;
		while((nc>-1)&&(nl<8)&&(e->tab[nl][nc]==0))
		{	nc--;nl++;}
		
		if((nc>-1)&&(nl<8)&&( (e->tab[nl][nc]==18)||(e->tab[nl][nc]==23)||(e->tab[nl][nc]==24)
		||(pecaPromovidaP2(e,e->tab[nl][nc]) ) ) )
		{		
			return 1;
					
		}
		
	}
	else
	{
		int nl=l+1;
		int nc =c+1;
		//printf("%d %d %d atacado por %d\n",l,nl,c,e->tab[nl][c]);
		while((nl<8)&&(nc<8)&&(e->tab[nl][nc]==0))
		{	nl++;nc++;}
		
		if((nl<8)&&(nc<8)&&( (e->tab[nl][nc]==2)||(e->tab[nl][nc]==7)||(e->tab[nl][nc]==8)
		||(pecaPromovidaP1(e,e->tab[nl][nc]) ) ) )
		{	
			return 1;	
		}
		nl=l-1;
		nc =c+1;
		while((nl>-1)&&(nc<8)&&(e->tab[nl][nc]==0))
		{	nl--;nc++;}
		
		if((nl>-1)&&(nc<8)&&( (e->tab[nl][nc]==2)||(e->tab[nl][nc]==7)||(e->tab[nl][nc]==8)
		||(pecaPromovidaP1(e,e->tab[nl][nc]) ) ) )
		{
			return 1;
		}
		
		nl=l-1;
		nc=c-1;
		while((nc>-1)&&(nl>-1)&&(e->tab[nl][nc]==0))
		{	nc--;nl--;}
		
		if((nc>-1)&&(nl>-1)&&( (e->tab[nl][nc]==2)||(e->tab[nl][nc]==7)||(e->tab[nl][nc]==8)
		||(pecaPromovidaP1(e,e->tab[nl][nc]) ) ) )
		{
			return 1;
		}
		nl=l+1;
		nc=c-1;
		while((nc>-1)&&(nl<8)&&(e->tab[nl][nc]==0))
		{	nc--;nl++;}
		
		if((nc>-1)&&(nl<8)&&( (e->tab[nl][nc]==2)||(e->tab[nl][nc]==7)||(e->tab[nl][nc]==8)
		||(pecaPromovidaP1(e,e->tab[nl][nc]) ) ) )
		{		
			return 1;
					
		}
		
	}
	
	return ataque;
}

int contraAtaqueLinhaColuna(int l,int c,estado *e,int jogador)
{
	int ataque=0;
	if(jogador==1)
	{
		int nl=l+1;
	
		//printf("%d %d %d atacado por %d\n",l,nl,c,e->tab[nl][c]);
		while((nl<8)&&(e->tab[nl][c]==0))
			nl++;
		
		if((nl<8)&&( (e->tab[nl][c]==18)||(e->tab[nl][c]==19)||(e->tab[nl][c]==20)
		||(pecaPromovidaP2(e,e->tab[nl][c])) ) )
		{	
			if((e->tab[nl][c]==19)||(e->tab[nl][c]==20))	
				return 50;
			else ataque = 90;	
		}
		nl=l-1;
		while((nl>-1)&&(e->tab[nl][c]==0))
			nl--;
		
		if((nl>-1)&&( (e->tab[nl][c]==18)||(e->tab[nl][c]==19)||(e->tab[nl][c]==20)
		||(pecaPromovidaP2(e,e->tab[nl][c])) ) )
		{
			if((e->tab[nl][c]==19)||(e->tab[nl][c]==20))	
				return 50;
			else ataque = 90;
		}
		
		
		int nc=c+1;
		while((nc<8)&&(e->tab[l][nc]==0))
			nc++;
		
		if((nc<8)&&( (e->tab[l][nc]==18)||(e->tab[l][nc]==19)||(e->tab[l][nc]==20)
		||(pecaPromovidaP2(e,e->tab[l][nc])) ) )
		{
			if((e->tab[l][nc]==19)||(e->tab[l][nc]==20))	
				return 50;
			else ataque = 90;
		}
		nc=c-1;
		while((nc>-1)&&(e->tab[l][nc]==0))
			nc--;
		
		if((nc>-1)&&( (e->tab[l][nc]==18)||(e->tab[l][nc]==19)||(e->tab[l][nc]==20)
		||(pecaPromovidaP2(e,e->tab[l][nc])) ) )
		{		
			if((e->tab[l][nc]==19)||(e->tab[l][nc]==20))	
				return 50;
			else ataque = 90;	
		}
		
	}
	else
	{
		int nl=l+1;
		while((nl<8)&&(e->tab[nl][c]==0))
			nl++;
		if((nl<8)&&( (e->tab[nl][c]==2)||(e->tab[nl][c]==3)||(e->tab[nl][c]==4)
		||(pecaPromovidaP1(e,e->tab[nl][c])) ) )
		{		
			if((e->tab[nl][c]==3)||(e->tab[nl][c]==4))	
				return 50;
			else ataque = 90;	
		}	

		nl=l-1;
		while((nl>-1)&&(e->tab[nl][c]==0))
			nl--;
		if((nl>-1)&&( (e->tab[nl][c]==2)||(e->tab[nl][c]==3)||(e->tab[nl][c]==4)
		||(pecaPromovidaP1(e,e->tab[nl][c])) ) )
		{		
			if((e->tab[nl][c]==3)||(e->tab[nl][c]==4))	
				return 50;
			else ataque = 90;	
		}

		

		int nc=c+1;
		while((nc<8)&&(e->tab[l][nc]==0))
			nc++;
		if((nc<8)&&( (e->tab[l][nc]==2)||(e->tab[l][nc]==3)||(e->tab[l][nc]==4)
		||(pecaPromovidaP1(e,e->tab[l][nc])) ) )
		{		
			if((e->tab[l][nc]==3)||(e->tab[l][nc]==4))	
				return 50;
			else ataque = 90;	
		}
		
		nc=c-1;
		while((nc>-1)&&(e->tab[l][nc]==0))
			nc--;
		if((nc>-1)&&( (e->tab[l][nc]==2)||(e->tab[l][nc]==3)||(e->tab[l][nc]==4)
		||(pecaPromovidaP1(e,e->tab[l][nc])) ) )
		{		
			if((e->tab[l][nc]==3)||(e->tab[l][nc]==4))	
				return 50;
			else ataque = 90;	
		}	
	}
	return ataque;
}

int ataqueLinhaColuna(int l,int c,estado *e,int jogador)
{
	int ataque=0;
	if(jogador==1)
	{
		int nl=l+1;
	
		//printf("%d %d %d atacado por %d\n",l,nl,c,e->tab[nl][c]);
		while((nl<8)&&(e->tab[nl][c]==0))
			nl++;
		
		if((nl<8)&&( (e->tab[nl][c]==18)||(e->tab[nl][c]==19)||(e->tab[nl][c]==20)
		||(pecaPromovidaP2(e,e->tab[nl][c])) ) )
		{	
			return 1;	
		}
		nl=l-1;
		while((nl>-1)&&(e->tab[nl][c]==0))
			nl--;
		
		if((nl>-1)&&( (e->tab[nl][c]==18)||(e->tab[nl][c]==19)||(e->tab[nl][c]==20)
		||(pecaPromovidaP2(e,e->tab[nl][c])) ) )
		{
			return 1;
		}
		
		
		int nc=c+1;
		while((nc<8)&&(e->tab[l][nc]==0))
			nc++;
		
		if((nc<8)&&( (e->tab[l][nc]==18)||(e->tab[l][nc]==19)||(e->tab[l][nc]==20)
		||(pecaPromovidaP2(e,e->tab[l][nc])) ) )
		{
			return 1;
		}
		nc=c-1;
		while((nc>-1)&&(e->tab[l][nc]==0))
			nc--;
		
		if((nc>-1)&&( (e->tab[l][nc]==18)||(e->tab[l][nc]==19)||(e->tab[l][nc]==20)
		||(pecaPromovidaP2(e,e->tab[l][nc])) ) )
		{		
			return 1;
					
		}
		
	}
	else
	{
		int nl=l+1;
		while((nl<8)&&(e->tab[nl][c]==0))
			nl++;
		if((nl<8)&&( (e->tab[nl][c]==2)||(e->tab[nl][c]==3)||(e->tab[nl][c]==4)
		||(pecaPromovidaP1(e,e->tab[nl][c])) ) )
			return 1;	

		nl=l-1;
		while((nl>-1)&&(e->tab[nl][c]==0))
			nl--;
		if((nl>-1)&&( (e->tab[nl][c]==2)||(e->tab[nl][c]==3)||(e->tab[nl][c]==4)
		||(pecaPromovidaP1(e,e->tab[nl][c])) ) )
			return 1;

		

		int nc=c+1;
		while((nc<8)&&(e->tab[l][nc]==0))
			nc++;
		if((nc<8)&&( (e->tab[l][nc]==2)||(e->tab[l][nc]==3)||(e->tab[l][nc]==4)
		||(pecaPromovidaP1(e,e->tab[l][nc])) ) )
			return 1;
		
		nc=c-1;
		while((nc>-1)&&(e->tab[l][nc]==0))
			nc--;
		if((nc>-1)&&( (e->tab[l][nc]==2)||(e->tab[l][nc]==3)||(e->tab[l][nc]==4)
		||(pecaPromovidaP1(e,e->tab[l][nc])) ) )
			return 1;	
	}
	
	return ataque;
}

int ataqueRei(int l,int c,estado *e,int jogador)
{
	int ataque=0;
	if(jogador==1)
	{
		if((l+1<8)&&(e->tab[l+1][c]==17))
			return 1;
		if((l-1>-1)&&(e->tab[l-1][c]==17))
			return 1;
		if((c+1<8)&&(e->tab[l][c+1]==17))
			return 1;
		if((c-1>-1)&&(e->tab[l][c-1]==17))
			return 1;
		if((c-1>-1)&&(l-1>-1)&&(e->tab[l-1][c-1]==17))
			return 1;
		if((c+1<8)&&(l+1<8)&&(e->tab[l+1][c+1]==17))
			return 1;
		if((c-1>-1)&&(l+1<8)&&(e->tab[l+1][c-1]==17))
			return 1;
		if((l-1>-1)&&(c+1<8)&&(e->tab[l-1][c+1]==17))
			return 1;
	}
	else
	{
		if((l+1<8)&&(e->tab[l+1][c]==1))
			return 1;
		if((l-1>-1)&&(e->tab[l-1][c]==1))
			return 1;
		if((c+1<8)&&(e->tab[l][c+1]==1))
			return 1;
		if((c-1>-1)&&(e->tab[l][c-1]==1))
			return 1;
		if((c-1>-1)&&(l-1>-1)&&(e->tab[l-1][c-1]==1))
			return 1;
		if((c+1<8)&&(l+1<8)&&(e->tab[l+1][c+1]==1))
			return 1;
		if((c-1>-1)&&(l+1<8)&&(e->tab[l+1][c-1]==1))
			return 1;
		if((l-1>-1)&&(c+1<8)&&(e->tab[l-1][c+1]==1))
			return 1;
	}
	return ataque;
}

int ataqueCavalo(int l,int c,estado *e,int jogador)
{
	int ataque=0;
	if(jogador==1)
	{
		int nl=l+2;
		int nc=c+1;
		
		if((nl<8)&&(nc<8)&&( (e->tab[nl][nc]==21)||(e->tab[nl][nc]==22) ) )
		{	
			return 1;	
		}

		nl=l+2;
		nc=c-1;
		
		if((nl<8)&&(nc>-1)&&( (e->tab[nl][nc]==21)||(e->tab[nl][nc]==22) ) )
		{	
			return 1;	
		}

		nl=l-2;
		nc=c+1;
		
		if((nl>-1)&&(nc<8)&&( (e->tab[nl][nc]==21)||(e->tab[nl][nc]==22) ) )
		{	
			return 1;	
		}

		nl=l-2;
		nc=c-1;
		
		if((nl>-1)&&(nc>-1)&&( (e->tab[nl][nc]==21)||(e->tab[nl][nc]==22) ) )
		{	
			return 1;	
		}

		nl=l-1;
		nc=c-2;
		
		if((nl>-1)&&(nc>-1)&&( (e->tab[nl][nc]==21)||(e->tab[nl][nc]==22) ) )
		{	
			return 1;	
		}
		nl=l+1;
		nc=c-2;
		
		if((nl<8)&&(nc>-1)&&( (e->tab[nl][nc]==21)||(e->tab[nl][nc]==22) ) )
		{	
			return 1;	
		}

		nl=l-1;
		nc=c+2;
		
		if((nl>-1)&&(nc<8)&&( (e->tab[nl][nc]==21)||(e->tab[nl][nc]==22) ) )
		{	
			return 1;	
		}
		nl=l+1;
		nc=c+2;
		
		if((nl<8)&&(nc<8)&&( (e->tab[nl][nc]==21)||(e->tab[nl][nc]==22) ) )
		{	
			return 1;	
		}	
	}
	else
	{
		int nl=l+2;
		int nc=c+1;
		
		if((nl<8)&&(nc<8)&&( (e->tab[nl][nc]==5)||(e->tab[nl][nc]==6) ) )
		{	
			return 1;	
		}

		nl=l+2;
		nc=c-1;
		
		if((nl<8)&&(nc>-1)&&( (e->tab[nl][nc]==5)||(e->tab[nl][nc]==6) ) )
		{	
			return 1;	
		}

		nl=l-2;
		nc=c+1;
		
		if((nl>-1)&&(nc<8)&&( (e->tab[nl][nc]==5)||(e->tab[nl][nc]==6) ) )
		{	
			return 1;	
		}

		nl=l-2;
		nc=c-1;
		
		if((nl>-1)&&(nc>-1)&&( (e->tab[nl][nc]==5)||(e->tab[nl][nc]==6) ) )
		{	
			return 1;	
		}

		nl=l-1;
		nc=c-2;
		
		if((nl>-1)&&(nc>-1)&&( (e->tab[nl][nc]==5)||(e->tab[nl][nc]==6) ) )
		{	
			return 1;	
		}
		nl=l+1;
		nc=c-2;
		
		if((nl<8)&&(nc>-1)&&( (e->tab[nl][nc]==5)||(e->tab[nl][nc]==6) ) )
		{	
			return 1;	
		}

		nl=l-1;
		nc=c+2;
		
		if((nl>-1)&&(nc<8)&&( (e->tab[nl][nc]==5)||(e->tab[nl][nc]==6) ) )
		{	
			return 1;	
		}
		nl=l+1;
		nc=c+2;
		
		if((nl<8)&&(nc<8)&&( (e->tab[nl][nc]==5)||(e->tab[nl][nc]==6) ) )
		{	
			return 1;	
		}	
	}
	
	return ataque;
}
int posicaoNaoAtacada(int l,int c,estado *e,int jogador)
{
	if(ataquePeao(l,c,e,jogador))
	{
		return 0;	
	}
	if(ataqueLinhaColuna(l,c,e,jogador)) //Dama e Torre
	{

		return 0;
	}
	if(ataqueRei(l,c,e,jogador))
	{

		return 0;
	}
	if(ataqueDiagonal(l,c,e,jogador))    //Dama e Bispo
		return 0;
	if(ataqueCavalo(l,c,e,jogador))
		return 0;
	
	return 1;
}

void destroiListaFilhos(lista *l)
{
	if(l->head==NULL)
		return;

	no *aux = l->head;
	l->head = aux->prox;
	l->tam = l->tam-1;
	destroiEstado(aux->o);
	free(aux);

	destroiListaFilhos(l);
}


void destroiEstado(estado *e)
{
	destroiListaFilhos(e->filhos);
	free(e->filhos);
	free(e);
}


void mostraEstado(estado *e)
{
	if(!e) return;

	int i,j;
	printf("+------------------------+");
	for(i=7;i>=0;i--)
	{
		printf("\n");				
		for(j=7;j>=0;j--)
		{	
		if((e->tab[i][j])&&(j==7))
		{
			if(e->tab[i][j]>9)
				printf("+ %d",e->tab[i][j]);
			else
				printf("+ %d ",e->tab[i][j]);
		}		
		else if((e->tab[i][j])&&(j==0))
		{
			if(e->tab[i][j]>9)
				printf(" %d+",e->tab[i][j]);
			else
				printf(" %d +",e->tab[i][j]);
		}		
		else if(e->tab[i][j])
		{
			if(e->tab[i][j]>9)
				printf(" %d",e->tab[i][j]);
			else
				printf(" %d ",e->tab[i][j]);
		}
		else if(e->tab[i][j]==0)
		{
			if((j+i)%2==0)
			{
				if(j==7)
					printf("+   ");
				else if(j==0)
					printf("   +");
				else
					printf("   ");
			}
			else
			{
				if(j==7)
					printf("+|||");
				else if(j==0)
					printf("|||+");
				else 
					printf("|||");
				}
			}				
							
		}
				//printf("\n+                        +\n");
	}
	printf("\n+------------------------+\n");


	//informaEstado(e);
}

void mostraEstadoFile(estado *e,FILE *fp)
{
	if(!e) return;

	int i,j;
	fprintf(fp,"+------------------------+");
	for(i=7;i>=0;i--)
	{
		fprintf(fp,"\n");				
		for(j=7;j>=0;j--)
		{	
		if((e->tab[i][j])&&(j==7))
		{
			if(e->tab[i][j]>9)
				fprintf(fp,"+ %d",e->tab[i][j]);
			else
				fprintf(fp,"+ %d ",e->tab[i][j]);
		}		
		else if((e->tab[i][j])&&(j==0))
		{
			if(e->tab[i][j]>9)
				fprintf(fp," %d+",e->tab[i][j]);
			else
				fprintf(fp," %d +",e->tab[i][j]);
		}		
		else if(e->tab[i][j])
		{
			if(e->tab[i][j]>9)
				fprintf(fp," %d",e->tab[i][j]);
			else
				fprintf(fp," %d ",e->tab[i][j]);
		}
		else if(e->tab[i][j]==0)
		{
			if((j+i)%2==0)
			{
				if(j==7)
					fprintf(fp,"+   ");
				else if(j==0)
					fprintf(fp,"   +");
				else
					fprintf(fp,"   ");
			}
			else
			{
				if(j==7)
					fprintf(fp,"+|||");
				else if(j==0)
					fprintf(fp,"|||+");
				else 
					fprintf(fp,"|||");
				}
			}				
							
		}
				//printf("\n+                        +\n");
	}
	fprintf(fp,"\n+------------------------+\n");


	//informaEstado(e);
}

void mostraFilhos(estado *e)
{
	int tam=e->filhos->tam;
	if(tam==0)
		printf("Lista Vazia");
	else
	{	
		no *aux = e->filhos->head;
		int i=0;
		estado *filho; filho = aux->o;
		for(i;i<tam;i++)
		{
			filho = aux->o;
			mostraEstado(filho);
			printf("h(filho) = %d\n",filho->h);
			printf("h_corrente(filho) = %d\n",fatMaterial(filho));
			printf("material(filho) + distancia(R1,R2) = %d\n",material(filho));
			aux=aux->prox;
		}						
	}	
	//noSet=0;	
}

int estadoTerminal(estado *e)
{
	pos r1 = getPos(e,1);
	pos r2 = getPos(e,17);
	int atacadoRei1 = !posicaoNaoAtacada(r1.l,r1.c,e,1);

	//printf("Rei 1 atacado = %d\n",atacadoRei1);
	
	int atacadoRei2 = !posicaoNaoAtacada(r2.l,r2.c,e,2);

	//printf("Rei 2 atacado = %d\n",atacadoRei2);

	//printf("E->filhos->tam = %d\n",e->filhos->tam);
	if((e->filhos->tam==0)&&(atacadoRei1||atacadoRei2))
		return 1;
	else if((e->filhos->tam==0)&&(!atacadoRei1&&!atacadoRei2))
		return -1; //empate por afogamento
	else if(qtLances==0)
		return -1; //empate por fim de quantidade de lances com reisozinho
	else
		return 0;
}

int verificaMovimento(estado *e,int pl,int pc,int ml,int mc)
{
	if( ((pl<8)||(pl>-1)) && ((pc<8)||(pc>-1)) 
	&&  ((ml<8)||(ml>-1)) && ((mc<8)||(mc>-1)) )
	
		return 1;
	else
		return 0;
}

byte **constroiTabuleiroTemporario()
{
	byte **tabTemp = (byte **)malloc(sizeof(byte*)*8);

	int i,j;
	for(i=0;i<8;i++)
		tabTemp[i]=(byte *)malloc(sizeof(byte)*8);

	return tabTemp;	
}

void copia(byte **tabuleiro,estado *e)
{
	int i,j;
	for(i=0;i<8;i++)
		for(j=0;j<8;j++)
			tabuleiro[i][j]=e->tab[i][j];
}

void destroiTabTemp(byte **tab)
{
	int i;
	for(i=0;i<8;i++)
		free(tab[i]);
	free(tab);
}

int compara(estado *e,byte **tab)
{
	int i,j;
	for(i=0;i<8;i++)
		for(j=0;j<8;j++)
			if(tab[i][j]!=e->tab[i][j])
				return 0;

	return 1;
}

estado *contemNaLista(byte **tab,lista *filhos)
{
	no * aux = filhos->head;
	int tam=filhos->tam;
	
	int i=0;
	for(i;i<tam;i++)
	{
		if(compara(aux->o,tab))
			return aux->o;
		aux = aux->prox;
	}
		

	return NULL;
}

int jogadorDominaCentro(estado *e)
{
	int i,j;
	int p1,p2;
	p1=0;p2=0;
	for(i=2;i<6;i++)
	{
		for(j=2;j<6;j++)
		{
			if(e->tab[i][j]>0)
			{
				if(e->tab[i][j]<17)
					p1++;
				else
					p2++;
			}
		}
	}
	
	if(p1>p2)
		return 1;
	else if(p2>p1)
		return 2;
	else if(p2==p1)
		return 0;
	
}


int peaoDobrado(estado *e,pos peao,int player)
{
	int d=0;

	int l = peao.l;
	int c = peao.c;

	if(player==1)
	{
		if( ((l+1)<8) &&(e->tab[l+1][c]>8)&&(e->tab[l+1][c]<=16))
			d=1;
		if( ((l-1)>-1)&&(e->tab[l-1][c]>8)&&(e->tab[l-1][c]<=16))
			d=1;
		if( ((l+2)<8) &&(e->tab[l+2][c]>8)&&(e->tab[l+2][c]<=16))
			d=1;
		if( ((l-2)>-1)&&(e->tab[l-2][c]>8)&&(e->tab[l-2][c]<=16))
			d=1;
	}
	else
	{
		if( ((l+1)<8) &&(e->tab[l+1][c]>24)&&(e->tab[l+1][c]<=32))
			d=1;
		if( ((l-1)>-1)&&(e->tab[l-1][c]>24)&&(e->tab[l-1][c]<=32))
			d=1;
		if( ((l+2)<8) &&(e->tab[l+2][c]>24)&&(e->tab[l+2][c]<=32))
			d=1;
		if( ((l-2)>-1)&&(e->tab[l-2][c]>24)&&(e->tab[l-2][c]<=32))
			d=1;
	}

	return d;
}

int pontuaTorre(estado *e,pos torre)
{
	/*int movel = 0;
	int movec = 0;
		
	int l = torre.l;
	int c = torre.c;
	
	int nl = l+1;	
	while((nl<8)&&(e->tab[nl][c]==0))
	{	movel++; nl++; }

	nl = l-1;
	while((nl>-1)&&(e->tab[nl][c]==0))
	{	movel++; nl--; }
	
	int nc = c+1;
	while((nc<8)&&(e->tab[l][nc]==0))
	{	movec++; nc++; }

	nc = c-1;
	while((nc>-1)&&(e->tab[l][nc]==0))
	{	movec++; nc--; }

	if( (movel>=4)||(movec>=4) )
	{
		if(momentoJogo==3)
			return 50;
		else 
			return 40;
	}
	else
		return 40;*/

	return 50;
}

int mod(int a)
{
	if(a<0)
		return -a;
	else 
		return a;
}


int material(estado *e)
{
		int fat1=0;
		int fat2=0;
		int i;

		if((e->damaP1.l==-1)&&(e->torre1P1.l==-1)&&(e->torre2P1.l==-1))
		{
			//printf("Entrou 2 \n");
			int l = e->reiP2.l;
			int c = e->reiP2.c;
			int lo = e->reiP1.l;
			int co = e->reiP1.c;

			int a = mod(l-lo);
			int b = mod(c-co);

			int dist;
			if(a>b)
				dist = a;
			else
				dist = b;

			fat1 = fat1 + dist;

			int distC1 = co;
			int distC2 = 7 - co;
			int distC;

			if(distC1>distC2)
				distC = distC1;
			else
				distC = distC2;

			int distL1 = lo;
			int distL2 = 7 - lo;
			int distL;

			if(distL1>distL2)
				distL = distL1;
			else
				distL = distL2;


			if(distL<distC)
				fat1 = fat1 + (distL);
			else
				fat1 = fat1 + (distC);
			
		}

		if((e->damaP2.l==-1)&&(e->torre1P2.l==-1)&&(e->torre2P2.l==-1))
		{
		
			int l = e->reiP1.l;
			int c = e->reiP1.c;
			int lo = e->reiP2.l;
			int co = e->reiP2.c;

			int a = mod(l-lo);
			int b = mod(c-co);
			int dist;
			if(a>b)
				dist = a;
			else
				dist = b;

			fat2 = fat2 + dist;	

			int distC1 = co;
			int distC2 = 7 - co;
			int distC;

			if(distC1>distC2)
				distC = distC1;
			else
				distC = distC2;

			int distL1 = lo;
			int distL2 = 7 - lo;
			int distL;

			if(distL1>distL2)
				distL = distL1;
			else
				distL = distL2;

	
		}
	       
		for(i=0;i<8;i++)
		{
			if(e->peaoP1[i].l != -1)
			{
				if(promovidoP1(e,i))
					fat1 = fat1 + 90;
				else
				{
					//if(e->peaoP1[i].l==4)
					//	fat1 = fat1 + 15;
					//if(e->peaoP1[i].l==5)
					//	fat1 = fat1 + 15;
					//else if(e->peaoP1[i].l==6)
					//	fat1 = fat1 + 30;
					//else if(e->peaoP1[i].l==7)
					//	fat1 = fat1 + 90;
					//else 
						fat1 = fat1 + 10;
				}
			}
		}

		for(i=0;i<8;i++)
		{
			if(e->peaoP2[i].l != -1)
			{
				if(promovidoP2(e,i))
					fat2 = fat2 + 90;
				else
				{
					//if(e->peaoP2[i].l==3)
					//	fat2 = fat2 + 15;
					//if(e->peaoP2[i].l==2)
						fat2 = fat2 + 15;
					////else if(e->peaoP2[i].l==1)
					//	fat2 = fat2 + 30;
					//else if(e->peaoP2[i].l==0)
					//	fat2 = fat2 + 90;
					//else 
						fat2 = fat2 + 10;
				}
			}
		}

		if(e->torre1P1.l != -1)
			fat1 = fat1 + 50;//pontuaTorre(e,e->torre1P1);
		if(e->torre1P2.l != -1)
			fat2 = fat2 + 50;//pontuaTorre(e,e->torre1P2);
		if(e->torre2P1.l != -1)
			fat1 = fat1 + 50;//pontuaTorre(e,e->torre2P1);
		if(e->torre2P2.l != -1)
			fat2 = fat2 + 50;//pontuaTorre(e,e->torre2P2);

		if(e->cavalo1P1.l != -1)
			fat1 = fat1 + 30;
		if(e->cavalo1P2.l != -1)
			fat2 = fat2 + 30;
		if(e->cavalo2P1.l != -1)
			fat1 = fat1 + 30;
		if(e->cavalo2P2.l != -1)
			fat2 = fat2 + 30;

		if(e->bispo1P1.l != -1)
			fat1 = fat1 + 30;
		if(e->bispo1P2.l != -1)
			fat2 = fat2 + 30;
		if(e->bispo2P1.l != -1)
			fat1 = fat1 + 30;
		if(e->bispo2P2.l != -1)
			fat2 = fat2 + 30;

		if(e->damaP1.l != -1)
			fat1 = fat1 + 90;
		if(e->damaP2.l != -1)
			fat2 = fat2 + 90;

		return fat1-fat2;
}

int fatMaterial(estado *e)
{
	int fat1 = 0;
	int fat2 = 0; 


	if((e->reiP1Mexeu)||((e->torre1P1Mexeu)&&(e->torre2P1Mexeu)))
		fat2=fat2+5;
	if((e->reiP2Mexeu)||((e->torre1P2Mexeu)&&(e->torre2P2Mexeu)))
		fat1=fat1+5;

	if(momentoJogo==3)
	{	   
		if((e->damaP1.l==-1)&&(e->torre1P1.l==-1)&&(e->torre2P1.l==-1)&&
				(nenhumaPromocao(e,1)))
		{
			int l = e->reiP2.l;
			int c = e->reiP2.c;
			int lo = e->reiP1.l;
			int co = e->reiP1.c;

			int a = mod(l-lo);
			int b = mod(c-co);
			
			int dist;
			if(a>b)
				dist = a;
			else
				dist = b;

			fat1 = fat1 + dist;

			int distC1 = co;
			int distC2 = 7 - co;
			int distC;

			if(distC1<distC2)
				distC = distC1;
			else
				distC = distC2;

			int distL1 = lo;
			int distL2 = 7 - lo;
			int distL;

			if(distL1<distL2)
				distL = distL1;
			else
				distL = distL2;

			if(distL<distC)
				fat1 = fat1 + (distL);
			else
				fat1 = fat1 + (distC);

			if(!posicaoNaoAtacada(e->reiP1.l,e->reiP1.c,e,1))
				fat2 = fat2+10;

		}

		if((e->damaP2.l==-1)&&(e->torre1P2.l==-1)&&(e->torre2P2.l==-1)&&
				(nenhumaPromocao(e,2)))
		{
			
			int l = e->reiP1.l;
			int c = e->reiP1.c;
			int lo = e->reiP2.l;
			int co = e->reiP2.c;

			int a = mod(l-lo);
			int b = mod(c-co);
			
			int dist;
			if(a>b)
				dist = a;
			else
				dist = b;

			fat2 = fat2 + dist;

			int distC1 = co;
			int distC2 = 7 - co;
			int distC;

			if(distC1<distC2)
				distC = distC1;
			else
				distC = distC2;

			int distL1 = lo;
			int distL2 = 7 - lo;
			int distL;

			if(distL1<distL2)
				distL = distL1;
			else
				distL = distL2;

			if(distL<distC)
				fat2 = fat2 + (distL);
			else
				fat2 = fat2 + (distC);	

			if(!posicaoNaoAtacada(e->reiP2.l,e->reiP2.c,e,2))
				fat1 = fat1+10;
		}
			
		if(e->reiP1.l==-1)
			return -2000;
		if(e->reiP2.l==-1)
			return  2000;

		int i;
	       
		for(i=0;i<8;i++)
		{
			if(e->peaoP1[i].l != -1)
			{
				if(promovidoP1(e,i))
					fat1 = fat1 + 90;
				//if(e->peaoP1[i].l==4)
				//	fat1 = fat1 + 15;
				else if(e->peaoP1[i].l==5)
					fat1 = fat1 + 15;
				else if(e->peaoP1[i].l==6)
					fat1 = fat1 + 30;
				else if(e->peaoP1[i].l==7)
					fat1 = fat1 + 90;
				else 
					fat1 = fat1 + 10;
			}
		}

		for(i=0;i<8;i++)
		{
			if(e->peaoP2[i].l != -1)
			{
				if(promovidoP2(e,i))
					fat2 = fat2 + 90;
				//else if(e->peaoP2[i].l==3)
				//	fat2 = fat2 + 15;
				else if(e->peaoP2[i].l==2)
					fat2 = fat2 + 15;
				else if(e->peaoP2[i].l==1)
					fat2 = fat2 + 30;
				else if(e->peaoP2[i].l==0)
					fat2 = fat2 + 90;
				else
					fat2 = fat2 + 10;
			}
		}
		if(e->torre1P1.l != -1)
			fat1 = fat1 + 50;//pontuaTorre(e,e->torre1P1);
		if(e->torre1P2.l != -1)
			fat2 = fat2 + 50;//pontuaTorre(e,e->torre1P2);
		if(e->torre2P1.l != -1)
			fat1 = fat1 + 50;//pontuaTorre(e,e->torre2P1);
		if(e->torre2P2.l != -1)
			fat2 = fat2 + 50;//pontuaTorre(e,e->torre2P2);

		if(e->cavalo1P1.l != -1)
			fat1 = fat1 + 30;
		if(e->cavalo1P2.l != -1)
			fat2 = fat2 + 30;
		if(e->cavalo2P1.l != -1)
			fat1 = fat1 + 30;
		if(e->cavalo2P2.l != -1)
			fat2 = fat2 + 30;

		if(e->bispo1P1.l != -1)
			fat1 = fat1 + 35;
		if(e->bispo1P2.l != -1)
			fat2 = fat2 + 35;
		if(e->bispo2P1.l != -1)
			fat1 = fat1 + 35;
		if(e->bispo2P2.l != -1)
			fat2 = fat2 + 35;

		if(e->damaP1.l != -1)
			fat1 = fat1 + 90;
		if(e->damaP2.l != -1)
			fat2 = fat2 + 90;


		e->fatCorrente = fat1 - fat2;
	}

	else
	{	
		if(ataquePeao(e->cavalo1P1.l,e->cavalo1P1.c,e,1))
			fat1=fat1-20;
		if(ataquePeao(e->cavalo2P1.l,e->cavalo2P1.c,e,1))
			fat1=fat1-20;
		if(ataquePeao(e->cavalo1P2.l,e->cavalo1P2.c,e,2))
			fat2=fat2-20;
		if(ataquePeao(e->cavalo2P2.l,e->cavalo2P2.c,e,2))
			fat2=fat2-20;

		int dominador = jogadorDominaCentro(e);   

		if(dominador==0)
		{
			if(e->reiP1.l==-1)
				return -2000;
			if(e->reiP2.l==-1)
				return  2000;

			int i;
		       
			for(i=0;i<8;i++)
			{
				if(e->peaoP1[i].l != -1)
				{
					if(promovidoP1(e,i))
						fat1 = fat1 + 90;
					//else if(e->peaoP1[i].l==4)
					//	fat1 = fat1 + 15;
					//else if(e->peaoP1[i].l==5)
					//	fat1 = fat1 + 15;
					//else if(e->peaoP1[i].l==6)
					//	fat1 = fat1 + 30;
					//else if(e->peaoP1[i].l==7)
					//	fat1 = fat1 + 90;
					else
					{
						if(peaoDobrado(e,e->peaoP1[i],1))
							fat1 = fat1 + 8;					
						else
							fat1 = fat1 + 10;
					}		
				}
			}

			for(i=0;i<8;i++)
			{
				if(e->peaoP2[i].l != -1)
				{
					if(promovidoP2(e,i))
						fat2 = fat2 + 90;
					//else if(e->peaoP2[i].l==3)
					//	fat2 = fat2 + 15;
					//else if(e->peaoP2[i].l==2)
					//	fat2 = fat2 + 15;
					//else if(e->peaoP2[i].l==1)
					//	fat2 = fat2 + 30;
					//else if(e->peaoP2[i].l==0)
					//	fat2 = fat2 + 90;
					else
					{
						if(peaoDobrado(e,e->peaoP2[i],2))
							fat2 = fat2 + 8;					
						else
							fat2 = fat2 + 10;
					}		
				}
			}

			if(e->torre1P1.l != -1)
				fat1 = fat1 + pontuaTorre(e,e->torre1P1);
			if(e->torre1P2.l != -1)
				fat2 = fat2 + pontuaTorre(e,e->torre1P2);
			if(e->torre2P1.l != -1)
				fat1 = fat1 + pontuaTorre(e,e->torre2P1);
			if(e->torre2P2.l != -1)
				fat2 = fat2 + pontuaTorre(e,e->torre2P2);

			if(e->cavalo1P1.l != -1)
				fat1 = fat1 + 40;
			if(e->cavalo1P2.l != -1)
				fat2 = fat2 + 40;
			if(e->cavalo2P1.l != -1)
				fat1 = fat1 + 40;
			if(e->cavalo2P2.l != -1)
				fat2 = fat2 + 40;

			if(e->bispo1P1.l != -1)
				fat1 = fat1 + 35;
			if(e->bispo1P2.l != -1)
				fat2 = fat2 + 35;
			if(e->bispo2P1.l != -1)
				fat1 = fat1 + 35;
			if(e->bispo2P2.l != -1)
				fat2 = fat2 + 35;

			if(e->damaP1.l != -1)
				fat1 = fat1 + 90;
			if(e->damaP2.l != -1)
				fat2 = fat2 + 90;


			e->fatCorrente = fat1 - fat2;
		}
		else
		{
			if(e->reiP1.l==-1)
				return -2000;
			if(e->reiP2.l==-1)
				return  2000;

			int i;
		       
			for(i=0;i<8;i++)
			{
				if(e->peaoP1[i].l != -1)
				{
					if(promovidoP1(e,i))
						fat1 = fat1 + 90;
					//else if(e->peaoP1[i].l==4)
					//	fat1 = fat1 + 15;
					//else if(e->peaoP1[i].l==5)
					//	fat1 = fat1 + 15;
					//else if(e->peaoP1[i].l==6)
					//	fat1 = fat1 + 30;
					//else if(e->peaoP1[i].l==7)
					//	fat1 = fat1 + 90;
					else if((dominador==1)&&(e->peaoP1[i].l>=2)&&(e->peaoP1[i].l<=5)
							&&(e->peaoP1[i].c>=2)&&(e->peaoP1[i].c<=5))
					{
						if(peaoDobrado(e,e->peaoP1[i],1))
							fat1 = fat1 + 9;					
						else
							fat1 = fat1 + 15;
					}
					else
					{
						if(peaoDobrado(e,e->peaoP1[i],1))
							fat1 = fat1 + 8;					
						else
							fat1 = fat1 + 10;
					}
				}
			}

			for(i=0;i<8;i++)
			{
				if(e->peaoP2[i].l != -1)
				{
					if(promovidoP2(e,i))
						fat2 = fat2 + 90;
					//else if(e->peaoP2[i].l==3)
					//	fat2 = fat2 + 15;
					//else if(e->peaoP2[i].l==2)
					//	fat2 = fat2 + 15;
					//else if(e->peaoP2[i].l==1)
					//	fat2 = fat2 + 30;
					//else if(e->peaoP2[i].l==0)
					//	fat2 = fat2 + 90;
					else if((dominador==2)&&(e->peaoP2[i].l>=2)&&(e->peaoP2[i].l<=5)
							&&(e->peaoP2[i].c>=2)&&(e->peaoP2[i].c<=5))
					{
						if(peaoDobrado(e,e->peaoP2[i],2))
							fat2 = fat2 + 9;					
						else
							fat2 = fat2 + 15;
					}
					else
					{
						if(peaoDobrado(e,e->peaoP2[i],2))
							fat2 = fat2 + 8;					
						else
							fat2 = fat2 + 10;
					}
				}
			}

			if(e->torre1P1.l != -1)
			{
				if((dominador==1)&&(e->torre1P1.l>=2)&&(e->torre1P1.l<=5)
					&&(e->torre1P1.c>=2)&&(e->torre1P1.c<=5))
					fat1 = fat1 + 70;
				else
					fat1 = fat1 + pontuaTorre(e,e->torre1P1);
			}
			if(e->torre1P2.l != -1)
			{
				if((dominador==2)&&(e->torre1P2.l>=2)&&(e->torre1P2.l<=5)
					&&(e->torre1P2.c>=2)&&(e->torre1P2.c<=5))
					fat2 = fat2 + 70;
				else
					fat2 = fat2 + pontuaTorre(e,e->torre1P2);
			}
			if(e->torre2P1.l != -1)
			{
				if((dominador==1)&&(e->torre2P1.l>=2)&&(e->torre2P1.l<=5)
					&&(e->torre2P1.c>=2)&&(e->torre2P1.c<=5))
					fat1 = fat1 + 70;
				else
					fat1 = fat1 + pontuaTorre(e,e->torre2P1);
			}
			if(e->torre2P2.l != -1)
			{
				if((dominador==2)&&(e->torre2P2.l>=2)&&(e->torre2P2.l<=5)
					&&(e->torre2P2.c>=2)&&(e->torre2P2.c<=5))
					fat2 = fat2 + 70;
				else
					fat2 = fat2 + pontuaTorre(e,e->torre2P2);
			}

			if(e->cavalo1P1.l != -1)
			{
				if((dominador==1)&&(e->cavalo1P1.l>=2)&&(e->cavalo1P1.l<=5)
					&&(e->cavalo1P1.c>=2)&&(e->cavalo1P1.c<=5))
					fat1 = fat1 + 50;
				else
					fat1 = fat1 + 40;
			}
			if(e->cavalo1P2.l != -1)
			{
				if((dominador==2)&&(e->cavalo1P2.l>=2)&&(e->cavalo1P2.l<=5)
					&&(e->cavalo1P2.c>=2)&&(e->cavalo1P2.c<=5))
					fat2 = fat2 + 50;
				else
					fat2 = fat2 + 40;
			}
			if(e->cavalo2P1.l != -1)
			{
				if((dominador==1)&&(e->cavalo2P1.l>=2)&&(e->cavalo2P1.l<=5)
					&&(e->cavalo2P1.c>=2)&&(e->cavalo2P1.c<=5))
					fat1 = fat1 + 50;
				else
					fat1 = fat1 + 40;
			}
			if(e->cavalo2P2.l != -1)
			{
				if((dominador==2)&&(e->cavalo2P2.l>=2)&&(e->cavalo2P2.l<=5)
					&&(e->cavalo2P2.c>=2)&&(e->cavalo2P2.c<=5))
					fat2 = fat2 + 50;
				else
					fat2 = fat2 + 40;
			}

			if(e->bispo1P1.l != -1)
			{
				if((dominador==1)&&(e->bispo1P1.l>=2)&&(e->bispo1P1.l<=5)
					&&(e->bispo1P1.c>=2)&&(e->bispo1P1.c<=5))
					fat1 = fat1 + 45;
				else
					fat1 = fat1 + 35;
			}
			if(e->bispo1P2.l != -1)
			{
				if((dominador==2)&&(e->bispo1P2.l>=2)&&(e->bispo1P2.l<=5)
					&&(e->bispo1P2.c>=2)&&(e->bispo1P2.c<=5))
					fat2 = fat2 + 45;
				else
					fat2 = fat2 + 35;
			}
			if(e->bispo2P1.l != -1)
			{
				if((dominador==1)&&(e->bispo2P1.l>=2)&&(e->bispo2P1.l<=5)
					&&(e->bispo2P1.c>=2)&&(e->bispo2P1.c<=5))
					fat1 = fat1 + 45;
				else
					fat1 = fat1 + 35;
			}
			if(e->bispo2P2.l != -1)
			{
				if((dominador==2)&&(e->bispo2P2.l>=2)&&(e->bispo2P2.l<=5)
					&&(e->bispo2P2.c>=2)&&(e->bispo2P2.c<=5))
					fat2 = fat2 + 45;
				else
					fat2 = fat2 + 35;
			}

			if(e->damaP1.l != -1)
				fat1 = fat1 + 90;
			if(e->damaP2.l != -1)
				fat2 = fat2 + 90;


			e->fatCorrente = fat1 - fat2;
		}
	}
	return e->fatCorrente;
}

int fat(estado *e)
{
	e->h = fatMaterial(e);
	return e->h;
}


int h(estado *e,int final,int player)
{
	if( (final==1)&&(player==1) )
	{	e->h=-2000;return -2000; }
	else if( (final==1)&&(player!=1) )
	{   	e->h= 2000;return  2000; }
	else if(final==-1)
	{   	e->h= 0;return  0; }

	return fat(e); 
}


 
int min(int a,int b)
{
	if(a<b)
		return a;
	else return b;
}



int max(int a,int b)
{
	if(a>b)
		return a;
	else return b;
}


//Russel &Norvig																																																																																																																																						
int alfabeta(estado *e,int depth,int alfa,int beta,int player)
{	
	constroiFilhos(e,player);
	noSet = noSet + e->filhos->tam;
	int final = estadoTerminal(e);
	if( (depth == 0) || (final!=0) )
	{	
		if(player==1)
			return h(e,final,player)+ depth;
		else
			return h(e,final,player)- depth;
	}
	int v;
	if(player==1)
	{
		int quantFilhos = e->filhos->tam;
		no *filho = e->filhos->head;	int i;
		v = - 2000;
		for(i=0;i<quantFilhos;i++)
		{
			v = max( v , alfabeta(filho->o,depth-1,alfa,beta,2) );
			if(beta<=v) /*poda ramo*/
			{	e->h = v; return v; }
			alfa = max(alfa,v);
			filho = filho->prox;
		}	
		e->h = v;
		return v;
	}
	else
	{
		int quantFilhos = e->filhos->tam;
		no *filho = e->filhos->head;	int i;
		v = +2000;
		for(i=0;i<quantFilhos;i++)
		{
			v = min( v , alfabeta(filho->o,depth-1,alfa,beta,1) );
			if(v<=alfa) /*poda ramo*/
			{	e->h = v;return v; }
			beta = min(beta,v);			
			filho = filho->prox;
		}	
		e->h = v;
		return v;
	}
}


void movePeca(estado *e,estado *filho,int *pl,int *pc,int *ml,int *mc,int p)
{
	//if((e->reiP1.l != filho->reiP1.l)||(e->reiP1.c != filho->reiP1.c))
	//{	*pl = e->reiP1.l;*pc = e->reiP1.c;
	//	*ml = filho->reiP1.l;*mc = filho->reiP1.c ;
	//}
	if((e->reiP2.c != filho->reiP2.c)&&(e->torre1P2.c != filho->torre1P2.c))
	{
		*pl = e->reiP2.l;*pc = e->reiP2.c; 
		*ml = filho->reiP2.l;*mc = filho->reiP2.c ;		
	}
	else if((e->reiP2.c != filho->reiP2.c)&&(e->torre2P2.c != filho->torre2P2.c))
	{
		*pl = e->reiP2.l;*pc = e->reiP2.c; 
		*ml = filho->reiP2.l;*mc = filho->reiP2.c ;		
	}
	else if((e->reiP2.l != filho->reiP2.l)||(e->reiP2.c != filho->reiP2.c))
	{	
		*pl = e->reiP2.l;*pc = e->reiP2.c; 
		*ml = filho->reiP2.l;*mc = filho->reiP2.c ;
	}
	else if((e->torre1P2.l != filho->torre1P2.l)||(e->torre1P2.c != filho->torre1P2.c))
	{	*pl = e->torre1P2.l;*pc = e->torre1P2.c;
		if( filho->torre1P2.l != -1)
		{	
			*ml = filho->torre1P2.l;
			*mc = filho->torre1P2.c ;
		}
	}
	else if((e->torre2P2.l != filho->torre2P2.l)||(e->torre2P2.c != filho->torre2P2.c))
	{	*pl = e->torre2P2.l;*pc = e->torre2P2.c;
		if( filho->torre2P2.l != -1)
		{	
			*ml = filho->torre2P2.l;
			*mc = filho->torre2P2.c ;
		}
	}



	//if((e->damaP1.l != filho->damaP1.l)||(e->damaP1.c != filho->damaP1.c))
	//{	*pl = e->damaP1.l;*pc = e->damaP1.c;
	//	if( filho->damaP1.l!=-1)
	//	{
	//		*ml = filho->damaP1.l;
	//		*mc = filho->damaP1.c ;
	//	}
	//}

	if((e->damaP2.l != filho->damaP2.l)||(e->damaP2.c != filho->damaP2.c))
	{	*pl = e->damaP2.l;*pc = e->damaP2.c;
		if( filho->damaP2.l != -1)
		{	
			*ml = filho->damaP2.l;
			*mc = filho->damaP2.c ;
		}
	}

	//if((e->torre1P1.l != filho->torre1P1.l)||(e->torre1P1.c != filho->torre1P1.c))
	//{	*pl = e->torre1P1.l;*pc = e->torre1P1.c;
	//	if( filho->torre1P1.l!=-1)
	//	{
	//		*ml = filho->torre1P1.l;
	//		*mc = filho->torre1P1.c ;
	//	}
	//}



//	if((e->cavalo1P1.l != filho->cavalo1P1.l)||(e->cavalo1P1.c != filho->cavalo1P1.c))
//	{	*pl = e->cavalo1P1.l;*pc = e->cavalo1P1.c;
//		if( filho->cavalo1P1.l!=-1)
//		{
//			*ml = filho->cavalo1P1.l;
//			*mc = filho->cavalo1P1.c ;
//		}
//	}

	if((e->cavalo1P2.l != filho->cavalo1P2.l)||(e->cavalo1P2.c != filho->cavalo1P2.c))
	{	*pl = e->cavalo1P2.l;*pc = e->cavalo1P2.c;
		if( filho->cavalo1P2.l != -1)
		{	
			*ml = filho->cavalo1P2.l;
			*mc = filho->cavalo1P2.c ;
		}
	}

//	if((e->cavalo2P1.l != filho->cavalo2P1.l)||(e->cavalo2P1.c != filho->cavalo2P1.c))
//	{	*pl = e->cavalo2P1.l;*pc = e->cavalo2P1.c;
//		if( filho->cavalo2P1.l != -1)
//		{	
//			*ml = filho->cavalo2P1.l;
//			*mc = filho->cavalo2P1.c ;
//		}
//	}

	if((e->cavalo2P2.l != filho->cavalo2P2.l)||(e->cavalo2P2.c != filho->cavalo2P2.c))
	{	*pl = e->cavalo2P2.l;*pc = e->cavalo2P2.c;
		if( filho->cavalo2P2.l != -1)
		{	
			*ml = filho->cavalo2P2.l;
			*mc = filho->cavalo2P2.c ;
		}
	}

//	if((e->bispo1P1.l != filho->bispo1P1.l)||(e->bispo1P1.c != filho->bispo1P1.c))
//	{	*pl = e->bispo1P1.l;*pc = e->bispo1P1.c;
//		if( filho->bispo1P1.l!=-1)
//		{
//			*ml = filho->bispo1P1.l;
//			*mc = filho->bispo1P1.c ;
//		}
//	}
	if((e->bispo1P2.l != filho->bispo1P2.l)||(e->bispo1P2.c != filho->bispo1P2.c))
	{	*pl = e->bispo1P2.l;*pc = e->bispo1P2.c;
		if( filho->bispo1P2.l != -1)
		{	
			*ml = filho->bispo1P2.l;
			*mc = filho->bispo1P2.c ;
		}
	}
//	if((e->bispo2P1.l != filho->bispo2P1.l)||(e->bispo2P1.c != filho->bispo2P1.c))
//	{	*pl = e->bispo2P1.l;*pc = e->bispo2P1.c;
////		if( filho->bispo2P1.l != -1)
	//	{	
	//		*ml = filho->bispo2P1.l;
	//		*mc = filho->bispo2P1.c ;
	//	}
	//}
	if((e->bispo2P2.l != filho->bispo2P2.l)||(e->bispo2P2.c != filho->bispo2P2.c))
	{	*pl = e->bispo2P2.l;*pc = e->bispo2P2.c;
		if( filho->bispo2P2.l != -1)
		{	
			*ml = filho->bispo2P2.l;
			*mc = filho->bispo2P2.c ;
		}
	}
	//if((e->torre2P1.l != filho->torre2P1.l)||(e->torre2P1.c != filho->torre2P1.c))
	//{	*pl = e->torre2P1.l;*pc = e->torre2P1.c;*ml = filho->torre2P1.l;*mc = filho->torre2P1.c }
	//if((e->torre1P2.l != filho->torre1P2.l)||(e->torre1P2.c != filho->torre1P2.c))
	//{	*pl = e->torre2P2.l;*pc = e->torre2P2.c;*ml = filho->torre2P2.l;*mc = filho->torre2P2.c }

	/*int i;
	for(i=0;i<8;i++)
	{
		if((e->peaoP1[i].l != filho->peaoP1[i].l)||(e->peaoP1[i].c != filho->peaoP1[i].c))
		{	*pl = e->peaoP1[i].l;*pc = e->peaoP1[i].c;
			if(filho->peaoP1[i].l!=-1)			
			{
				*ml = filho->peaoP1[i].l;
				*mc = filho->peaoP1[i].c;
			}
		 }
	}*/
	int i;
	for(i=0;i<8;i++)
	{
		if((e->peaoP2[i].l != filho->peaoP2[i].l)||(e->peaoP2[i].c != filho->peaoP2[i].c))
		{	*pl = e->peaoP2[i].l;*pc = e->peaoP2[i].c;
			if(filho->peaoP2[i].l!=-1)			
			{
				*ml = filho->peaoP2[i].l;
				*mc = filho->peaoP2[i].c; 
			}
		}
	}
}

estado *melhorFilho(estado *e,int player)
{
	no * aux= e->filhos->head;
	int tam = e->filhos->tam;
	estado *melhorFilho;
	int i;
	if(player==1)
	{

		estado *filho;
		for(i=0;i<tam;i++)
		{	
			filho = aux->o;
			if(filho->h == e->h)
				return filho;
			aux = aux->prox;
		}
	}
	else
	{
		estado *filho;
		for(i=0;i<tam;i++)
		{	
			filho = aux->o;

			if(filho->h == e->h)
				return filho;

			aux = aux->prox;
		}
	}
	
	
}
estado *melhorFilhoStart(estado *e,int player)
{
	no * aux= e->filhos->head;
	int tam = e->filhos->tam;
	estado *melhorFilho; 
	int i;
	if(player==1)
	{
		int h; 
		int hMax = -3000;
		estado *filho;
		for(i=0;i<tam;i++)
		{	
			
			filho = aux->o;
			h=fatMaterial(filho);
			if((filho->h == e->h)&&(h>=hMax))
			{
				melhorFilho = filho;	
				hMax = h;
			}
			aux = aux->prox;
		}
	}
	else
	{
		int h;
		int hMax = +3000;
		estado *filho;
		for(i=0;i<tam;i++)
		{	
			filho = aux->o;
			h=fatMaterial(filho);
			if((filho->h == e->h)&&(h<=hMax))
			{
				melhorFilho = filho;
				hMax = h;
			}

			aux = aux->prox;
		}
	}
	return melhorFilho;
}


lista *melhoresFilhos(estado *e,int player)
{
	no * aux= e->filhos->head;
	int tam = e->filhos->tam; 
	lista *melhoresFilhos = constroiLista();
	int i;
	if(player==1)
	{
		estado *filho;
		for(i=0;i<tam;i++)
		{	
			filho = aux->o;

			if(filho->h == 2000)
			{	
				lista *final = constroiLista();
				insereLista(final,filho); 
				return final;
			} 
			//if(filho->h> 1900)
			//	insereLista(melhoresFilhos,filho);
			else if(filho->h == e->h)
				insereLista(melhoresFilhos,filho);
			aux = aux->prox;
		}
	}
	else
	{
		estado *filho;
		for(i=0;i<tam;i++)
		{	
			filho = aux->o;

			if(filho->h == -2000)
			{	
				lista *final = constroiLista();
				insereLista(final,filho); 
				return final;
			} 
			//if(filho->h<-1900)
				//insereLista(melhoresFilhos,filho);
			else if(filho->h == e->h)
				insereLista(melhoresFilhos,filho);

			aux = aux->prox;
		}
	}
	return melhoresFilhos;
}

pos pecaCapturada(estado *e,estado *next,int i)
{
	if(i==1)
	{
		int i; pos pe,pn;
		for(i=1;i<17;i++)
		{
			pe = getPos(e,i);
			pn = getPos(next,i);
			if((pn.l==-1)&&(pe.l!=-1))
				return pe;
		}
	}
}

estado *capturandoPecaOponente(estado *e,lista *bests,int i)
{
	estado *melhorFilho = NULL;

	if(i==1)
	{
		no *aux = bests->head;
		int tam = tamLista(bests);	
	}
	else
	{
		no *aux = bests->head;
		int tam = tamLista(bests);
		int pCurrent = npecas(e,1);
		int pNext; pos capturada;
		for(aux;aux;aux=aux->prox)
		{
			estado *next = aux->o;
			pNext = npecas(next,1);
			capturada = pecaCapturada(e,next,1);
			if(pNext<pCurrent)
			{
				if(posicaoNaoAtacada(capturada.l,capturada.c,e,2))
					return next;
			}
		}
	}
	return melhorFilho;
}

int valorMaterialContraAtaque(estado *e,int peca)
{
	if((peca==1)||(peca==17))
		return 0;
	else if((peca==2)||(peca==18))
		return 90;
	else if((peca==3)||(peca==4)||(peca==19)||(peca==20))
		return 50;
	else if((peca==5)||(peca==6)||(peca==21)||(peca==22))
		return 30;
	else if((peca==7)||(peca==8)||(peca==23)||(peca==24))
		return 30;

	else if((peca>8)&&(peca<17))
	{
		if(promovidoP1(e,9-peca))
			return 90;
		else
			return 10;
	}

	else if((peca>24)&&(peca<33))
	{
		if(promovidoP2(e,25-peca))
			return 90;
		else
			return 10;
	}
}		

int buscaMenorContraAtaqueMaterial(int ml,int mc,estado *e,int jogador)
{
	int diag,vert,rei;
	diag = 2000;
	vert = 2000;
	rei  = 2000;

	if(contraAtaquePeao(ml,mc,e,jogador)!=0)
		return 10;
	
	else if(ataqueCavalo(ml,mc,e,jogador)!=0)
		return 30;

	if(contraAtaqueDiagonal(ml,mc,e,jogador)!=0)
		diag = contraAtaqueDiagonal(ml,mc,e,jogador);

	if(contraAtaqueLinhaColuna(ml,mc,e,jogador)!=0)
		vert = contraAtaqueLinhaColuna(ml,mc,e,jogador);

	if(ataqueRei(ml,mc,e,jogador)!=0)
		rei  = 100;	

	return min(min(diag,vert),rei);	
}

/*estado *evitandoAmeaca(estado *e,lista *bests,int i)
{
	if(i==1)
	{
	}
	else
	{
		estado *next;
		no * aux = bests->head;
		for(aux;aux;aux=aux->prox)
		{
			next = aux->o;
			movePeca(e,next,&pl,&pc,&ml,&mc,2);
			
			if(!posicaoNaoAtacada(e->damaP2.l,e->damaP2.c,e,2))&&
			pecaAtacante = next->tab[ml][mc];	
			atacanteDefendido = !posicaoNaoAtacada(ml,mc,next,1);
			contraAtacada = !posicaoNaoAtacada(ml,mc,next,2);
						
			//if(!contraAtacada)					
			//{ printf("nÃ£o contra atacada\n");return next; }
			if((atacanteDefendido)&&(contraAtacada))
			{
				valorPecaAtacante = valorMaterialContraAtaque(next,next->tab[ml][mc]);
				valorMenorPecaContraAtacante = buscaMenorContraAtaqueMaterial(ml,mc,next,2);

				mostraEstado(next);
				printf("%d %d\n",atacanteDefendido,contraAtacada);
				printf("%d\n",valorMenorPecaContraAtacante);
				printf("%d\n",valorPecaAtacante);
				if(valorMenorPecaContraAtacante > valorPecaAtacante)
					return next;
			}
		}
	}
}*/

estado *ameacandoPecaOponentedeFormaEstrategica(estado *e,lista *bests,int i)
{
	estado *melhorFilho = NULL;

	if(i==1)
	{
		no *aux = bests->head;
		int tam = tamLista(bests);
	}
	else
	{
		int ml,mc,pl,pc;
		int pecaAtacante; 
		int atacanteDefendido; 
		int contraAtacada;

		int valorPecaAtacante;
		int valorMenorPecaContraAtacante;
		no *aux = bests->head;
		for(aux;aux;aux=aux->prox)
		{
			estado *next = aux->o;
			movePeca(e,next,&pl,&pc,&ml,&mc,2);
			
			
			pecaAtacante = next->tab[ml][mc];	
			
			atacanteDefendido = !posicaoNaoAtacada(ml,mc,next,1);
			contraAtacada = !posicaoNaoAtacada(ml,mc,next,2);
						
			
			if((atacanteDefendido)&&(contraAtacada)&&(pecaAmeacada(next,1)))
			{
				valorPecaAtacante = valorMaterialContraAtaque(next,next->tab[ml][mc]);
				valorMenorPecaContraAtacante = buscaMenorContraAtaqueMaterial(ml,mc,next,2);

				if(valorMenorPecaContraAtacante > valorPecaAtacante)
					return next;
			}
		}
	}
	return NULL;
}

estado *ameacandoPecaOponente(estado *e,lista *bests,int i)
{
	estado *melhorFilho = NULL;

	if(i==1)
	{
		no *aux = bests->head;
		int tam = tamLista(bests);
	}
	else
	{
		no *aux = bests->head;
		int pCurrent = nAmeacas(e,1);
		int pNext;
		int pMax = 17;
		for(aux;aux;aux=aux->prox)
		{
			estado *next = aux->o;
			pNext = nAmeacas(next,1);
			if((pNext>pCurrent)&&(pecaAmeacada(e,1)<pMax))
			{
				melhorFilho=next;
				pMax = pecaAmeacada(e,1);
			}
		}
	}
	return melhorFilho;
}

estado *capturandoPecaOponenteDefendida(estado *e,lista *bests,int i)
{
	estado *melhorFilho = NULL;

	if(i==1)
	{
		no *aux = bests->head;
		int tam = tamLista(bests);
	}
	else
	{
		no *aux = bests->head;
		int tam = tamLista(bests);
		int pCurrent = npecas(e,1);
		int pNext; 
		for(aux;aux;aux=aux->prox)
		{
			estado *next = aux->o;
			pNext = npecas(next,1);
			if(pNext<pCurrent)
				return next;
		}
	}
	return melhorFilho;
}

estado *diminueExpansaoOponente(estado *e,lista *bests,int i)
{
	estado *melhorFilho = NULL;

	if(i==1)
	{
		no *aux = bests->head;
		int tam = tamLista(bests);
	}
	else
	{
		int expMax=2000; int expCurrent;
		no *aux = bests->head;
		int tam = tamLista(bests);
		for(aux;aux;aux=aux->prox)
		{
			estado *next = aux->o;
			expCurrent = tamLista(next->filhos);
			if(expCurrent<expMax)
			{
				melhorFilho = next;
				expMax = expCurrent;
			}
		}
	}

	return melhorFilho; /*detalhe*/
}

estado *melhorFilhoDaListaAntiga(lista *bests,int i) /*FunÃ§Ã£o utilizada antigamente*/
{
	no *aux = bests->head;
	estado *filho,*melhorFilho;
	melhorFilho = NULL;

	if(i==1)
	{
		int fatMax = -2000;
		int reiAtacado = 0;
		melhorFilho=aux->o;		
		for(aux;aux;aux=aux->prox)
		{
			filho=aux->o;
			if(filho->h==2000)
				return filho;

			constroiFilhos(filho,2);
			no * neto = filho->filhos->head;
			int fatMinFilho = 2000;
			for(neto;neto;neto=neto->prox)
				if(fatMaterial(neto->o)<fatMinFilho)
					fatMinFilho = fatMaterial(neto->o); 	

			//filho->fatCorrente = filho->fatCorrente + filho->h;
			if((filho->fatCorrente>=fatMax))//&&(filho->ReiP2Atacado>=reiAtacado))
			{
				if((fatMinFilho>=filho->fatCorrente))
				{
					melhorFilho=filho;
					fatMax = filho->fatCorrente;
					melhorFatNeto = fatMinFilho;
				}
			}
		}
	}
	else
	{
		int fatMax = 2000;
		int reiAtacado = 0;
		melhorFilho=aux->o;
		for(aux;aux;aux=aux->prox)
		{
			filho=aux->o;
			
			if(filho->h==-2000)
				return filho;
		
			constroiFilhos(filho,1);
			no * neto = filho->filhos->head;
			int fatMaxFilho = -2000;
	
			for(neto;neto;neto=neto->prox)
				if(fatMaterial(neto->o)>fatMaxFilho)
					fatMaxFilho = fatMaterial(neto->o); 

			//filho->fatCorrente = filho->fatCorrente + filho->h;
			if((filho->fatCorrente<=fatMax))//&&(filho->ReiP1Atacado>=reiAtacado))
			{
				if((fatMaxFilho<=filho->fatCorrente))
				{
					melhorFilho=filho;
					fatMax = filho->fatCorrente;
					melhorFatNeto = fatMaxFilho;
				}
			}
		}
	}
	return melhorFilho;

}


int ml0 = -1;
int mc0 = -1;
int ml1 = -1;
int mc1 = -1;
int ml2 = -1;
int mc2 = -1;
int pecaRepetida=0;

int repetidas = 0;

estado *escolheFilhoNaoRepetido(lista *bests)
{
	no *aux=bests->head; estado *filho;
	for(aux;aux;aux=aux->prox)
	{	//printf("escolheFilhoNaoRepetido\n");
		filho=aux->o;
		//printf("filho->tab[ml1][mc1] = %d , pecaRepetida = %d\n",filho->tab[ml1][mc1],pecaRepetida);		
		if(filho->tab[ml1][mc1]!=pecaRepetida)
			return filho;
	}
	return NULL;
}

estado *melhorFilhoDaLista(estado *e,lista *bests,int i) /*FunÃ§Ã£o utilizada para escolher melhor filho*/
{
	estado *melhorFilho = NULL;
	int h;

	if((repetidas>=1))
	{
		repetidas=0;
		if(bests->tam > 1)
		 	melhorFilho = escolheFilhoNaoRepetido(bests);

		printf("Repetido...\n");
		if(melhorFilho!=NULL)
			return melhorFilho;
		
	}

	/*melhorFilho = evitandoAmeaca(e,bests,i);
	if(melhorFilho!=NULL)
	{
		printf("evitando Ameaca\n");
		return melhorFilho;
	}*/

	melhorFilho = ameacandoPecaOponentedeFormaEstrategica(e,bests,i);
	if(melhorFilho!=NULL)
	{
		printf("capturandoPecaOponente de forma Estrategica\n");
		return melhorFilho;
	}

	melhorFilho = capturandoPecaOponente(e,bests,i);
	if(melhorFilho!=NULL)
	{
		printf("capturandoPecaOponente\n");
		return melhorFilho;
	}

	melhorFilho = ameacandoPecaOponente(e,bests,i);
	if(melhorFilho!=NULL)
	{
		printf("ameacandoPecaOponente\n");
		return melhorFilho;
	}

	melhorFilho = capturandoPecaOponenteDefendida(e,bests,i);
	if(melhorFilho!=NULL)
	{
		printf("capturandoPecaOponenteDefendida\n");
		return melhorFilho;
	}

	/*melhorFilho = diminueExpansaoOponente(e,bests,i);
	if(melhorFilho!=NULL)
	{
		printf("diminueExpansaoOponente\n");
		return melhorFilho;
	}*/

	melhorFilho = (estado *)melhorFilhoDaListaAntiga(bests,i);	
	printf("melhor filho com jogadas prosperas\n");
	return melhorFilho;
}




int verificaLim(int valor, int maximo)
{
	if((0<=valor)&&(valor<maximo))
		return 1;
	else return 0;
}

int acaoDentroTabuleiro(int pl,int pc,int ml,int mc)
{
	if((verificaLim(pl,8))&&(verificaLim(pc,8))&& 
		(verificaLim(ml,8))&&(verificaLim(mc,8)) )
		return 1;
	else return 0;
}		

void mostraProm(estado *e)
{
	byte p1 = e->prom1;
	byte p2 = e->prom2;
	int i;
	for(i=0;i<8;i++)
	{
		printf("%d",p1%2);
		p1 = p1 >> 1;
	}
	printf(" ");
	for(i=0;i<8;i++)
	{
		printf("%d",p2%2);
		p2 = p2 >> 1;
	}
	printf("\n");
}

int main()
{
	/*As variaveis pl e pc registram, 
	respectivamente linha e coluna selecionada*/
	int pl,pc; 
	/*As variaveis ml e mc registram, 
	respectivamente linha e coluna de destino*/
	int ml,mc; 

	//int repetic = 0;
	
	/*Constroi o Estado Inicial do tabuleiro*/ 
	//estado *e = constroiEstadoInicialTeste3(); 	
	//estado *e = constroiEstadoInicial8(); 	
	estado *e = constroiEstadoInicial();
	//estado *e = constroiEstadoInicialErro();
	/*Estado auxiliar para copias*/
	estado *aux; 
	/* matriz de tabuleiro temporario*/
	byte **tabuleiroT = constroiTabuleiroTemporario(); 

	/*FILE *fp = fopen("jogadas.txt","w"); 
	Arquivo utilizado opcionalmente para arquivar o jogo*/
	int inicio = 1; //Indica primeira rodada
	/*Jogador que comeca (1 - Ser humano ou 2 - Computador) */
	int i=1;

	/*Busca e classificaÃ§Ã£o para as primeiras jogadas do jogador Humano*/
	int a = alfabeta(e,DEPTH,-2000,2000,i); 
	while((estadoTerminal(e)==0)||(inicio))
	{
		/*Repere enquanto o estado atual nÃ£o Ã© terminal 
		ou estamos no primeiro lance do jogo*/	

		/*Se numero de rodadas Ã© maior que 10 ou 
		algum jogador possui metade das pecas capturadas
		Atualizamos a variavel global de momentoJogo
		para Final de jogo esta variÃ¡vel representa a 
		componente heurÃ­stica Temporal que influencia no calculo das 
		componentes heurÃ­sticas materiais e posicionais*/
		if( (rodadas>=10) || (npecas(e,1)<=8) || (npecas(e,2)<=8) )
			momentoJogo=3;

		/*FunÃ§Ã£o Utilizada para verificar 
		se algum rei estÃ¡ sozinho iniciando 
		a contagem de 50 lances
		para ser declarado empate caso 
		o oponente nÃ£o execute um Xeque-Mate*/
		verificaReiSozinho(e);
		if(reiSo)
			qtLances--;
		/*mostraEstadoFile(e,fp); 
		//Utilizado opcionalmente para escrever o estado atual em um arquivo*/

		if(i==1) /*Movimento do jogador humano*/
		{	
			do{
				//Utiliza um tabuleiro temporario copiado do estado
				copia(tabuleiroT,e); 	

				/*InformaÃ§Ã£o das prÃ³ximas jogadas e estado atual*/
				//printf("mostrando filhos de %d\n",i);				
				//mostraFilhos(e);
				//printf("fim mostrando filhos de %d\n",i);

				printf("\n\nEstado Atual:{\n");
				mostraEstado(e);
				printf("FAT atual = %d\n",e->h);
				printf("}\n\n");
				
				/*InformaÃ§Ã£o de nÃ³s construidos na 
				busca heurÃ­stica competitiva realizada*/
				printf("Estimativa de posicoes avaliadas pela busca alfabeta = %d\n", noSet);

				/*Demais informaÃ§Ãµes*/	
				printf("Rodadas = %d, Momento do jogo = %d\n"
				,rodadas,momentoJogo);
				printf("Rei sozinho = %d\n", reiSo);

				if(reiSo)
					printf("qt de Lances 	restantes com rei Sozinho = %d\n", qtLances);

				//printf("Promocoes: "); mostraProm(e);

								

				//LÃª a aÃ§Ã£o do jogador humano
				printf("Digite a linha e coluna da peca que deseja mover\n");
				scanf("%d %d",&pl,&pc);
				printf("Digite a linha e coluna da posicao para qual deseja mover\n");
				scanf("%d %d",&ml,&mc);

				/*Verifica se a jogada esta dentro do tabuleiro*/
				if(acaoDentroTabuleiro(pl,pc,ml,mc)) 
				{
					pos r1 = getPos(e,1); /*Posicao Rei*/
					pos t1 = getPos(e,3); /*Posicao Torre1*/
					pos t2 = getPos(e,4); /*Posicao Torre2*/

					/*Atualiza tabuleiro temporario*/
					if((r1.l==0)&&(r1.c==3)&&(r1.l==pl)&&(r1.c==pc)&&
					(ml==0)&&(mc==1)&&(t1.l==0)&&(t1.c==0)) 
					{
						/*ROOK Menor*/
						tabuleiroT[pl][pc]=0;
						tabuleiroT[ml][mc]=e->tab[pl][pc];
						tabuleiroT[0][0]=0;
						tabuleiroT[0][2]=3;
					}
					else if((r1.l==0)&&(r1.c==3)&&(r1.l==pl)&&(r1.c==pc)&&
						(ml==0)&&(mc==5)&&(t2.l==0)&&(t2.c==7)) 
					{
						/*ROOK Maior*/
						tabuleiroT[pl][pc]=0;
						tabuleiroT[ml][mc]=e->tab[pl][pc];
						tabuleiroT[0][7]=0;
						tabuleiroT[0][4]=4;
					}
					else	
					{
						/*Movimento ou Captura*/
						tabuleiroT[pl][pc]=0;
						tabuleiroT[ml][mc]=e->tab[pl][pc];	
					}
				}

				/*Repete enquanto o movimento nÃ£o estÃ¡ no tabuleiro ou 
				nÃ£o pertence a lista de prÃ³ximas jogadas, como exemplo
				o movimento deixa seu rei em xeque ou o movimento Ã© invÃ¡lido*/
			}while( (!verificaMovimento(e,pl,pc,ml,mc)) || (contemNaLista(tabuleiroT,e->filhos)==NULL) );
			
			/*Estado filho escolhido pelo jogador humano*/
			estado *filhoEscolhido = contemNaLista(tabuleiroT,e->filhos);
			
			/*Copia estado escolhido para um estado auxiliar 
			pois todos os descendentes do estado antigo 
			seram removidos da memÃ³ria*/
			aux = constroiEstadoApartirDe(filhoEscolhido); 

			/*Libera memÃ³ria utilizada pela Ãºltima busca 
			apagando todos os descendentes construidos 
			na Ã¡rvore de busca (Easy-Memory)*/
			destroiEstado(e); 

			/*Estado atual passa a ser o filho copiado*/
			e = aux; 

			/*Variavel global reiniciada para contar 
			conjunto de nÃ³s avaliados na prÃ³xima busca*/
			noSet=0; 

			/*Busca HeurÃ­stica e classificaÃ§Ã£o 
			para o prÃ³ximo jogador (jogador automÃ¡tico) 
			no estado escolhido com uma profundidade 
			DEPTH definida no cabeÃ§alho do programa.
			Esta profundidade pode ser ajustada 
			de acordo com seus interesses e paciÃªncia*/
			i=2; a=alfabeta(e,DEPTH,-2000,2000,i); 
		}

		else  /*Movimento do jogador automÃ¡tico*/
		{	
			do{
				printf("Filhos do jogador 2\n");
			
				mostraFilhos(e);

				printf("Fim Filhos do jogador 2\n");
	
				if(rodadas==0)  /*Abertura simples e inicial do jogador automÃ¡tico*/
				{
						/*move peao do rei uma casa para frente*/

						pl=6;pc=3;ml=4;mc=3;
						tabuleiroT[6][3]=0;
						tabuleiroT[5][3]=28;					
				}				
				else
				{

					/*Lista que contÃ©m os estados filhos 
					com melhor decisÃ£o para jogador automÃ¡tico*/
					lista* bests = melhoresFilhos(e,i);

					/*Escolhe o possÃ­vel melhor filho dos melhores filhos*/
					estado *filhoEscolhido = melhorFilhoDaLista(e,bests,i);	

					if(filhoEscolhido)
					{
						/*Verifica qual peÃ§a foi mexida pelo jogador automÃ¡tico*/
						movePeca(e,filhoEscolhido,&pl,&pc,&ml,&mc,i);
						/*Variaveis globais utilizadas pelo jogador automatico
						  para impedir repeticao de 3 lances*/
						ml2 = ml1; 
						mc2 = mc1;
						ml1 = ml0;
						mc1 = mc0;
						ml0 = ml;
						mc0 = mc;
						
						if((ml2==ml)&&(mc2==mc))
						{	
							
							pecaRepetida = filhoEscolhido->tab[ml][mc];
							repetidas++; 
						}

						/*Captura*/
						if(tabuleiroT[ml][mc]!=0) 
						{								
							pos *capturada = getPosA(e,tabuleiroT[ml][mc]) ;
							setPos(capturada,-1,-1);
						}
						/*ROOK Menor*/
						if((e->reiP2.c != filhoEscolhido->reiP2.c)&&
							(e->torre1P2.c != filhoEscolhido->torre1P2.c))
						{
							tabuleiroT[7][0]=0;
							tabuleiroT[7][2]=19;
						}
						/*ROOK Maior*/						
						else if((e->reiP2.c != filhoEscolhido->reiP2.c)&&
							(e->torre2P2.c != filhoEscolhido->torre2P2.c))
						{
							tabuleiroT[7][7]=0;
							tabuleiroT[7][4]=20;
						}
						/*Peca movimentada*/
						tabuleiroT[pl][pc]=0;
						tabuleiroT[ml][mc]=e->tab[pl][pc];
					
					}
					else 
				/*Se acontecer algum erro na escolha da proxima jogada*/
					{
						printf("Erro na funcao que escolhe melhor filho\n");
						destroiEstado(e);
						return 0;
					}
				}
				
				/*Repete enquanto o movimento nÃ£o estÃ¡ no tabuleiro ou 
			        nÃ£o pertence a lista de prÃ³ximas jogadas       */
			}while( (!verificaMovimento(e,pl,pc,ml,mc)) || 
			(contemNaLista(tabuleiroT,e->filhos)==NULL) );

			mostraEstado(e);
			/*Estado filho escolhido pelo jogador automÃ¡tico*/
			estado *filhoEscolhido = contemNaLista(tabuleiroT,e->filhos);

			/*Copia estado escolhido para um estado auxiliar 
			pois todos os descendentes do estado antigo 
			seram removidos da memÃ³ria*/
			aux = constroiEstadoApartirDe(filhoEscolhido);

			/*Libera memÃ³ria utilizada pela Ãºltima busca 
			apagando todos os descendentes construidos 
			na Ã¡rvore de busca (Easy-Memory)*/
			destroiEstado(e);

			/*Estado atual passa a ser o filho copiado*/
			e = aux;

			/*Apos jogada realizada pelo jogador 
			automÃ¡tico o numero de rodadas Ã© incrementado*/
			rodadas++; 

			/*Variavel global reiniciada para contar 
			conjunto de nÃ³s avaliados na prÃ³xima busca*/
			noSet=0; 

			/*Busca HeurÃ­stica e classificaÃ§Ã£o 
			para o prÃ³ximo jogador (jogador Humano) 
			no estado escolhido pelo jogador 
			automÃ¡tido com uma profundidade DEPTH 
			definida no cabeÃ§alho do programa. 
			Esta profundidade pode ser ajustada 
			de acordo com seus interesses e paciÃªncia*/
			i=1; a = alfabeta(e,DEPTH,-2000,2000,i);
		}
		inicio = 0;  		
	}
	/*Fim da partida*/
	mostraEstado(e);	
	/*mostraEstadoFile(e,fp);fclose(fp);*/

	/*Calcula Resultado da partida*/
	int resultado = estadoTerminal(e);
	
	/*Empate Final dos 50 lances com Rei sozinho*/
	if((resultado==-1)&&(qtLances==0))
		printf("Empate - Fim de 50 lances com Rei sozinho\n");

	/*Empate com Rei afogado*/		
	else if((resultado==-1)&&(tamLista(e->filhos)==0))
		printf("Empate - Rei afogado\n");

	/* Xeque-Mate :) */
	else 
	{
		if(i==1)
			printf("Xeque Mate - Voce perdeu a partida!\n");
		else
			printf("Xeque Mate - Voce venceu a partida!\n");	
	}

	/*Destroi o estado e Libera tabuleiro temporÃ¡rio da memÃ³ria*/
	destroiEstado(e);
	destroiTabTemp(tabuleiroT);
	/*Bye - See you in the next game*/	
	return 0;
}